(use-modules (guix transformations))

;; Check if this is still necessary (https://github.com/sverrirs/jekyll-paginate-v2/pull/195)
(define jekyll-paginate-v2
  ((options->transformation
     '((with-branch . "ruby-jekyll-paginate-v2=cache-mismatch-error")
       (with-git-url . "ruby-jekyll-paginate-v2=https://github.com/jameshi16/jekyll-paginate-v2")))
   (specification->package "ruby-jekyll-paginate-v2")))

(packages->manifest
  (cons jekyll-paginate-v2
        (map specification->package
             '("jekyll" "ruby-i18n" "ruby" "rsync"))))
;; run `rm Gemfile.lock` then `jekyll build -d public` then rsync the result to rivendell.
;; cd public
;; rsync --progress --recursive * ${USER}@rivendell.linuxfromscratch.org:/srv/www/www.fr.linuxfromscratch.org/

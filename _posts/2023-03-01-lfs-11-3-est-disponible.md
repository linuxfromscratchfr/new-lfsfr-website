---
layout: post
title: LFS 11.3 est disponible
date: 2023-03-01
tags: LFS
---

Nous sommes heureux de vous annoncer la sortie de LFS 11.3 conjointement en
version SysV et Systemd, ainsi que la sortie de BLFS 11.3 dans ces deux
versions.

Il s'agit d'une version majeure de LFS et BLFS.

Cette nouvelle version contient une mise à jour majeure de la chaîne d’outils
avec les versions gcc-12.2.0, glibc-2.37 et binutils-2.40. Un énorme travail
rédactionnel a été réalisé pour moderniser les textes tout au long du livre.
Enfin, le noyau Linux a été mis à jour vers la version 6.1.11.

Un grand merci à celles et ceux qui ont contribué à la traduction et aux
relectures ! Nous tenons tout particulièrement à remercier les étudiants et étudiantes en Master
traduction et interprétation à l'Université de Rennes 2, qui cette année encore
ont répondu présents pour nous aider à traduire !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur le canal IRC, le
[dépôt gitlab](https://framagit.org/linuxfromscratchfr/new-lfsfr-website/)
ou [mastodon](https://mamot.fr/@lfsfr).

Bonne lecture !

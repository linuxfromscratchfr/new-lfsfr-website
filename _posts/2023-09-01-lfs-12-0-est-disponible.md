---
layout: post
title: LFS 12.0 est disponible
date: 2023-09-01
tags: LFS
---

Nous sommes heureux de vous annoncer la sortie de LFS 12.0 conjointement en
version SysV et Systemd, ainsi que la sortie de BLFS 12.0 dans ces deux
versions.

Il s'agit d'une version majeure de LFS et BLFS.

Cette nouvelle version contient une mise à jour majeure de la chaîne d’outils
avec les versions binutils-2.41, gcc-13.2.0 et glibc-2.38. En tout, 38 paquets
ont été mis à jour depuis la dernière version. Enfin, le noyau Linux a été mis
à jour vers la version 6.4.12.

Parmi les changements majeurs les plus notables se trouvent :

- L'ajout de libxcrypt avec un paquet séparé. Ce paquet était avant inclus
  dans glibc.
- L'extraction d'udev directement à partir de l'archive de systemd pour la
  version sysV du livre. Ce changement nécessite également de construire les
  modules Python jinja et markupsafe.
- L'utilisation du nouveau paquet pkgconf au lieu de pkg-config car ce dernier
  n'est plus maintenu.
- L'ajout du nouveau module Python flit-core aux dépendances requises du module
  wheel.

Un grand merci à celles et ceux qui ont contribué à la traduction et aux
relectures !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur le canal IRC, le
[dépôt gitlab](https://framagit.org/linuxfromscratchfr/new-lfsfr-website/)
ou [mastodon](https://mamot.fr/@lfsfr).

Bonne lecture !

---
layout: post
title: Une étape décisive de franchie
date: 2011-03-06
tags: MAIN
---

Nous sommes particulièrement heureux de vous annoncer qu'après deux ans et demi de travail actif, la traduction de blfs a été partiellement terminée. Pourquoi partiellement? Parce que c'est celle de la révision svn n°7863 qui a été terminée. Reste actuellement à la mettre à jour. Mais l'ampleur du travail est sans commune mesure avec ce qui a été accompli depuis la reprise du projet et nous pouvons espérer définitivement nous synchroniser sur le projet anglophone d'ici l'été.

Cette étape est cruciale pour le projet francophone lfs. Elle va permettre de lui donner une nouvelle dynamique. Les tâches à accomplir seront désormais différentes, comme vous le verrez en consultant les moyens dont vous disposez pour nous aider sur les pages de chaque projet. Pour blfs, il s'agira d'analyser un fichier txt, pour y déceler les différences qu'il indique entre la VO actuelle et celle sur laquelle nous nous sommes appuyés, pour ensuite les répercuter sur la traduction. Pour les autres projets, un travail de relecture, de coordination, de finition peut s'opérer. hlfs exige davantage de reprise, mais le flou de la version originale peut nous donner du temps. Enfin, plus d'astuces peuvent être traduites. Tous les éléments figureront sur le site.

D'autres éléments importants de cette étape: un débogage xml peut commencer utilement pour, à terme, générer le html de blfs, voire le pdf. De plus, le livre peut être amendé pour être rendu plus spécifique au public visé: compiler openoffice.org en français, firefox, seamonkey... en rajoutant les bonnes options et en amendant de ce fait le livre pour les francophones.

Bref, vous l'aurez compris, cette étape marque un aboutissement et ouvre des tâches nouvelles, plus variées et où davantage de personnes peuvent s'investir. N'hésitez donc pas.
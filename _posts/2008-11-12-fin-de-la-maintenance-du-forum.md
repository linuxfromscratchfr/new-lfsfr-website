---
layout: post
title: Fin de la maintenance du forum
date: 2008-11-12
tags: MAIN
---

Le forum hébergé par absolinux est à nouveau fonctionnel. La mise à jour s'est déroulée avec succès. Nous allons grâce à cette évolution rendre ce forum encore plus performant et sécurisé.

N'hésitez pas à poser vos questions, nous nous ferons un plaisir d'y répondre dans les meilleurs délais.
---
layout: post
title: BLFS 11.3 est là !
date: 2023-03-01
tags: BLFS
---

Nous sommes heureux de vous annoncer la sortie de BLFS 11.3 conjointement
en version SysV et Systemd.

Cette nouvelle version de BLFS contient environ 1000 paquets en plus du livre
Linux From Scratch de base version 11.3. Cette publication fait suite à la
mise à jour de 1357 paquets depuis la version précédente et un
énorme travail rédactionnel a été réalisé pour améliorer les textes et
la présentation tout au long du livre.

Parmi les autres changements dans BLFS, on trouve l'ajout de nombreux modules
Python pour pouvoir générer la documentation des paquets qui utilisent les
applications Gi-DocGen ou Sphinx.  Plusieurs environnements de bureau ont
également été mis à jour :

- Gnome 43
- KDE/Plasma 5.26.5
- Xfce 4.18

Enfin, livre s'est vu rajouter un nouveau pilote Xorg, intel-media-driver, pour
fournir l'accélération vidéo sur les CPU Intel de gammes Broadwell et suivantes.

Un grand merci à celles et ceux qui ont contribué à la traduction et à la
relecture ! Nous tenons tout particulièrement à remercier les étudiants et étudiantes en Master
traduction et interprétation à l'Université de Rennes 2, qui cette année encore
ont répondu présents pour nous aider à traduire !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur le canal IRC, le
[dépôt gitlab](https://framagit.org/linuxfromscratchfr/new-lfsfr-website/)
ou [mastodon](https://mamot.fr/@lfsfr).

Bonne lecture !

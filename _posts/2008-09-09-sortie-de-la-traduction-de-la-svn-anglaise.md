---
layout: post
title: Sortie de la traduction de la SVN anglaise
date: 2008-09-09
tags: LFS
---

L'équipe de traduction de LFS est ravie de vous annoncer qu'elle vient d'achever la relecture de la traduction de la version SVN anglaise du livre LFS. Vous pouvez la trouver ici.

Cette version évoluera en permanence en fonction des mises à jour de la version anglaise. Bonne lecture.
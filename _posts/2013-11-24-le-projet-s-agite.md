---
layout: post
title: Le projet s'agite
date: 2013-11-24
tags: MAIN
---

Notre projet de traduction a désormais adopté un rythme de croisière confortable. Nous sommes en effet capables de publier rapidement de nouvelles versions parallèles à celles anglaises et de suivre les mises à jour. Pour BLFS, le travail de Denis et ses outils permettent désormais de faire face aux nombreux commits et d'organiser un rythme de travail régulier et plus espacé.

Désormais, nous sommes en mesure d'avoir un peu de recul, chaque semaine, sur vactualité upstream du projet. Nous vous la ferons dès à présent partager à travers une news hebdomadaire, ici et sur la liste de diffusion, propre à chaque projet. Elle comportera des éléments statistiques ou factuels vous permettant de savoir ce qu'il se passe en amont de votre projet préféré.

Cette évolution va donner au site un certain mouvement. Nous espérons que vous serez ainsi contents d'avoir des news plus fraîches et accessibles d'abord aux anglophones, portées désormais sur la place francophone!
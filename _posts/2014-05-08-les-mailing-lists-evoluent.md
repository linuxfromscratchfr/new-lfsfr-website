---
layout: post
title: Les mailing lists évoluent
date: 2014-05-08
tags: MAIN
---

Comme vous le savez, les listes de diffusion du projet lfs-fr sont hébergées directement au sein du serveur de linux from scratch. Il se trouve que l'hébergeur du projet a proposé d'augmenter les capacités du serveur, tant en termes de capacité de travail que de bande passante.

La première conséquence est que les adresses des listes de diffusion changent. Celle de la traduction est bien sûr concernée. Après l'arrobas, il faudra désormais écrire lists.linuxfromscratch.org et non plus le simple nom du domaine de LFS. Cela ne signifie pas que vous devez vous réinscrire, les comptes ont été migrés. Cela signifie par contre que vous devez utiliser désormais cette adresse pour écrire à la liste.

Bref, changement impactant, apportant en apparence peu aux lecteurs finaux. Pourtant, outre la bande passante, LFS a pensé à ses fidèles et leur a fait une petite surprise.

Elle était demandée depuis longtemps, ils l'ont fait: les archives des listes de diffusion sont de nouveau publiques. Désormais, vous pouvez comme avant consulter les archives complètes des listes, dont lfs-traducfr. Ces archives ne redémarrent pas au mois de mai, mais elles incluent toute la période où elles n'étaient pas consultables. Elles sont donc exhaustives.

Enfin, petit trait esthétique: l'ensemble de ;a page d'info de la liste a été traduit.

Cette migration a donc eu beaucoup de conséquences positives. Elle laisse la place à de nouvelles évolutions sur lesquelles nous travaillons et que nous partagerons ici le moment venu. En attendant, profitez de ces outils d'aide et d'échanges et amusez-vous bien avec les ouvrages. N'oubliez pas de nous faire remonter les erreurs que vous y trouvez, par exemple via la liste de diffusion.

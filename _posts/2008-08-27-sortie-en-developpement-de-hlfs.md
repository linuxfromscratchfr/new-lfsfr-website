---
layout: post
title: Sortie en développement de HLFS
date: 2008-08-27
tags: HLFS
---

27 août 2008

La rentrée sera donc marquée par la sortie de la traduction de HLFS. La version anglaise est en développement. Celle française suivra de manière synchronisée.

Cette version traduite, bien entendu, est à relire et il faut s'assurer qu'elle est compilable. Donc il reste du travail, mais elle est disponible. Vous pouvez la trouver ici.

N'hésitez pas si vous voulez relire et/ou déboguer. Les contributions sont bienvenues.
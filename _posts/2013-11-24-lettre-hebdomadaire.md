---
layout: post
title: Lettre hebdomadaire
date: 2013-11-24
tags: BLFS
---

Statistiques de la semaine :
- 30 commits traduits
- Les paquets suivants ont été mis à jour :
 - Passage à Python-2.7.6
 - Passage à iptables-1.4.21
 - Passage à ruby-2.0.0-p353
 - Passage à dovecot-2.2.8
 - Passage à ffmpeg-2.1.1
 - Passage à krb5-1.11.4
 - Passage à libpng-1.6.7
 - Passage à librsvg-2.40.1
 - Passage à midori-0.5.6
 - Passage à thunderbird-24.1.1
 - Passage à openldap-2.4.38
 - Passage à Python-3.3.3
 - Passage à clutter-1.16.2
 - Passage à epiphany-3.10.2
 - Passage à pixman-0.32.4
 - Passage à seamonkey-2.22.1
 - Passage à ImageMagick-6.8.7-6
 - Passage à glibmm-2.38.1
 - Passage à icewm-1.3.8
 - Passage à harfbuzz-0.9.24
 - Passage à libdrm-2.4.48
 - Passage à apr-util-1.5.3
 - Passage à apr-1.5.0
 - Passage à php-5.5.6

Les faits de la semaine :

Un nouvel auteur :

 Pierre qui poste régulièrement sur les différentes ML BLFS vient de rejoindre les auteurs en effectuant ses premiers commits pour
le livre.


La version r12273 du 23/11/2013 est en ligne sur le site
---
layout: post
title: Quelques infos
date: 2005-04-16
tags: MAIN
---

Il restait encore deux fichiers à traduire pour LFS-6.0. Cela a été fait cette semaine. Mais, cette traduction a toujours besoin de vous pour une relecture. Même si la sortie de LFS-6.0 ne devrait plus tarder, ce sera toujours ça de fait.

BLFS-6.0 est sorti depuis quelques jours. Je compte commencer sa mise à jour mi-mai. En attendant, toute mon attention se concentre sur d'autres projets.
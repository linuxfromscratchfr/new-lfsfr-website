---
layout: post
title: BLFS 7.9 est enfin là !
date: 2016-09-25
tags: BLFS
---

Nous sommes heureux de vous annoncer la sortie de BLFS 7.9 en version SysV. Du fait d'importants changements dans l'équipe de traduction, cette version a mis beaucoup de temps avant d'être publiée. Il s'agit par ailleurs de la première version de BLFS à être passée sur le système de traduction gettext, comme LFS, ce qui a permis au livre de bénéficier de la première relecture complète de son histoire. Grâce à ces nouveautés, de nombreuses traductions ont été améliorées et nous avons élargi le nombre de contributeurs.

La version stable en anglais est actuellement la version 7.10 de BLFS, et nous allons devoir prendre encore un peu de temps avant de pouvoir la publier. La prochaine version devrait aussi être la première version BLFS à sortir conjointement dans sa variante SysV et Systemd, ce qui demandera un peu de travail avant d'y parvenir.

Un grand merci à celles et ceux qui ont contribué à la traduction, en particulier à Alexandre, Denis, Jean-Philippe et TheSuperGeek.

Suite au changement de système de traduction, il est possible que certaines parties du livre ne soient pas affichées correctement. Si vous trouvez la moindre erreur, n'hésitez surtout pas à nous en faire part, que ce soit sur IRC, sur le forum ou sur les listes de diffusion.
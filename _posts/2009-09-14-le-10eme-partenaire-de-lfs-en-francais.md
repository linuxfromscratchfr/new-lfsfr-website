---
layout: post
title: Le 10ème partenaire de lfs en français
date: 2009-09-14
tags: MAIN
---

En cette rentrée, l'équipe de traduction de lfs est ravie de pouvoir inscrire un nouveau partenaire à notre projet, qui est le dixième site. Avec 400 visites quotidiennes référencées, nous avons été sollicités par un certain nombre de projets, plus ou moins hétéroclites et liés au logiciel libre, pour être référencés. Leur étude nous a révélé des atouts à leur sujet et nous a conduit à accepter leur demande. Certains partenaires sont même des coups de coeur de notre équipe. L'arrivée du dixième site nous a semblé un bon moment pour souligner l'événement, notamment car ce n'est pas la partie du site la plus regardée.

Cela nous montre que le projet trouve un certain écho sur le Web. Merci à vous. C'est en effet grâce aux utilisateurs que le site est bien référencé, puisque sa place sur les moteurs de recherche dépend du nombre de visites. Le forum voit enfin une activité progresser, mettant en évidence l'intérêt croissant pour lfs parmi les francophones. Cela nous motive à terminer l'actuelle relecture de clfs et le gros chantier de blfs, en attendant la mise à jour de hlfs qui s'annonce importante.

Nous attendons nos futurs partenaires, contributions et retours concernant le projet. N'hésitez pas, même si vous voulez nous aider. Notre équipe a besoin de soutien, notamment pour assurer les traductions, les relectures, et une certaine interactivité avec le projet originel en anglais. Cette dernière voie est aussi un des facteurs de la promotion de lfs.

---
layout: post
title: Passage de cvs à svn
date: 2009-02-22
tags: MAIN
---

Le projet lfs-fr vient de connaître une transformation attendue depuis longtemps. Ses sources sont actuellement sur des dépôts svn et non plus cvs. Vous retrouverez les adresses d'accès sur les pages appropriées.

Cette transformation vise à faciliter la gestion et la coordination du projet, tant pour les gestionnaires que pour les utilisateurs. Son interface Web en français et son arborescence mieux présentée, sa clarté dans les numéros de révision vous aideront à mieux vous y retrouver.

N'hésitez pas pour tout retour.
---
layout: post
title: LFS fait lui aussi sa rentrée
date: 2013-09-08
tags: LFS
---

L'équipe de LFS-fr est heureuse de vous annoncer la publication de LFS version 7.4, accompagné de sa traduction disponible 2H après la publication anglaise. Cette version contient de nombreux changements par rapport à celle 7.3, notamment s'agissant des versions de logiciels aussi importants que Binutils, le noyau Linux,, GCC et Glibc. Vous y trouverez aussi des corrections de sécurité. La traduction, quant à elle, capitalise les relectures faites depuis la dernière version.

Un travail rédactionnel a également été accompli sur les explications contenues dans l'ouvrage, qui a cherché à la fois à les clarifier et à les améliorer. Pour cette version, la traduction a fait l'objet d'une relecture particulièrement poussée, dont nous remercions infiniment les contributeurs.

Nous tenions à remercier tous ceux qui ont participé à la traduction du livre et à sa relecture. Leur soutien nous a été précieux et leur fidélité est particulièrement motivante.

N'hésitez pas à lire et télécharger le livre sur ce site aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
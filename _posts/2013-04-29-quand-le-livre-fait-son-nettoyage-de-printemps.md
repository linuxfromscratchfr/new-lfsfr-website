---
layout: post
title: Quand le livre fait son nettoyage de printemps
date: 2013-04-29
tags: CLFS
---

L'équipe CLFS a annoncé la sortie de la version stable 2.0.0 la semaine dernière.

L'équipe de traduction annonce la publication de la traduction pour les
livres 64bits et multilib, sachant qu'en multilib, le travail est en
cours et partiellement réalisé (notamment, pas relu).

Nous sommes donc heureux de vous proposer CLFS 2.0.0 en français pour
64bits et multilib. Autant dire que les changements par rapport à la
version 1.1 sont considérables, les mises à jour majeures. D'importantes évolutions sont également apparues au niveau de l'ensemble d'outils.

Bonne lecture et n'hésitez pas à nous adresser tout retour sur ce projet.
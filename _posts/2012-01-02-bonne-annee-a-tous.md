---
layout: post
title: Bonne année à tous
date: 2012-01-02
tags: MAIN
---

Toute l'équipe francophone de Linux From Scratch se joint à moi pour souhaiter à tous les visiteurs, lecteurs, utilisateurs et contributeurs du projet une bonne année. Nous espérons que 2012 vous apportera tout ce que vous souhaitez, la santé et la sérénité.

LFS poursuivra son parcours, en espérant rester toujours intéressant pour ses lecteurs. Le projet de traduction sera, je l'espère, autant dynamique, voire davantage, que cette année. Merci d'ailleurs aux contributeurs qui nous aident à avancer, à y croire, à poursuivre l'effort.
---
layout: post
title: Fin du débogage de CLFS
date: 2009-01-07
tags: CLFS
---

Le xml de CLFS vient d'être débogué! Cela signifie qu'il est possible de générer une version html et pdf des ouvrages. A été entièrement traduit l'ouvrage décrivant la construction d'un système pur 64 bits. La relecture peut commencer.

Ceux qui veulent y aider peuvent se manifester. Nous n'attendons que cela pour la mise en ligne du html.
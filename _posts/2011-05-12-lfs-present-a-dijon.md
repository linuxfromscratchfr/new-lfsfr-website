---
layout: post
title: LFS présent à Dijon
date: 2011-05-12
tags: MAIN
---

Nous avons eu le plaisir, lors du salon les Solutions Linux 2011, de croiser des personnes intéressées par lfs qui en ont discuté avec les personnes présentes sur le stand de l'association Traduc.org.

Aussi nous poursuivons la tournée. Le 27 et 28 mai se tiennent à Dijon les Journées du Logiciel Libre [http://coagul.org/drupal/%C3%A9v%C3%A8nement/deux-journ%C3%A9es-conf%C3%A9rences-et-dateliers-%C3%A0-liut-dijon](http://coagul.org/drupal/%C3%A9v%C3%A8nement/deux-journ%C3%A9es-conf%C3%A9rences-et-dateliers-%C3%A0-liut-dijon). L'association Traduc.org s'y rendra et sera représentée par son trésorier et son Président. C'est pourquoi on peut dire que lfs sera représenté. Outre le stand Traduc, il y aura une conférence sur l'accessibilité offerte par le logiciel libre.

Cet événement vous permettra de rencontrer dans la région lyonnaise, pour ceux qui le souhaitent et qui le peuvent, des travailleurs solitaires mais qui aiment le contact du public... Alors n'hésitez pas à vous y rendre.

Habitants de Dijon ou en transit dans la région qui veulent voir physiquement un traducteur de lfs et échanger avec lui, n'hésitez pas à nous rejoindre vendredi et samedi. Toutes les informations pratiques figurent sur le site de l'événement. Nous espérons vous rencontrer.
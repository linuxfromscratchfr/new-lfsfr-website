---
layout: post
title: LFS 7.10 est publiée
date: 2016-09-10
tags: LFS
---

Nous sommes heureux de vous annoncer la publication de la traduction de LFS version 7.10, dans ses versions SysV et Systemd. Cette version, sortie deux jours après la version anglaise, contient une mise à jour majeure de la chaîne d'outils avec les versions glibc-2.24, binutils-2.27 et gcc-6.2.0. Au total, 29 paquets se sont vus mis à jour et un énorme travail rédactionnel a été réalisé pour améliorer les textes tout au long du livre.

Il s'agit d'une publication coordonnée entre la version SysV et Systemd. Ces deux variantes ont été fusionnées dans la version anglaise pour améliorer la maintenance de la majorité du texte qui était en commun. C'est aussi désormais chose faite dans la version française.

Un grand merci à celles et ceux qui ont contribué à la traduction, en particulier à Amaury et Jean-Philippe. La sortie de cette version valide la méthode de traduction gettext qui permet de faire participer plus facilement les traducteurs éventuels.

N'hésitez pas à lire et télécharger le livre sur le site www.fr.linuxfromscratch.org aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
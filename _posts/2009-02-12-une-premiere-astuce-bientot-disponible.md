---
layout: post
title: Une première astuce bientôt disponible
date: 2009-02-12
tags: ASTU
---

Grâce à un de nos nouveaux membres, le projet des astuces est lancé. Une première astuce vient d'être traduite et est actuellement en cours de relecture. Elle sera disponible en lecture d'ici très peu de temps. D'autres pourraient venir bientôt.

Quoiqu'il en soit n'hésitez pas à nous proposer des traductions ou à en demander.

Merci infiniment à notre nouveau membre pour sa contribution, qui inaugure le projet et le fait décoler:  le voici lancé.
---
layout: post
title: BLFS évolue
date: 2011-12-05
tags: BLFS
---

Depuis quelques semaines, l'activité autour de BLFS s'est de nouveau intensifiée. Le livre se met à jour régulièrement, ce qui prend un temps considérable si on veut demeurer synchronisés.

Suite à des discussions sur la liste de diffusion, il a donc été décidé qu'un contributeur supplémentaire participerait activement au maintien à jour de ce projet. Plus qu'un coordinateur, blfs se trouve à présent suivie par une équipe de coordination, qui peut agir selon le temps de ses composants. Pour ce faire, une procédure a été mise en place.

Sa structure permet à tout nouveau contributeur d'intervenir à la hauteur de ses désirs. Soit il contribue et passe par un des membres de l'équipe de coordination pour que ses changements soient intégrés; soit il participe directement à la coordination en se voyant autorisé des droits d'écriture sur le dépôt svn du projet.

À l'appui de ce dispositif, blfs se den! d'une page de wiki, hébergée par l'association [Traduc.org](http://traduc.org).

Cette structure devrait, nous l'espérons, pérenniser la coordination de la traduction et améliorer sa réactivité. De cette façon, malgré l'ampleur de l'ouvrage, nous resterons synchronisés, sans qu'un temps disproportionné ne soit consacré par une personne au suivi particulier de ce projet.

Afin de tout savoir sur cette évolution, je vous propose le parcours suivant: allez d'abord sur la page Participez, qui vous amènera sur le wiki. Ce cheminement vous permettra de tout maîtriser. Au besoin, contactez le coordinateur, dont le mail de contact est dans la rubrique L'équipe.
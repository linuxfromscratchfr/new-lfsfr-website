---
layout: post
title: lfs représenté aux Rencontres Bretonnes du Logiciel Libre
date: 2010-05-15
tags: MAIN
---

Le 15 et 16 mai se tiennent à Rennes les Rencontres Bretonnes du Logiciel Libre au 18 rue des Plantes. L'association Traduc.org a trouvé le moyen de s'y rendre par son vice-président, qui est aussi le coordinateur de lfs. C'est pourquoi on peut dire que lfs sera représenté. Outre le stand Traduc, il y aura une conférence sur l'accessibilité offerte par le logiciel libre.

Prémice des RMLL à Bordeaux, cet événement vous permettra de rencontrer en Bretagne pour ceux qui le souhaitent et quc le peuvent des travailleurs solitaires... Alors n'hésitez pas à vous y rendre. Au surplus, aux RMLL, Traduc ne tiendra qu'une conférence et une table ronde, et non un stand. Donc l'approche sera différente.

Rennais et rennaises, bretons et bretonnes ou en transit en Bretagne qui veulent voir physiquement un traducteur de lfs et échanger avec lui, n'hésitez pas à nous rejoindre samedi et dimanche de 10H à 18H. Nous espérons vous rencontrer.
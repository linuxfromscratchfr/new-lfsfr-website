---
layout: post
title: Une nouvelle étape pour BLFS
date: 2012-11-08
tags: BLFS
---

BLFS (Beyond Linux From Scratch) est le livre qui explique la suite de la construction d'un système depuis 0. Il est possible de construire aussi bien des programmes pour transformer votre LFS en serveur que pour la transformer en machine de bureau.

Les auteurs de BLFS ont décidé il y a quelque temps qu'il n'y aurait plus de publication de BLFS, la version svn en perpétuelle évolution est
la version à consulter.

Cependant, une étape vient d'être atteinte.

2 mois après la publication de la dernière version stable de LFS ( version 7.2), l'ensemble des paquets (+ de 600) a pu être testé et adapté pour cette dernière version de LFS. Il est à  noter d'ailleurs que BLFS contient les dernières versions stables des principaux logiciels.

Une image de cette version de BLFS entièrement compatible avec LFS 7.2
est téléchargeable sur le site sur le site de LFS.

La version française de BLFS est traduite au jour le jour et est synchrone avec la version anglaise avec un retard allant de quelques dizaines de minutes à quelques jours (en fonction de la disponibilité des traducteurs et des modifications effectuées).

Afin d'offrir un service équivalent en français, nous avons également mis en ligne une image de la version de BLFS entièrement compatible avec
LFS 7.2.

Vous pouvez charger cette image ici.
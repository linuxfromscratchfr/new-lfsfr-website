---
layout: post
title: La méthode .po réussit son premier coup d'essai
date: 2014-12-04
tags: MAIN
---

La première traduction avec la méthode des fichiers .po vient d'être publiée dans le projet LFS: il s'agit de la version 7.6 Systemd.

En effet habituellement la traduction se fait directement dans le xml en lisant les fichiers diff de la version anglaise ce qui
effrayait beaucoup les nouveaux traducteurs. Cette nouvelle méthode devrait ouvrir des perspectives (logiciels d'édition de .po, de traduction assistée, etc).

Nous espérons donc que beaucoup de nouveaux traducteurs viendront prochainement nous rejoindre afin de nous aider à traduire les
différentes architectures de CLFS ainsi que BLFS-systemd.

Nous remercions le porteur de cette méthodologie, Amaury, dont le travail a été particulièrement précieux pour mettre au point la méthode. Une première étape est franchie et en augure d'autres que nous atteindrons progressivement. D'ores et déjà, nous savons que nous pouvons passer un livre en .po et le traduire pour générer du html et du PDF. C'est donc un bon premier coup d'essai.
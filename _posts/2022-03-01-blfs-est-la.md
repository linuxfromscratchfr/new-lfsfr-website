---
layout: post
title: BLFS 11.1 est là !
date: 2022-03-01
tags: BLFS
---

Nous sommes heureux de vous annoncer la sortie de BLFS 11.1 conjointement
en version SysV et Systemd.

Cette nouvelle version de BLFS contient environ 1000 paquets en plus du livre
Linux From Scratch de base version 11.1. Cette publication fait suite à la
mise à jour de plus de 900 paquets depuis la version précédente et un
énorme travail rédactionnel a été réalisé pour améliorer les textes et
la présentation tout au long du livre.

Un grand merci à celles et ceux qui ont contribué à la traduction et à la
relecture ! En particulier, nous avons bénéficié de l'aide encore cette
année des étudiants de Master traduction et interprétation de l'Université
de Rennes 2.

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur la liste de diffusion, le canal IRC
ou [mastodon](https://mamot.fr/@lfsfr).

Bonne lecture !

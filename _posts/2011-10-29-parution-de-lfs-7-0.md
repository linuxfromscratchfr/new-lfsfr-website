---
layout: post
title: Parution de LFS 7.0
date: 2011-10-29
tags: LFS
---

L'équipe de LFS-fr est heureuse de vous annoncer la publication de LFS version 67.0. Cette version contient de nombreux changements par rapport à celle 6.8, notamment s'agissant des versions de logiciels aussi importants que Binutils, le noyau Linux,, GCC et Glibc. Vous y trouverez aussi des corrections de sécurité.

Un travail rédactionnel a également été accompli sur les explications contenues dans l'ouvrage, qui a cherché à la fois à les clarifier et à les améliorer.

Enfin, cette version est marquée par un travail considérable sur les scripts de démarrage, entraînant une régénération des scripts et une réécriture du chapitre 7.

Nous tenions à remercier tous ceux qui ont participé à la traduction du livre. Leur soutien nous a été précieux et leur fidélité est particulièrement motivante.

N'hésitez pas à lire et télécharger le livre sur ce site aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
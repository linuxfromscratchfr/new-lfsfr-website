---
layout: post
title: Danger pour notre hébergeur de projet
date: 2009-12-06
tags: MAIN
---

Comme vous le savez, notamment en examinant la liste de nos partenaires, notre projet de traduction de LFS fait partie d'un ensemble, géré par l'association traduc.org. Elle nous offre un serveur pour héberger le site et le dépôt svn.

Cette association, ancienne et précurseur de bien des projets de traduction actuels comme le ldp ou autre, est aujourd'hui menacée de disparition. Son bureau et son Conseil d'Administration ont récemment annoncé sur leur liste de diffusion leur intention de démissionner lors de la prochaine assemblée générale du 16 décembre. Ils invoquent à l'appui de cette décision une absence de motivation.

L'enjeu est donc à présent double. D'une part, il faut du monde pour reconstituer un bureau et un Conseil d'Administration, soit minimum 3 à 6 personnes. Ensuite, il faudra revitaliser l'association, ce qui consiste surtout en la recherche de projets dont la traduction serait intéressante, la recherche de gens intéressés pour coordonner le travail de façon constante. Le reste des tâches tient principalement à la gestion d'une association: le bureau gère les problématiques d'hébergement (site, wiki, un serveur subversion, mail, liste de diffusion) et un petit compte bancaire pour financer le tout. La répartition du travail aboutit à ce que chacun devrait avoir une tâche raisonnable à sa charge et conforme à ses affinités, ses disponibilités etc.

Le problème est donc un manque de moyens humains: l'association aurait besoin de personnes qui, sans fournir un immense travail, pe répartissent les travaux et soient motivées pour travailler sur la durée et en accomplissant régulièrement leur tâche. En tout cas il serait fort dommageable que cette association, historiquement symbolique et dont le travail est utile, disparaisse faute de repreneurs. Inutile d'ajouter que le projet de traduction lfs devrait, au surplus, trouver un autre hébergeur en urgence.

Nous espérons en conséquence que l'association trouvera les ressources humaines pour survivre, car son poids historique reste important dans le monde du libre. L'assemblée générale du 16 décembre sera déterminante. L'association lance un appel à tous les volontaires, tant sur traduc.org, sur les listes et sur leur canal irc où se tiendra l'assemblée générale à 20H30 (#traduc sur irc.oftc.net).

À suivre. N'hésitez pas à répondre présent.
---
layout: post
title: Nouvel épisode du feuilleton systemd
date: 2013-12-23
tags: LFS
---

Un développeur avait commencé le développement d'une branche spécifique du livre LFS validée par le projet mais non officielle. Cette version, développée par Armin, reprend le livre original et l'adapte avec systemd au lieu de sysvinit.

En septembre, Armin a pris ses distances par rapport au projet LFS, pour des raisons tant personnelles que relationnelles. Le projet systemd s'est ainsi arrêté et, début décembre, Bruce, mainteneur principal de LFS avec Matthiew, a décidé de supprimer la branche.

Mais un nouvel épisode a été amorcé quelques jours plus tard. Armin, contre toute attente, a rallié de nouveau le projet et proposé de poursuivre son travail sur Systemd. L'équipe ayant accepté, la branche a été rétablie. Quelques semaines plus tard, soit le 18 décembre, Armin annonçait que LFS systemd était abouti et synchronisé avec la version svn de LFS. Il invite la liste à tester et s'engage à assurer la mise à jour d la branche synchronisée avec celle du tronc.

Deux livres parallèles sont donc lisibles à ce jour: le tronc avec sysvinit et la branche systemd, distincte du tronc par ce paquet et ses conséquences, visibles principalement au chapitre 7.

L'équipe de traduction préfère pour le moment s'axer sur les versions officielles, stables et publiées. C'est pourquoi nous n'envisageons pas de traduire la branche. Si des gens sont tentés par l'idée de le faire, qu'ils n'hésitent pas à nous contacter. Leur contribution sera étudiée avec le plus grand intérêt.
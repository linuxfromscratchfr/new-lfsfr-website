---
layout: post
title: Des astuces encore et encore
date: 2009-04-30
tags: ASTU
---

Une nouvelle astuce sur logrotate vient d'être traduite. Merci à son traducteur pour son travail, qui a au surplus le mérite de la qualité.

Nous en sommes ainsi à la troisième traduction. L'équipe du projet est ravie de ces contributions. Ce sont elles dont on a besoin pour venir à bout d'un tel projet. N'hésitez pas.

Tout commentaire est par ailleurs bienvenu, notamment pour mettre à jour ou réagir suite aux astuces.
---
layout: post
title: LFS 9.0 est disponible
date: 2019-09-01
tags: LFS
---

Nous sommes heureux de vous annoncer la sortie de LFS 9.0 conjointement en
version SysV et Systemd, ainsi que la sortie de BLFS 9.0 dans ces deux
versions.

Cette nouvelle version contient une mise à jour majeure de la chaîne d’outils
avec les versions glibc-2.30 et gcc-9.2.0. Au total, ce sont 33 paquets qui ont
été mis à jour. Un énorme travail rédactionnel a été réalisé pour améliorer les
textes tout au long du livre. Enfin, le noyau Linux a été mis à jour vers la
version 5.2.8.

Un grand merci à celles et ceux qui ont contribué à la traduction et aux
relectures !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur la liste de diffusion, le canal IRC
ou mastodon.

Bonne lecture !

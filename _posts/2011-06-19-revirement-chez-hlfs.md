---
layout: post
title: Revirement chez hlfs
date: 2011-06-19
tags: HLFS
---

Depuis quelque temps, le livre hlfs était très tranquille. Il semble que la décision prise par son mainteneur d'origine de tout passer au format txt, tout en mettant à jour l'ouvrage, a exigé un trop lourd travail.

L'activité du projet devenait donc faible. Du coup, la traduction s'est elle-même arrêtée, car tout devenait compliqué et précaire.

Depuis novembre, l'équipe a repris la version 6.7 de LFS et le format xml, base du livre hlfs. C'est cette découverte qui a poussé l'équipe française à resynchroniser l'ouvrage français et anglais.

Nous pouvons ainsi vous annoncer que les deux livres sont à nouveau synchronisés et relativement à jour. Leur lecture est donc très intéressante et la synchronisation habituelle reprend.

Bonne lecture,
---
layout: post
title: BLFS 12.1 est là !
date: 2024-03-01
tags: BLFS
---

Nous sommes heureux de vous annoncer la sortie de BLFS 12.1 conjointement
en version SysV et Systemd.

Cette nouvelle version de BLFS contient environ 1000 paquets en plus du livre
Linux From Scratch de base version 12.1.

Parmi les changements dans BLFS, on trouve l'ajout de Qt6, sysmon-qt, xdg-desktop-portal,
simple-scan, snapshot, wireplumber, power-profiles-daemon et de nombreux autres
paquets auxiliaires.  Cette version ajoute également les paquets SPIRV et Vulkan
pour prendre en charge les pilotes Vulkan de mesa.  Consultez les notes de
changement du livre pour trouver la liste complète des changements.

Avertissement d'obsolescence : les futures versions de BLFS retireront la bibliothèque
GTK2 ainsi que Python2, qui ne sont plus maintenus.

Un grand merci à celles et ceux qui ont contribué à la traduction et à la
relecture !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur le canal IRC, le
[dépôt gitlab](https://framagit.org/linuxfromscratchfr/new-lfsfr-website/)
ou [mastodon](https://mamot.fr/@lfsfr).

Bonne lecture !

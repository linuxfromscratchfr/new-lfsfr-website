---
layout: post
title: Le projet a un leader à lui
date: 2012-06-09
tags: BLFS
---

C'est avec une joie immense que le coordinateur du projet lfs-fr peut vous annoncer l'entrée en scène de Denis Mugnier pour coordonner le projet blfs-fr. Son aide a été depuis le départ capitale et je tenais à l'en remercier chaleureusement. Depuis quelques mois, il assume l'essentiel des mises à jour de la traduction du livre blfs, dans sa version svn. Il a mis en place les outils qui permettent d'affronter sereinement l'immensité du travail que représente la traduction de blfs. Dès lors, il devient un support technique possible et une interface incontournable entre blfs-fr et blfs.

C'est donc tout naturellement qu'il s'impose comme leader du projet. Tout en restant dans le projet global lfs-fr, c'est avec confiance et joie que je lui laisse gérer ce projet. Je ne m'en éloigne pour autant pas, mais je ne le suivrai qu'à titre subsidiaire, qout en contribuant autant que possible. Je l'aiderai notamment s'il s'agit de communiquer sur le site, mais il se chargera lui-même des relations avec blfs qu'il conduit avec brillo! Cette aide me permettra de maintenir le cap des autres projets: clfsfr, lfsfr et les astuces.

Un grand merci à lui pour son travail considérable et sa contribution inestimable, et pour avoir accepté cette responsabilité. Allégeant mon travail,cette évolution me motive encore plus.
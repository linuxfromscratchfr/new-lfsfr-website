---
layout: post
title: LFS aux JDLL
date: 2012-11-17
tags: MAIN
---

Pour la première fois, le projet Linux From Scratch francophone sort de sa coquille. Il se présente, publiquement, aux Journées du Logiciel Libre à Lyon. Nous proposons une réflexion sur sa destination "geek" ou pas, le dimanche 18 novembre.

N'hésitez pas à venir nous rencontrer et, par là même, découvrir l'association Traduc.org qui héberge ce projet en français! Nous serons ravis d'échanger avec vous sur ces thèmes.
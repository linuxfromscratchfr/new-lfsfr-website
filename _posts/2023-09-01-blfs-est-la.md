---
layout: post
title: BLFS 12.0 est là !
date: 2023-09-01
tags: BLFS
---

Nous sommes heureux de vous annoncer la sortie de BLFS 12.0 conjointement
en version SysV et Systemd.


The BLFS version includes approximately 1000 packages beyond the base
Linux From Scratch Version 11.2 book. This release has 1543 updates
from the previous BLFS version including package updates and numerous text and 
formatting changes.

New in this version of BLFS is the LXQt Desktop Environment
and an alternative method of building Qt with lighter
dependency requirements than the full Qt package.

DEPRECATION NOTICE: Future versions of BLFS will remove
the unmaintained LXDE Desktop Environment and support for
the Reiser file system (reiserfsprogs).



Cette nouvelle version de BLFS contient environ 1000 paquets en plus du livre
Linux From Scratch de base version 12.0. Cette publication fait suite à la
mise à jour de 1543 paquets depuis la version précédente et un
énorme travail rédactionnel a été réalisé pour améliorer les textes et
la présentation tout au long du livre.

Parmi les autres changements dans BLFS, on trouve l'ajout de l'environnement
de bureau LXQt et d'une méthode de construction de Qt alternative qui a moins
de dépendances requises que le paquet Qt complet.

Avertissement d'obsolescence : les futures versions de BLFS supprimeront
l'environnement de bureau LXDE qui n'est plus maintenu ainsi que la prise en
charge du système de fichiers Reiser (reiserfsprogs).

Un grand merci à celles et ceux qui ont contribué à la traduction et à la
relecture !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur le canal IRC, le
[dépôt gitlab](https://framagit.org/linuxfromscratchfr/new-lfsfr-website/)
ou [mastodon](https://mamot.fr/@lfsfr).

Bonne lecture !

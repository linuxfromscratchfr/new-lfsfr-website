---
layout: post
title: Première astuce disponible
date: 2009-03-08
tags: ASTU
---

Nous vous l'annoncions, c'est chose faite. La première astuce a été relue et mise en ligne. Bien qu'elle ne soit disponible qu'en lecture, il est très facile de la télécharger puisqu'il s'agit d'un fichier texte. N'hésitez donc pas.

Elle porte sur le paramétrage d'un démarrage à la mode BSD/Slackware. Merci à son traducteur qui l'a proposée. Le temps de relecture a été plus long que prévu, mais nous y sommes arrivés.

Une prochaine arrive. Elle doit également être relue. N'hésitez pas à consulter la rubrique lecture pour voir les nouvelles astuces au fur et à mesure.
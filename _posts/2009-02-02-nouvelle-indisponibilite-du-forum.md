---
layout: post
title: Nouvelle indisponibilité du forum
date: 2009-02-02
tags: MAIN
---

Voilà que l'hébergeur du forum absolinux s'y met. Il rencontre de gros soucis techniques. Par  conséquent, nous ne pouvons pas utiliser le forum pendant quelques jours.

N'hésitez pas à passer par l'irc et les listes de diffusion en attendant. Nous espérons dans les meilleurs délais rétablir avec l'hébergeur d'absolinux une meilleure relation.
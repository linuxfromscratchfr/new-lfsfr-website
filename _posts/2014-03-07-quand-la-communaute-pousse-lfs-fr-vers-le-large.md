---
layout: post
title: Quand la communauté pousse LFS-FR vers le large
date: 2014-03-07
tags: LFS
---

A la demande générale et sous l'impulsion d'un contributeur, l'équipe LFS a décidé de se lancer dans la traduction de la branche Systemd du livre. Ce projet consiste en l'ajustement de la version stable actuelle avec celle systemd. Il ne s'agit donc pas de tout retraduire, mais d'adapter.

Rassurez-vous, la version actuelle, maintenue en amont, continue sa vie. Mais vous pouvez désormais aider sur les deux branches du livre Linux From Scratch. Pour savoir ce que cela représente et comment participer au projet, rendez-vous sur http://traduc.org/lfsfr-systemd

Il est recommandé de privilégier la liste de diffusion pour communiquer avec nous.
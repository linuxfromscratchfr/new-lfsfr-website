---
layout: post
title: Réajustement de lfs svn
date: 2009-01-17
tags: LFS
---

Comme promis la relecture des codes (commandes) suggérés par la version svn anglaise a  été faite dans la traduction. Elle contenait moins d'erreurs  mais il a fallu en corriger.

À présent, la 6.4 comme la svn anglaise contiennent rigoureusement les mêmes commandes, plus aucun risque d'erreur. N'hésitez pas à nous signaler toute autre erreur si vous en voyez. Nous les corrigerons. Le plus important a  en tout cas été  corrigé: les commandes.
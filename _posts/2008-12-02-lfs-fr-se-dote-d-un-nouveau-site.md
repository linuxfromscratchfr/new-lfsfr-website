---
layout: post
title: lfs-fr se dote d'un nouveau site
date: 2008-12-02
tags: MAIN
---

L'équipe de traduction de LFS est heureuse de vous annoncer la sortie de son nouveau site hébergeant les projets traduits. On l'a promis depuis longtemps, c'est chose faite.

Ce nouveau site n'a aucune ambition graphique. Il s'inspire fortement du modèle de nos amis anglophones. Son objectif majeur est davantage d'améliorer son hergonomie afin de mieux trouver les informations relatives à chaque projet. L'équipe en a profité pour mettre à jour des informations devenues obsolètes ou inusitées.

N'hésitez pas à nous donner votre opinion. Nous espérons que vous y trouverez une information plus exacte et précise, de manière plus rapide et liée spécifiquement au(x) projet(s) que vous suivez.

Merci en tout cas au développeur de ce site pour sa contribution indispensable.
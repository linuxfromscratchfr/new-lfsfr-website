---
layout: post
title: CLFS 2.1.0 enfin publié
date: 2013-12-14
tags: CLFS
---

Cross-lfs permet de construire des systèmes plus complexes que LFS, en se basant néanmoins sur ce livre. Il propose par exemple de construire un système multilib 32-64bits, ou de construire, à partir de n'importe quelle architecture, un système utilisable sur sparc, mips, ppc, etc.

Pour cibler le maximum d'utilisateurs, lfs-fr a décidé de ne traduire que les livres 64bits (non supportés par LFS à l'époque) et multilib. À ce jour la traduction multilib est partielle mais le livre est utilisable.

Notez enfin que CLFS exige d'avoir une expérience LFS, le support étant moins pédagogiquement abouti.

L'équipe de traduction annonce la publication de la traduction pour les livres 64bits et multilib, sachant qu'en multilib, le travail est en cours et partiellement réalisé (notamment, pas relu).

Nous sommes donc heureux de vous proposer CLFS 2.1 en français pour 64bits et multilib.  Cette version inclut des modifications pédagogiques, techniques et des mises à jour.

N'hésitez pas pour tout retour sur ce projet!

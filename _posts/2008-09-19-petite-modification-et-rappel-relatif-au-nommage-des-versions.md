---
layout: post
title: Petite modification et rappel relatif au nommage des versions
date: 2008-09-19
tags: LFS
---

Dans un double souci d'anticipation des futures évolutions et de clarification suite à des confusions remarquées lors d'échanges privés, voici quelques explications sur le nommage des versions.

Sauf pour HLFS, les autres projets ont une version stable et une version de développement. LFS appelle utilise svn pour leurs versions de développement. Ils numérotent leurs livres stables.

Ici, nous utilisons CVS pour nos versions non parfaites des traductions. Il est donc possible qu'il existe ici une version cvs de la traduction de la svn anglaise.

Une version svn anglaise traduite correctement peut être mise en ligne ici. Jusque-là, nous l'appelions svn. À présent, nous utiliserons la même nomination que LFS. Pour le livre lfs, vous pouvez lire et télécharger, par exemple, la version SVN-20080711. Cela sera plus facile pour les futures versions svn, permettant éventuellement de conserver une version plus vieille de développement anglaise au besoin.

Il en sera de même pour les autres projets. Même HLFS, une fois en ligne définitivement, verra son nom SVN-annéemoisjour.

Pour l'heure, sachez que la traduction de la svn anglaise s'appelle à présent SVN-20080711.
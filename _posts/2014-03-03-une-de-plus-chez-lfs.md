---
layout: post
title: Une de plus chez LFS
date: 2014-03-03
tags: LFS
---

L'équipe de LFS-fr est heureuse de vous annoncer la publication de LFS version 7.5, accompagné de sa traduction disponible une heure après la publication anglaise. Cette version contient de nombreux changements par rapport à celle 7.4, notamment s'agissant des versions de logiciels aussi importants que Binutils, le noyau Linux,, GCC et Glibc. Vous y trouverez aussi des corrections de sécurité. La traduction, quant à elle, capitalise les relectures faites depuis la dernière version.

Un travail rédactionnel a également été accompli sur les explications contenues dans l'ouvrage, qui a cherché à la fois à les clarifier et à les améliorer. Côté mise en forme, les feuilles de style ont été modifiées pour se conformer au nouveau format Docbook (nouvelle version) et poser un cadre pour rendre une version Ebook faisable.

N'hésitez pas à lire et télécharger le livre sur ce site aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
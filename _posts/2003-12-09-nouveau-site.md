---
layout: post
title: Nouveau site
date: 2003-12-09
tags: MAIN
---

Je viens de mettre en ligne la nouvelle version du site de traduction de LFS et BLFS. N'hésitez pas nous dire ce que vous en pensez. TNT a disparu car il apportait plus de désagrément qu'autre chose. De plus, les archives PDF et PS sont de nouveau générées.
---
layout: post
title: Naissance d'un nouveau projet
date: 2008-09-16
tags: MAIN
---

Un de nos contributeurs développe des astuces en anglais. Il a accepté d'en sortir une version française parallèle. Cela permet l'ouverture d'un sous-projet Astuces, qui contiendra les astuces de notre contributeur et qui s'efforcera, à terme, de traduire les astuces en anglais existantes dans le projet LFS.

Ce nouveau projet, ambitieux, est à longue échéance et il reste à déterminer quoi traduire exactement. Vos demandes peuvent nous aider à fixer des priorités.

N'hésitez donc pas. Ce projet sort à peine de l'oeuf.
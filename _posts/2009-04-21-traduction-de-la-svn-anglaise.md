---
layout: post
title: Traduction de la svn anglaise
date: 2009-04-21
tags: CLFS
---

C'était un objectif de longue haleine, le voilà atteint. La traduction de la svn vient d'être achevée. Reste le débogage xml et la relecture. Mais le livre x86_64-64 (pure64) est traduit. N'hésitez pas à nous aider à finir ce qu'il reste.

Prochainement, il est prévu de traduire le livre multilib. Une fois le premier livre relu, cette extension sera très facile. Du coup, on envisage presque de traduire les autres livres, l'extension n'étant pas aussi fastidieuse vu l'organisation de clfs.

N'hésitez donc pas à nous rejoindre, plus on est plus on aura vite fini et on pourra étoffer.
---
layout: post
title: Les dernières nouvelles en amont
date: 2013-11-24
tags: LFS
---

Notons la mise à jour cette semaine de Check, Bison, Tar, Iproute2 et Linux.

Pour revenir en arrière, un projet de développement du livre vers Systemd avait été entamé et porté par un auteur. Son départ est synonyme de la suspension du développement de cette branche expérimentale.
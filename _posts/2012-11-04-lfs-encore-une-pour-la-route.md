---
layout: post
title: LFS, encore une pour la route!
date: 2012-11-04
tags: LFS
---

L’équipe de LFS-fr est heureuse de vous annoncer la publication de LFS version 7.2. Cette version contient de nombreux changements par rapport à la 6.8, notamment s’agissant des versions de logiciels aussi importants que Binutils, le noyau Linux, GCC et glibc. Vous y trouverez aussi des corrections de sécurité.

Pour la partie francophone, un travail important de relecture a été entamé. S’il reste inachevé, il n’en a pas moins amélioré considérablement la fiabilité de l’ouvrage.

Un travail rédactionnel a également été accompli sur les explications contenues dans l’ouvrage, qui a cherché à la fois à les clarifier et à les améliorer. Des compléments ont également été insérés sur les technologies modernes de partitionnement. On notera enfin la poursuite du travail sur les scripts de démarrage.

Nous tenions à remercier tous ceux qui ont participé à la traduction du livre. Leur soutien nous a été précieux et leur fidélité est particulièrement motivante.

N’hésitez pas à lire et télécharger le livre sur ce site, aux formats HTML ou PDF, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.

Enfin, une version papier est toujours demandable auprès d’Accelibreinfo.
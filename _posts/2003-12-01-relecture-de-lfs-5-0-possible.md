---
layout: post
type: post
title: Relecture de LFS-5.0 possible
date: 2003-12-01
tags: LFS
---

Même si LFS-4.1 n'est pas entièrement relu, il est possible de commencer la relecture de LFS-5.0. Comme il reste peu de fichiers à relire pour LFS-4.1 et qu'en plus ces fichiers sont déjà réservés, nous n'abandonnons pas cette relecture.

---
layout: post
title: BLFS, ça vit vraiment?
date: 2010-08-15
tags: BLFS
---

Malgré la rareté des nouvelles, oui BLFS vit. Les adeptes de la liste de diffusion le savent, quelques commits svn leur sont envoyés. Ce travail reste cependant davantage un fil rouge qu'un vrai objectif immédiat. Mais que raconter régulièrement à son sujet? Aussi paraît-il opportun de faire aujourd'hui un petit état des lieux sur ce qui est fait, à faire et la méthode.

La méthode est "fimple". Elle a consisté à télécharger, il y a quelques mois, la dernière révision du livre (alors r7863). Puis, on applique un sed sur des expressions qui reviennent pour accélérer les choses et en combinant cette approche à des scripts Perl, on obtient un livre plus ou moins traduit. On traduit dans l'ordre alphabétique des répertoires et des fichiers. Sitôt un fichier traduit, on l'envoie vers la copie locale du dépôt svn et on commit. Le seul ioconvénient est que le dépôt et la copie locale jouissent assez peu de sed et des scripts Perl, mais là n'est pas l'objectif. Un dialogue entre contributeurs et traducteurs peut résoudre aisément ce problème.

Du coup, certaines traductions ont été faites. Une petite partie du livre est complètement traduite, une partie plus importante l'est partiellement. Les dossiers de a à "general" sont entièrement traduits. Le travail avance doucement, mais il progresse.

Que reste-t-il à faire? D'abord, terminer la traduction. Ensuite, appliquer un diff -ru entre la VO (r7863) et la version actuelle anglaise et répercuter les mises à jour. Si ce travail semble énorme, il présente l'avantage de ne pas poser de problèmes d'indentation, balises xml... Il semble donc à portée humaine.

Voilà donc où nous en sommes. Difficile de donner un délai de fin du travail. Les développeurs d'origine ont déjà abandonné l'idée de le stabiliser un jour vu la densité. La traduction ne saurait aller plus vite. Mais nous y croyons. N'hésitez donc pas à nous rejoindre et à observer régulièrement l'avancement du travail. Pour l'heure, l'ennui est que le livre en français est ingénérable en html/pdf à cause de problèmes xml (on part de la version 6.0 et on la porte à une version récente où les règles xml ont bougé). Mais... vous pouvez observer le chantier.

Nous essaierons d'ici la fin de reposter des nouvelles ici (plus régulièrement que ce qui a été fait jusque-là). Le meilleur fil d'actualité reste quand même la liste de diffusion.
---
layout: post
title: BLFS 8.3 est là !
date: 2018-09-02
tags: BLFS
---

Nous sommes heureux de vous annoncer la sortie de BLFS 8.3 conjointement en version SysV et Systemd.

Cette nouvelle version de BLFS contient environ 1000 paquets en plus du livre Linux From Scratch de base version 8.3. Cette publication fait suite à la mise à jour d'environ 700 paquets depuis la version précédente et un énorme travail rédactionnel a été réalisé pour améliorer les textes et la présentation tout au long du livre.

Cette version marque le retour du format pdf disponible au [téléchargement](/blfs/telecharger/), ainsi que l'arrivée du format epub, spécifiques à la version française.

Un grand merci à celles et ceux qui ont contribué à la traduction et à la relecture !

N'hésitez pas à lire et télécharger le livre sur le site [fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou epub, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.

Bonne lecture !

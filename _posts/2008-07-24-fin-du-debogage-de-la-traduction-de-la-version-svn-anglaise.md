---
layout: post
title: Fin du débogage de la traduction de la version svn anglaise
date: 2008-07-24
tags: LFS
---

Grâce à un travail remarquable de Guillaume, que je remercie de son soutien bien qu'il ait dû abandonner le projet de traduction LFS, la traduction de la version svn anglaise est déboguée. Elle se compile désormais en un document html Toutefois, nous ne la mettons pas en ligne, car elle comporte encore des erreurs de traduction. Une relecture doit être effectuée. N'hésitez pas à nous y aider. Plus on sera nombreux, plus on sera efficace. En attendant, elle est sur le CVS du projet.
---
layout: post
title: LFS 7.7 est là
date: 2015-03-07
tags: LFS
---

L'équipe de LFS-fr est heureuse de vous annoncer la publication de LFS version 7.7, accompagné de sa traduction disponible le jour même. Cette version contient de nombreux changements par rapport à celle 7.6, notamment s'agissant des versions de logiciels aussi importants que Binutils, le noyau Linux,, GCC et Glibc. Vous y trouverez aussi des corrections de sécurité. La traduction, quant à elle, capitalise les relectures faites depuis la dernière version.

Un travail rédactionnel a également été accompli sur les explications contenues dans l'ouvrage, qui a cherché à la fois à les clarifier et à les améliorer.


N'hésitez pas à lire et télécharger le livre sur le site www.fr.linuxfromscratch.org aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
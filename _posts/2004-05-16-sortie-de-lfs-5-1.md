---
layout: post
title: Sortie de LFS-5.1
date: 2004-05-16
tags: LFS
---

Matthew Burgess vient d'annoncer la disponibilité de LFS-5.1. Sa traduction
devrait bientôt démarrer... dès la fin de celle de BLFS-5.0.
---
layout: post
title: Le projet a déménagé
date: 2014-11-25
tags: MAIN
---

Nous vous l'annoncions depuis la rentrée, c'est désormais chose faite: la traduction du projet Linux From Scratch a déménagé! Elle a quitté son gentil hébergeur traditionnel, Traduc.org, après dix ans de collaboration constructive et riche, pour rejoindre ses origines. La traduction est désormais hébergée directement sur les serveurs de LFS. A la clé, une intégration accrue pour un meilleur support, un travail d'équipe renforcé, une dépendance du projet source et non de l'association qui le traduit.

Concrètement pour chacun, cette migration signifie que les adresses Web changent. Pour le site, allez désormais sur www.fr.linuxfromscratch.org qui sera le seul officiel et à jour. Pour le svn, c'est svn://svn.linuxfromscratch.org/fr-lfs

Tous les traducteurs sont invités à modifier les adresses de référence au projet francophone en conséquence. Tous les utilisateurs et lecteurs sont invités à mettre à jour leurs marque-pages.

Nous sommes en tout cas fiers de rejoindre la grande famille au plus proche à l'heure où elle vit des instants clés, liés à Systemd et après l'épisode clfs. Nous sommes la synthèse de toute cette histoire et espérons poursuivre avec l'équipe nos échanges.

N'hésitez pas à commenter, diffuser la nouvelle autour de vous!
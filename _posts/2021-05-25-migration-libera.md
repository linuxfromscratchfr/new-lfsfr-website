---
layout: post
title: Le canal IRC migre vers libera.chat
date: 2021-05-25
tags: MAIN
---

Le réseau IRC Freenode a récemment été déserté par ses administrateurs qui
ont fondé un nouveau réseau, suite à un conflit avec le propriétaire du nom
de domaine de Freenode. Le projet LFS a décidé de suivre les administrateurs
de Freenode sur leur nouveau projet, plutôt que rester sur Freenode et la
nouvelle administration. Nous avons suivi le mouvement.

Nous nous retrouverons donc désormais sur le canal #lfs-fr sur
[irc.libera.chat](https://libera.chat) (utilisez TLS et le port 6697).

À bientôt !

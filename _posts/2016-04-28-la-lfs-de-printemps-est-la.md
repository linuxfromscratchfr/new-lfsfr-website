---
layout: post
title: La LFS de printemps est là!
date: 2016-04-28
tags: LFS
---

Nous sommes heureux de vous annoncer la publication de la traduction de
LFS version 7.9, dans sa version basée sur Systemd. Cette
version contient de
nombreux changements par rapport à celle 7.8, notamment s'agissant des
versions de logiciels aussi importants que Binutils, le noyau Linux,,
GCC et Glibc. Vous y trouverez aussi des corrections de sécurité. La
traduction, quant à elle, capitalise les relectures faites depuis la
dernière version.

Un travail rédactionnel a également été accompli sur les explications
contenues dans l'ouvrage, qui a cherché à la fois à les clarifier et à
les améliorer.

Un grand merci à celles et ceux qui ont contribué à la traduction, en
particulier Amaury. Cette version aura permis de tester encore et de peaufiner la méthode gettext, en nous essayant à pottle.

N'hésitez pas à lire et télécharger le livre sur le site
www.fr.linuxfromscratch.org aux formats html ou pdf, puis à nous
adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
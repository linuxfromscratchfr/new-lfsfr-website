---
layout: post
title: Linux From Scratch 7.6 version Systemd traduite en français
date: 2014-12-04
tags: LFS
---

L'équipe de traduction francophone de Linux From Scratch est fière de vous annoncer que la version 7.6 Systemd de lfs est désormais complètement
traduite en français !

Systemd étant assez différent de Sysvinit cette version contient de nombreuses adaptations pour la configuration du système qui vous permettront en plus de comprendre le fonctionnement d'un système Linux, de comprendre les bases du fonctionnement et de la configuration d'un
système basé sur Systemd.

Nous remercions tous les contributeurs ayant participé à ce chantier, et en particulier Amaury, son coordinateur, qui a accompli un travail remarquable en un temps assez court. Ce travail résulte, malgré la brièveté du délai publication anglaise-traduction, d'un long processus dont nous sommes heureux de voir l'aboutissement ujourd'hui avec cette version publiée.

N'hésitez pas à nous adresser tous retours sur cette traduction et à venir demander de l'aide, sur la liste de diffusion, le forum ou le salon IRC.
---
layout: post
title: Une avancée majeure du projet
date: 2011-03-06
tags: BLFS
---

Nous sommes particulièrement heureux de vous annoncer qu'après deux ans et demi de travail actif, la traduction de blfs a été partiellement terminée. Pourquoi partiellement? Parce que c'est celle de la révision svn n°7863 qui a été terminée. Reste actuellement à la mettre à jour. Mais l'ampleur du travail est sans commune mesure avec ce qui a été accompli depuis la reprise du projet et nous pouvons espérer définitivement nous synchroniser sur le projet anglophone d'ici l'été.

Cette étape est cruciale pour le projet francophone lfs. Pour ce qui est de blfs, de nouvelles perspectives et possibilités d'aide s'ouvrent à vous. Les détails figureront dans la rubrique Participez..

Mais d'ores et déjà, voici un petit catalogue de mise en bouche. Tout d'abord, reste à analyser un fichier txt, pour y déceler les différences qu'il indique entre la VO actuelle et celle sur laquelle nous nous sommes appuyés, pour ensuite les répercuter sur la traduction. De plus, vous pouvez relire la traduction pour la corriger. Ensuite, un débogage xml peut commencer utilement pour, à terme, générer le html de blfs, voire le pdf. Enfin, le livre peut être amendé pour être rendu plus spécifique au public visé: compiler openoffice.org en français, firefox, seamonkey... en rajoutant les bonnes options et en amendant de ce fait le livre pour les francophones.

Bref, vous l'aurez compris, cette étape marque un aboutissement et ouvre des tâches nouvelles, plus variées et où davantage de personnes peuvent s'investir. N'hésitez donc pas.
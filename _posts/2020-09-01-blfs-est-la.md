---
layout: post
title: BLFS 10.0 est là !
date: 2020-09-01
tags: BLFS
---

Nous sommes heureux de vous annoncer la sortie de BLFS 10.0 conjointement
en version SysV et Systemd.

Cette nouvelle version de BLFS contient environ 1000 paquets en plus du livre
Linux From Scratch de base version 10.0. Cette publication fait suite à la
mise à jour de plus de 840 paquets depuis la version précédente et un
énorme travail rédactionnel a été réalisé pour améliorer les textes et
la présentation tout au long du livre.

Un grand merci à celles et ceux qui ont contribué à la traduction et à la
relecture !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur la liste de diffusion, le canal IRC
ou [mastodon](https://mamot.fr/@lfsfr).

Bonne lecture !

---
layout: post
title: Arrêt de la traduction de BLFS
date: 2005-10-01
tags: BLFS
---

Le mercredi 24 août de cette année, j'ai envoyé un message sur la liste de discussion lfs-traducfr pour annoncer que je cherchais un coordinateur pour BLFS, voire même pour LFS. Je n'ai malheureusement plus assez de temps pour m'occuper de tout mes projets. Bref, si vous voulez en savoir plus sur mes motivations, allez lire ce billet de mon blog.

Pour être franc, personne n'a encore souhaité reprendre ce poste. Je
poste donc cette nouvelle simplement pour vous avertir que la traduction de
BLFS-6.0, et encore plus celle de la 6.1, est tout simplement laissée en attente
d'une bonne volonté, que ce soit moi si ça me reprend ou quelqu'un d'autre.

Si vous êtes intéressé pour reprendre cette activité passionnante et
enrichissante, n'hésitez pas à me contacter sur mon compte gmail
(guillaume POINT lelarge CHEZ gmail POINT com).
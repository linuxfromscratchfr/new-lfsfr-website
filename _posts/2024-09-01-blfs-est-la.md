---
layout: post
title: BLFS 12.2 est là !
date: 2024-09-01
tags: BLFS
---

Nous sommes heureux de vous annoncer la sortie de BLFS 12.2 conjointement
en version SysV et Systemd.

Cette nouvelle version de BLFS contient environ 1000 paquets en plus du livre
Linux From Scratch de base version 12.2.

L'un des changements les plus significatifs dans BLFS a été la mise à jour de
KDE5 (Frameworks, Gear, Plasma) vers KDE6.  Des nouveaux paquets notables ont
fait leur apparition dans le livre, comme FreeRDP, gnome-connections et
les logiciels dolphin et konversation de KDE.  En tout, 32 autres paquets ont
été ajoutés pour prendre en charge d'autres paquets déjà présents dans le livre.
De plus, 21 paquets non maintenus ont été retirés.  Cela comprend notamment
Python2 et GTK2 et les paquets qui n'étaient plus mis à jour vers des versions
plus récentes.

En tout, ce sont plus de 925 tickets qui ont été fermés à travers plus de 1750
commits pour améliorer le livre.

Consultez [le journal des modifications](//fr.linuxfromscratch.org/view/blfs-12.2-fr/introduction/changelog.html)
pour voir la liste complète de tous les changements depuis la version précédente.

Avertissement d'obsolescence : les futures versions de BLFS retireront qt5.

Un grand merci à celles et ceux qui ont contribué à la traduction et à la
relecture !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur le canal IRC, le
[dépôt gitlab](https://framagit.org/linuxfromscratchfr/new-lfsfr-website/)
ou [mastodon](https://mamot.fr/@lfsfr).

Bonne lecture !

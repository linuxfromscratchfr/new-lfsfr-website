---
layout: post
title: Corrections du PDF de la traduction de LFS 6.5
date: 2009-11-22
tags: LFS
---

Grâce à vos retours, notamment sur notre canal irc #lfs-fr (irc.linuxfromscratch.org), nous avons pris connaissance de nombreuses erreurs sur le pdf de la traduction de LFS-6.5. Outre quelques coquilles habituelles, nous avons surtout été informés d'erreurs dues à la génération (des phrases cachées par un bloc commandes...).

Dans le souci que le pdf reste cohérent, nous en avons généré une nouvelle version téléchargeable dès à présent. Y subsistent encore des coquilles que nous corrigerons dans les meilleurs délais mais qui, contrairement aux problèmes rencontrés jusqu'ici, n'altèrent pas la compréhension de l'ouvrage ni sa justesse. Nous considérons cette correction comme importante et, de ce fait, invitons ceux qui ont déjà la version 6.5 en PDF à la télécharger à nouveau. Les futures corrections s'annoncent nombreuses mais moins fondamentales que celles faites aujourd'hui.

Bonne lecture à tous.
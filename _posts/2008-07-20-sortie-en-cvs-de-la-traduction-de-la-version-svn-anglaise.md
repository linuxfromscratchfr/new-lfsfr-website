---
layout: post
title: Sortie en CVS de la traduction de la version SVN anglaise
date: 2008-07-20
tags: LFS
---

Sur le dépôt CVS du site, vous pouvez dès à présent télécharger la version CVS de la traduction de la version SVN en anglais. Elle n'est pas encore compilable à cause d'erreurs dans le code xml qui sont en cours d'études. Une relecture globale serait par ailleurs nécessaire pour corriger les fautes de frappe.

N'hésitez pas à nous aider. D'avance merci infiniment.
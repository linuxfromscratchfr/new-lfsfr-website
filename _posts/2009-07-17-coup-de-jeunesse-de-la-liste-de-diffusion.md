---
layout: post
title: Coup de jeunesse de la liste de diffusion
date: 2009-07-17
tags: MAIN
---

L'état actuel du projet nous a donné envie de redynamiser la liste de diffusion francophone autour de lfs.

Du coup, un tri des archives a été fait et tous les spams ont été supprimés.

Par ailleurs, le 19 juillet, les utilisateurs seront triés et seuls ceux qui se seront manifestés comme souhaitant rester abonnés ne seront pas supprimés.

Enfin, une nouvelle finalité a été donnée à cette liste, vu que son volume d'activité est faible. Y seront postés dès à présent tous les compte-rendus des commits faits sur le serveur svn du projet. Les membres seront ainsi informés régulièrement des évolutions du projet et de ses progrès.

Elle regroupe donc aujourd'hui les traducteurs pour une collaboration dans leur travail. Elle fournit un espace d'entre-aide pour les utilisateurs francophones et les lecteurs des livres et des astuces traduits. Enfin, cette liste est à présent un espace d'information où sont postés les compte-rendus des modifications du serveur svn.

Ces évolutions et cette extension d'utilisation devraient, nous l'espérons, lui donner plus de dynamisme. N'hésitez pas à réagir et à y poster vos messages.
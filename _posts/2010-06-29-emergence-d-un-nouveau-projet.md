---
layout: post
title: Émergence d'un nouveau projet
date: 2010-06-29
tags: CLFS
---

Comme les amateurs de CLFS le savent, ce projet est particulièrement complexe. Outre les différents livres expliquant comment compiler un système pour un processeur x86, x8764, multilib... sur un PC, il en existe d'autres (sparc, mips...). Parmi eux, certains ouvrages sont consacrés au système "embarqués" (sysroot et embeeded). Ils appartiennent à un projet presque distinct de la collection courante de livres CLFS.

Jusqu'ici l'équipe CLFS, pour être utile à ses utilisateurs, n'a traduit que CLFS pour x86_64. La traduction multilib est en cours. Mais un utilisateur propose de traduire CLFS sysroot pour l'architecture ARM. C'est donc avec plaisir que nous l'accueillons parmi nous.

Un dépôt SVN sera créé prochainement pour cet ouvrage et vous pourrez y contribuer. N'hésitez pas à le lire, à le corriger et à traduire ce qui manque. Comme d'habitude, il sera disponible en téléchargement public et lecture publique lorsque la traduction aura été terminée et relue. En l'état, il est disponible en téléchargement, sous forme de fichiers XML, sur le dépôt svn pour ceux voulant participer.

Nous remercions en tout cas vivement l'utilisateur qui prend en charge cette traduction et complète notre collection. Bonne lecture à tous.
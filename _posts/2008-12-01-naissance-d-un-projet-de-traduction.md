---
layout: post
title: Naissance d'un projet de traduction
date: 2008-12-01
tags: CLFS
---

De nos jours, les processeurs sont de plus en plus capables  de supporter du 64 bits, ce qui les rend plus rapides. Il serait dommage que LFS ne puisse pas en profiter. Or, le système compilé par le livre LFS n'est qu'en 32 bits.

Le 32 bits n'est toutefois pas à exclure. Beaucoup de paquets en ont besoin. C'est pourquoi le système optimal moderne semble un 64 bits (x86_64) multilib afin de gérer les paquets ne connaissant que le 32 bits.

Il semble qu'un tel système soit celui que peuvent attendre légitimement la plupart des utilisateurs ayant un matériel récent. Dans ces conditions, il a été décidé de traduire le seul livre permettant de construire son propre système x86_64 multilib à partir de n'importe quelle autre architecture (32 bits, alpha, ppc).

Suite à cette traduction, nous proposerons celle du livre visant à construire un système pur 64 bits. Mais un tel système, bien qu'ayant de l'avenir car c'est vers là que tend l'évolujion de l'informatique, n'est pas universellement pleinement utilisable. Donc, il viendra plus tard, en bonus,  pour ceux pour lesquels le 64 bits pur suffit (moins nombreux que ceux aillant besoin d'un multilib, surtout chez la communauté LFS).
---
layout: post
title: LFS 10.1 est disponible
date: 2021-03-01
tags: LFS
---

Nous sommes heureux de vous annoncer la sortie de LFS 10.1 conjointement en
version SysV et Systemd, ainsi que la sortie de BLFS 10.1 dans ces deux
versions.

Cette nouvelle version contient une mise à jour majeure de la chaîne d’outils
avec les versions glibc-2.33 et binutils-2.36.1. Au total, ce sont 40
paquets qui ont été mis à jour. Un énorme travail rédactionnel a été réalisé
pour moderniser les textes tout au long du livre. Enfin, le noyau Linux a été
mis à jour vers la version 5.10.17.

Un grand merci à celles et ceux qui ont contribué à la traduction et aux
relectures !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur la liste de diffusion, le canal IRC
ou [mastodon](https://mamot.fr/@lfsfr).

Bonne lecture !

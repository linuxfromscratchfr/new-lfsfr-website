---
layout: post
title: Recherche de contributeurs
date: 2008-07-13
tags: MAIN
---

Bientôt, une version française de la version svn actuelle anglaise de LFS sera mise en ligne et mise à jour de manière synchronisée.

Le projet BLFS sera repris plus tard, probablement dans sa version SVN, les précédentes versions stables non traduites pourraient ne jamais l'être.

Ce vaste projet ajouté à celui de HLFS implique un temps considérable. Je ne pourrai notamment pas assumer les relectures de la traduction de la version SVN de LFS et de HLFS. Si vous souhaitez y aider, ainsi que contribuer au projet BLFS non encore repris ou à la traduction de HLFS en cours, n'hésitez surtout pas à me contacter sur mengualjeanphi CHEZ free.fr. D'avance merci.

Sur cette adresse, vous pouvez envoyer aussi toutes vos suggestions, voire compléter le projet par la traduction d'autres ouvrages de la LFS dont la traduction n'est aujourd'hui pas à l'ordre du jour.
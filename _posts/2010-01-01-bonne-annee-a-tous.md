---
layout: post
title: Bonne année à tous
date: 2010-01-01
tags: MAIN
---

En ces premières minutes de l'année 2010, toute l'équipe de lfs-fr se joint à moi pour vous souhaiter une très bonne année 2010, tant du point de vue de la santé que de tous les aspects de votre vie et de celle de vos proches. Que tous vos désirs se réalisent et que cette année soit pleine de succès et de prospérité.

En ce qui concerne plus particulièrement lfs-fr, nous espérons poursuivre notre chemin en 2010 comme en 2009. Cette année nous a apporté beaucoup de satisfactions et montré que notre travail n'est pas inutile. J'en profite pour remercier chaleureusement tous nos traducteurs, nos relecteurs et les utilisateurs qui se sont manifestés, sur le forum absolinux, sur le canal IRC ou sur la liste de diffusion. Grâce à eux, l'entreprise de traduction a beaucoup progressé, et il ne reste à ce jour que blfs à traduire tandis que clfs 64 bits est terminé et multilib est en voie d'achèvement. Restent aussi, à titre moins fondamental, des astuces et, peut-être, sortira la mise à jour de hlfs qui n'est à ce jour toujours pas lisible par le public (sauf sur subversion).

La manifestation par les utilisateurs de leurs questions, suggestions et visites sur notre site (près de 450 par jour en moyenne) nous permet de progresser, de corriger et d'améliorer la traduction. Elle nous permet aussi de trouver un écho encourageant notre travail. C'est donc une satisfaction dont nous vous remercions.

En ma qualité de coordinateur, je tiens à remercier spécialement les traducteurs et relecteurs qui m'entourent et qui me donnent l'énergie de poursuivre ce projet. Car s'il est passionnant, il avancerait moins vite sans ces contributions, pour des raisons matérielles, mais aussi parce que travailler seul serait moins motivant et ne créerait pas la même dynamique. Un grand merci à vous donc.

J'en profite enfin pour souhaiter au projet une bonne année, pleine de prospérité et de réussite, en espérant qu'il intéresse toujours plus de gens, qu'il attire des contributions et qu'il parvienne à atteindre son objectif de traduction presqu'exhaustive du projet lfs.

Merci donc encore à tous, bonne année à nouveau et... haut les coeurs!

Bon nouvel an,
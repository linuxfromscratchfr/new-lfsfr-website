---
layout: post
title: LFS poursuit sa route
date: 2013-03-04
tags: LFS
---

L'équipe de LFS-fr est heureuse de vous annoncer la publication de LFS version 7.3, accompagné de sa traduction disponible quelques heures plus tard dès samedi en lecture. Cette version contient de nombreux changements par rapport à celle 7.2, notamment s'agissant des versions de logiciels aussi importants que Binutils, le noyau Linux,, GCC et Glibc. Vous y trouverez aussi des corrections de sécurité. Mais la traduction capitalise les relectures faites depuis la dernière version.

Un travail rédactionnel a également été accompli sur les explications contenues dans l'ouvrage, qui a cherché à la fois à les clarifier et à les améliorer. Des compléments ont également été insérés sur les technologies modernes de partitionnement. On notera aussi la poursuite du travail sur les scripts de démarrage.

Nous tenions à remercier tous ceux qui ont participé à la traduction du livre. Leur soutien nous a été précieux et leur fidélité est particulièrement motivante.

N'hésitez pas à lire et télécharger le livre sur ce site aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
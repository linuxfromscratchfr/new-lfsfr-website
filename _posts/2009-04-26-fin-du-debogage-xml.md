---
layout: post
title: Fin du débogage xml
date: 2009-04-26
tags: CLFS
---

Grâce à un travail de toute l'équipe clfs, le débogage xml est terminé. La relecture du livre pure64 (ou x86_64-64) peut commencer. Le x86_64 ou multilib sera bientôt traduit; mais il faudrait que des relectures interviennent avant.

N'hésitez par conséquent pas à nous aider! On approche de la fin du plus gros travail pour ces ouvrages. Merci en tout cas à tout ceux qui nous ont aidé au débogage et... espérons que le livre sera relu. La version stable 1.1 pourrait être mise en ligne sans relecture pour gagner du temps, bien que la relecture soit souhaitable.

En tout cas le projet avance, ce qui nous motive fortement.
---
layout: post
title: Création d'un canal LFS IRC francophone
date: 2008-07-23
tags: MAIN
---

Besoin d'aide avec les livres traduits ? ou de manière générale sur un des livres du projet LFS ? Vous ne parlez pas anglais et ne pouvez demander sur #lfs-support sur le serveur irc.linuxfromscratch.org ? Aucun souci. A été ouvert aujourd'hui un canal francophone d'aide relatif à LFS. N'hésitez pas à vous y rendre, il se situe sur le même serveur que #lfs-support et s'intitule #lfs-fr. Nous essaierons de vous y aider ou vous pourriez y aider des gens si vous le souhaitez. En outre, vous pouvez y discuter avec l'équipe de traduction et nous signaler tout problème avec le livre, ce qui nous aidera. Et bien sûr vous pourrez nous y rencontrer pour en savoir plus sur les manières dont vous pouvez nous aider. Ce canal qui se veut convivial est à votre disposition. N'hésitez pas.
---
layout: post
title: Mise en place de la traduction de BLFS-6.0
date: 2005-06-20
tags: BLFS
---

Sur les versions précédentes, un paquetage était représenté par quatres fichiers : description, informations, installation, configuration. Parfois, il y en avait plus. Pour la version 6.0 de BLFS, l'équipe a décidé de passer à un seul fichier par paquetage. Du coup, il est pratiquement impossible de récupérer les différences entre les fichiers des deux dernières versions. Du coup, cela oblige à tout traduire de nouveau. C'est énormément de travail mais c'est un travail que je peux déléguer, au moins en partie. Vous avez donc un bloc supplémentaire sur la gauche pour suivre l'évolution de la traduction et pour y participer en réservant des fichiers. N'oubliez pas de m'envoyer vos demandes de réservation par mail (guillaume POINT lelarge CHEZ gmail POINT com).

J'ai déjà terminé quelques chapitres, j'ai aussi réservé plusieurs autres fichiers. Allez, je retourne au travail :)
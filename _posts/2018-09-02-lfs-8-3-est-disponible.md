---
layout: post
title: LFS 8.3 est disponible
date: 2018-09-02
tags: LFS
---

Nous sommes heureux de vous annoncer la sortie de LFS 8.3 conjointement en version SysV et Systemd, ainsi que la sortie de BLFS 8.3 dans ces deux versions.

Cette nouvelle version contient une mise à jour majeure de la chaîne d’outils avec les versions glibc-2.28, binutils-2.31.1 et gcc-8.2.0. Un énorme travail rédactionnel a été réalisé pour améliorer les textes tout au long du livre. Enfin, le noyau Linux a été mis à jour vers la version 4.18.5.

Cette version marque l'arrivée du format epub disponible au [téléchargement](/lfs/telecharger/), spécifique à la version française du livre.

Un grand merci à celles et ceux qui ont contribué à la traduction et aux relectures !

N'hésitez pas à lire et télécharger le livre sur le site [fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou epub, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.

Bonne lecture !

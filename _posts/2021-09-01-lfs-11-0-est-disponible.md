---
layout: post
title: LFS 11.0 est disponible
date: 2021-09-01
tags: LFS
---

Nous sommes heureux de vous annoncer la sortie de LFS 11.0 conjointement en
version SysV et Systemd, ainsi que la sortie de BLFS 11.0 dans ces deux
versions.

Il s'agit d'une version majeure de LFS et BLFS.

La raison derrière la nouvelle version majeure est que cette version
n'utilise plus le système du « /usr séparé ». Cela signifie que, comme
pour la plupart des distributions modernes, /bin est un lien symbolique
version /usr/bin. De même, /lib et /sbin sont tous deux des liens symboliques
vers le dossier correspondant dans /usr. Cette nouvelle façon de faire
satisfait de nouvelles exigences de systemd.

Cette nouvelle version contient une mise à jour majeure de la chaîne d’outils
avec les versions gcc-11.2.0, glibc-2.34 et binutils-2.37. Un énorme travail
rédactionnel a été réalisé pour moderniser les textes tout au long du livre.
Enfin, le noyau Linux a été mis à jour vers la version 5.13.12.

Un grand merci à celles et ceux qui ont contribué à la traduction et aux
relectures !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur la liste de diffusion, le canal IRC
ou [mastodon](https://mamot.fr/@lfsfr).

Bonne lecture !

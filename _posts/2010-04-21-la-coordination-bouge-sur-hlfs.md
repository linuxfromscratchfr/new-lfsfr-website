---
layout: post
title: La coordination bouge sur hlfs
date: 2010-04-21
tags: HLFS
---

Ce matin le mainteneur de hlfs, Robert Connolly, a déclaré son intention de céder sa place de mainteneur de HLFS. Il explique qu'il manque de temps pour assurer cette tâche et que, sans abandonner le projet, il prend des distances et un statut plus conforme à son temps disponible.

Cette nouvelle a bien entendu des conséquences sur la traduction. La première réponse qui est apparue à celle-ci vient de Robert Baker, lequel envisage de prendre la relève mais de donner une nouvelle impulsion au projet. Parmi ses idées, il propose un retour au format docbook, donc xml.

C'est la raison pour laquelle l'équipe de traduction fait une pause dans la mise à jour du livre en attendant la suite. Ayant toujours des traces du xml, retourner à ce format ne sera pas problématique. Mais il faut patienter et attendre que des décisions concrètes soient prises pour suivre le mouvement. Un travail dans le flou ne serait pas productif.

HLFS retourne donc en pause mais la version en ligne reste valable. Nous attendons à présent la suite des événements chez nos amis anglophones.
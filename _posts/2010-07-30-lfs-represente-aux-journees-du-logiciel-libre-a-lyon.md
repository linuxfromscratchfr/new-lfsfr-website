---
layout: post
title: LFS représenté aux Journées du Logiciel Libre à Lyon
date: 2010-07-30
tags: MAIN
---

Le 15 et 16 octobre se tiennent à Lyon les Journées du Logiciel Libre [http://jdll.org](http://jdll.org). L'association Traduc.org s'y rendra et sera représentée par son trésorier et son vice-président. C'est pourquoi on peut dire que lfs sera représenté. Outre le stand Traduc, il y aura une conférence sur l'accessibilité offerte par le logiciel libre.

Cet événement vous permettra de rencontrer dans la région lyonnaise, pour ceux qui le souhaitent et qui le peuvent, des travailleurs solitaires mais qui aiment le contact du public... Alors n'hésitez pas à vous y rendre.

Lyonnais et lyonnaises, habitants du Rhône ou en transit dans la région qui veulent voir physiquement un traducteur de lfs et échanger avec lui, n'hésitez pas à nous rejoindre vendredi et samedi. Toutes les informations pratiques figurent sur le site de l'événement. Nous espérons vous rencontrer.
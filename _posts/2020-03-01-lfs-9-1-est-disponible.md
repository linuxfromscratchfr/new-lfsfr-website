---
layout: post
title: LFS 9.1 est disponible
date: 2020-03-01
tags: LFS
---

Nous sommes heureux de vous annoncer la sortie de LFS 9.1 conjointement en
version SysV et Systemd, ainsi que la sortie de BLFS 9.1 dans ces deux
versions.

Cette nouvelle version contient une mise à jour majeure de la chaîne d’outils
avec les versions glibc-2.31 et binutils-2.34. Au total, ce sont 35 paquets qui
ont été mis à jour. Un nouveau paquet, zstd, a aussi été ajouté. Un énorme
travail rédactionnel a été réalisé pour améliorer les textes tout au long du
livre. Enfin, le noyau Linux a été mis à jour vers la version 5.5.3.

Un grand merci à celles et ceux qui ont contribué à la traduction et aux
relectures !

N'hésitez pas à lire et télécharger le livre sur le site
[fr.linuxfromscratch.org](//fr.linuxfromscratch.org) aux formats html, pdf ou
epub, puis à nous adresser vos retours sur la liste de diffusion, le canal IRC
ou mastodon.

Bonne lecture !

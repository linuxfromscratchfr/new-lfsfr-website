---
layout: post
title: Bonne nouvelle, la réservation de fichiers pour BLFS est possible :)
date: 2004-05-31
tags: BLFS
---

Je viens enfin de terminer la création des fichiers HTML permettant de connaître la liste des fichiers à traduire et leurs états.

La relecture peut donc réellement commencer. Il vous suffit de m'envoyer par courrier électronique le nom des fichiers que vous souhaitez relire et je vous les enverrais en réponse. Bon courage à tous :)
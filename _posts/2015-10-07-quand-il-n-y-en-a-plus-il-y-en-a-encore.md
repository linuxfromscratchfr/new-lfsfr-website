---
layout: post
title: Quand il n'y en a plus, il y en a encore!
date: 2015-10-07
tags: LFS
---

Une semaine après la version Systemd, nous sommes heureux de vous annoncer la publication de la traduction de LFS version 7.8, dans sa version principale basée sur SysV. Cette version, apparue une semaine après la version anglaise, contient de nombreux changements par rapport à celle 7.6, notamment s'agissant des versions de logiciels aussi importants que Binutils, le noyau Linux,, GCC et Glibc. Vous y trouverez aussi des corrections de sécurité. La traduction, quant à elle, capitalise les relectures faites depuis la dernière version.

Un travail rédactionnel a également été accompli sur les explications contenues dans l'ouvrage, qui a cherché à la fois à les clarifier et à les améliorer.

La principale nouveauté de cette version française est le visuel, qui a été aménagé pour une meilleure lisibilité sur appareils mobiles.

Un grand merci à celles et ceux qui ont contribué à la traduction, en particulier Amaury pour avoir publié en un temps record la version Systemd (ce qui a motivé l'équipe pour publier cette version-ci plus rapidement), et qui a largement contribué à corriger des coquilles dans cette version. Il a enfin permis le réaménagement visuel du livre traduit.

N'hésitez pas à lire et télécharger le livre sur le site www.fr.linuxfromscratch.org aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
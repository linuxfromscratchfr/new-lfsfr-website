---
layout: post
title: LFS 7.9 est publiée
date: 2016-03-08
tags: LFS
---

Nous sommes heureux de vous annoncer la publication de la traduction de
LFS version 7.9, dans sa version principale basée sur SysV. Cette version, apparue simultanément à la version anglaise, contient de nombreux changements par rapport à celle 7.8, notamment s'agissant des versions de logiciels aussi importants que Binutils, le noyau Linux,, GCC et Glibc. Vous y trouverez aussi des corrections de sécurité. La
traduction, quant à elle, capitalise les relectures faites depuis la dernière version.

Un travail rédactionnel a également été accompli sur les explications contenues dans l'ouvrage, qui a cherché à la fois à les clarifier et à les améliorer.

Un grand merci à celles et ceux qui ont contribué à la traduction, en particulier Amaury, Denis et Julien qui ont largement aidé à corriger cette version. Peut-être est-ce la dernière, ou l'avant-dernière, version traduite par la méthode mise en place depuis 2002. Les futures pourraient se traduire par la méthode Gettext. Dossier à suivre.


N'hésitez pas à lire et télécharger le livre sur le site www.fr.linuxfromscratch.org aux formats html ou pdf, puis à nous adresser vos retours sur la liste de diffusion, le forum ou le canal IRC.
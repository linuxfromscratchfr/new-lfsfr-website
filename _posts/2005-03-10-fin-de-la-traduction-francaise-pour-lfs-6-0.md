---
layout: post
title: Fin de la traduction française pour LFS-6.0
date: 2005-03-10
tags: LFS
---

Il faudra attendre un peu pour la relecture. En attendant, la version est disponible sur lfs.traduc.org.
---
layout: post
title: Sortie d'une nouvelle version de développement
date: 2008-10-20
tags: LFS
---

Comme annoncé, la LFS a profondément évolué. De très nombreuses mises à jour ont été effectuées. La traduction a suivi donc. De futures évolutions sont à prévoir, mais probablement mineures.

La traduction pourrait quant à elle subir de légères rectifications pour des raisons de xml. Elles interviendront prochainement et ne modifieront pas substantiellement le livre.

Bonne lecture et amusez-vous bien.
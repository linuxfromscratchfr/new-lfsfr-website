---
layout: post
title: Bientôt une 6.4 stable
date: 2008-11-06
tags: LFS
---

LFS vient de sortir une version presque stable en anglais: la 6.4rc1. Ceci annonce la dernière phase de test avant la sortie officielle d'une 6.4 stable contenant les paquets récents.

La traduction sera disponible très bientôt en ligne et téléchargement. Nous sommes nous aussi en voie de l'achèvement de la traduction de cette mise à jour, que nous espérons achever cette semaine ou, au plus tard, fin semaine prochaine.
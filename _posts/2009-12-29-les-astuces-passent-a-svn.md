---
layout: post
title: Les astuces passent à svn
date: 2009-12-29
tags: ASTU
---

Comme les habitués du projet lfs francophone le savent, la liste de diffusion permet aux utilisateurs d'échanger et aux traducteurs de s'entre-aider. C'est aussi la destination des compte-rendu de chaque commit fait sur le dépôt subversion. De cette façon, chacun est au courant des toutes dernières évolutions, mêmes minimes, de la traduction.

Il est apparu important que l'arrivée de nouvelles traductions d'astuces, leurs corrections successives jusqu'à la publication, suive un cheminement similaire. C'est pour cela qu'un nouveau répertoire, consacré aux astuces, vient d'être créé. Grâce au système de compte-rendu des commit, il permettra à chacun de connaître en temps réel l'arrivée de nouvelles traductions d'astuce, de corrections puis de publication. Il devrait aussi permettre un partage plus facile des fichiers texte.

C'est donc une nouvelle voie qui s'ouvre pour le projet, qui permettra encore plus de transparence et de visibilité. Cette piste devrait conforter le dynamisme de la liste.
---
layout: post
title: LFS 6.5 disponible au format pdf
date: 2009-08-19
tags: LFS
---

Nous vous annoncions la sortie de LFS 6.5 il y a deux jours mais ne pouvions proposer le téléchargement du livre au format pdf. C'est désormais chose faite. Le livre est à présent téléchargeable au format pdf, sur cette adresse. Le format html reste bien sûr toujours téléchargeable sous la forme de plusieurs fichiers et la consultation en ligne également.

Bonne lecture à tous et n'hésitez pas à nous faire part de vos impressions ou à demander de l'aide si nécessaire, grâce aux moyens exposés dans la rubrique support.
---
layout: post
title: BLFS 7.7 disponible !!
date: 2015-03-14
tags: BLFS
---

Bonjour à tous

Je suis content de vous annoncer la sortie de BLFS 7.7 et annoncer que cette version est disponible également en français.

Pour mémoire, BLFS (Beyond Linux From Scratch) est en quelque sorte la suite de LFS. C'est un livre qui propose d'installer sur une LFS différents paquets.

Il est ainsi possible de transformer une LFS en serveur LAMP ou alors même ajouter un gestionnaire de bureau pour une utilisation quotidienne de LFS (oui c'est possible !! ;o), j'ai d'ailleurs une LFS en OS principal sur le PC depuis lequel j'écris cet article).

Faire de la traduction est aussi un moyen de s'investir dans le libre sans avoir des compétences de développeurs.

Pour info, traduire BLFS est un travail quotidien pour lequel je passe environ 5 heures par semaine (en fait c'est juste une question de choix pour trouver du temps ;o) ).

Cette version du livre comporte plus de 700 paquets à jour (généralement, la dernière version stable de chaque logiciel est testé avec la version LFS stable). Ce livre de plus de presque 1600 pages est une bible également pour ceux qui souhaitent compiler leur propre paquet. Je suis vraiment content de pouvoir proposer cette version en français pour les non-anglophones.

Je veux également remercier Texou pour la traduction de LFS et pour la coordination de l'ensemble du projet de traduction en français, et amj qui est venu compléter l'équipe et qui gère la traduction de la version systemd de LFS. Merci à eux pour leur travail.
Les livres LFS sont traduits dans plusieurs langues, mais à ma connaissance, seule la traduction française propose les livres à la dernière version aussi rapidement (environ 1 grosse semaine de retard pour BLFS…. qq heures pour LFS !! ). C'est d'ailleurs ce travail qui a été reconnu par l'équipe des rédacteurs, puisque notre projet de traduction et les livres francophones sont maintenant hébergés sur le serveur du projet linuxfromscratch.org.

Il manque juste un courageux pour venir proposer une version fr de BLFS-SYSTEMD.

Et donc pour finir, juste qq chiffres sur cette version de BLFS 7.7:
1567 pages pour le PDF (contre 1559 pour la version 7.6)
1102 fichiers xml traduits
1232 commits sur la version anglaise ont été traduit depuis la version 7.6 (en 6 mois)

Vous retrouverez cette nouvelle version sur le site www.fr.linuxfromscratch.org.

A bientôt pour partager vos expériences de construction ;o)

LFS : vos règles, votre distribution …

Denis
---
layout: post
title: Encore une astuce
date: 2009-03-23
tags: ASTU
---

Notre nouveau membre nous a permis de traduire une seconde astuce. Elle concerne la construction de LFS dans un système disposant de peu d'espace disque. N'hésitez pas à la consulter.

Nous espérons l'arrivée d'autres bientôt et nous remercions le traducteur qui nous a fourni un travail très appréciable.
---
layout: post
title: LFS poursuit son chemin
date: 2008-11-15
tags: LFS
---

Pendant que la LFS 6.4 se stabilise doucement, les développeurs de LFS poursuivent les mises à jour, notamment par la version SVN. Fidèles à nos engagements, nous menons donc la traduction de la 6.4 lorsqu'elle évolue, et celle de la SVN à chaque changement.

C'est pourquoi vous pouvez à présent lire en ligne la version 6.3, 6.4rc1, et SVN (actuellement 20081113). Par contre, s'il a été décidé de rendre LFS-6.4rc1 disponible pour le téléchargement en HTML et PDF, afin de se caler sur l'idée du projet anglais, nous y avons renoncé pour la SVN. Elle est donc consultable en ligne mais uniquement téléchargeable sur le CVS au format xml.
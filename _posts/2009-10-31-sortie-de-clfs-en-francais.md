---
layout: post
title: Sortie de CLFS en français
date: 2009-10-31
tags: CLFS
---

Plus d'un an de travail aura été nécessaire. Il aura permis, grâce au soutien appréciable de notre relecteur, d'achever enfin la traduction du livre CLFS 1.1, livre permettant une compilation croisée visant la fabrication d'un système pur 64 bits.

Mais cette relecture a eu une portée plus grande. Elle nous a permis de corriger également la traduction de la version SVN anglaise. Même si cette dernière tâche n'a pu éliminer toutes les fautes, nous considérons qu'elle rend le livre lisible.

C'est pourquoi nous sommes fiers d'annoncer, enfin, la sortie de la traduction de CLFS 1.1 et SVN en 64 bits, la version 1.1 étant relue, celle SVN à relire mais utilisable.

La prochaine étape, qui n'est pas insurmontable, est la traduction du livre multilib. Nous privilégierons la version SVN anglaise pour des questions d'ancienneté de la 1.1. N'hésitez donc pas à nous aider à traduire et à relire ces travaux. Ils sont d'une ampleur moindre car une grande part est rattachée au tronc commun ou reprend les instructions pour le livre 64 bits. Une bonne partie du livre est donc déjà traduite.

Nous remercions encore une fois chaleureusement notre relecteur. La réalisation de ces livres concrétise un travail de longue haleine, ce qui nous encourage. N'hésitez pas à nous adresser des retours si vous pensez que la traduction est améliorable.
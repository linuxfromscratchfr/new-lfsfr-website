module Jekyll
    module Lfs

    def print_download(input)
      if /HTML/ =~ input
        "html"
      elsif /pdf.bz2/ =~ input
        "pdf compressé"
      elsif /pdf/ =~ input
        "pdf"
      elsif /html/ =~ input
        "html monofichier"
      elsif /epub/ =~ input
        "epub"
      elsif /XML/ =~ input
        "source xml"
      elsif /ps.bz2/ =~ input
        "postscript compressé"
      elsif /SGML/ =~ input
        "source sgml"
       else
        input
      end
    end

    def license_badger(name)
      name.gsub!(/\./,"_")
      site = @context.environments.first["site"]
      licenses = site.data['licenses']
      if licenses.key?(name)
        badge = '<a href="' + licenses[name]['url'] + '">
                    <img src="' + site['baseurl'] + licenses[name]['img']+ '"
                    alt="' + licenses[name]['alt'] + '" style="height:25px;" >
                 </a>'
        badge
      else
        name
      end
    end

  end
end


Liquid::Template.register_filter(Jekyll::Lfs)

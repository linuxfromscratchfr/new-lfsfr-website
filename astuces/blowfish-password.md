---
layout: astuce
use-site-title: true
title: Mots de passe Blowfish
author: Robert Connolly &lt;robert at linuxfromscratch.org&gt; (ashes)
date: 2006-12-10
license: Public Domain
synopsis: Mots de passe Blowfish.
description: Comment installer une bibliothèque de cryptographie blowfish et l'utiliser.
---

Un article sur l'algorithme blowfish est disponible ici : 
[http://www.usenix.org/events/usenix99/provos.html](http://www.usenix.org/events/usenix99/provos.html)

Téléchargements
===============

* [http://ca.lfs-matrix.net/patches/downloads/glibc/glibc-2.5-blowfish.patch](http://ca.lfs-matrix.net/patches/downloads/glibc/glibc-2.5-blowfish.patch)
* [http://ca.lfs-matrix.net/patches/downloads/shadow/shadow-4.0.18.1-owl_blowfish-1.patch](http://ca.lfs-matrix.net/patches/downloads/shadow/shadow-4.0.18.1-owl_blowfish-1.patch)
* [http://ca.lfs-matrix.net/patches/downloads/sysvinit/sysvinit-2.86-owl_blowfish.patch](http://ca.lfs-matrix.net/patches/downloads/sysvinit/sysvinit-2.86-owl_blowfish.patch)

Installation
============

## Avec Glibc chapitre 6 :

```bash
patch -Np1 -i ../glibc-2.5-blowfish.patch
```

## Avec Shadow chapitre 6 :

```bash
patch -Np1 -i ../shadow-4.0.18.1-owl_blowfish-1.patch &&
aclocal &&
autoconf &&
autoheader
```

Ce correctif de Shadow doit être regénéré avec les autotools pour pouvoir être
utilisé par différentes versions de Shadow. Tant que le correctif s'applique
sans erreur alors il devrait marcher avec n'importe quelle version de Shadow
que vous utilisez.

Ce correctif fera que blowfish utilisera `/dev/random` comme source d'entropie.
Si vous voulez utiliser `/dev/urandom` ou autre, utilisez l'option
`configure --with-random=...`

La commande Sed pour `MD5_CRYPT_ENAB` de la page sur Shadow ne fera aucune
différence. Si vous obtenez une erreur avec `make install` à cause d'une drôle
d'affaire d'autoconf, utilisez : 

```bash
make MKINSTALLDIRS=$(pwd)/mkinstalldirs install
```

Shadow pourra toujours utiliser les mots de passe MD5 ou DES si vous les
ajoutez à `/etc/shadow` à la main, ou si vous réinitialisez `CRYPT_PREFIX` à
`$1$` dans `/etc/login.defs`.

Après l'exécution de `passwd`, vous devriez trouver vos mots de passe présents
dans `/etc/shadow` et commençant par `$2a$`.

## Avec Sysvinit au chapitre 6:

```bash
patch -Np1 -i ../sysvinit-2.86-owl_blowfish.patch
```

OpenSSH peut être installé normalement et l'utilisation de
`--with-md5-passwords` est facultative (il pourra toujours utiliser les mots
de passe blowfish).

Remerciements
=============
*   Le projet Openwall. [http://www.openwall.com/crypt/](http://www.openwall.com/crypt/)
*   Solar Designer. &lt;solar at openwall&gt;
*   Thorsten Kukuk. [http://ftp.suse.com/pub/people/kukuk/](http://ftp.suse.com/pub/people/kukuk/)

Historique des changements
==========================

*   [04-02-2005]
    *   Astuce initiale.
*   [05-02-2005]
    *   Ajout d'une remarque sur --with-random.
    *   Ajout d'une remarque sur SSHD avec with-md5-passwords.
    *   Déplacement du fichier libxcrypt.la vers /usr/lib.
*   [06-02-2005]
    *   Ajout de sed pour xcrypt dans OpenSSH.
*   [24-02-2005]
    *   Rectification de l'emplacement d'installation des bibliothèques libxcrypt.
*   [13-11-2005]
    *   Passage à libxcrypt-2.3.
    *   On n'installe pas la libcrypt de libc mais on installe libxcrypt à la place.
*   [10-12-2005]
    *   Utiliser le Blowfish de Owl pour Glibc car libxcrypt est en conflit avec OpenSSH.
    *   Ajout de nouveaux correctifs pour Shadow et Sysvinit.

Historique de la traduction
===========================

*  [19-05-2009]
   *   Traduction initiale
*  [29-05-2009]
   *   « cryptage » est remplacé par « cryptographie », reformatage


---
layout: astuce
use-site-title: true
title: Remplacer GRUB par Syslinux
author: Denis Mugnier
date: 2012-06-15
license: CC-BY-SA-3
synopsis: Installation de syslinux à la place de grub
description: Syslinux est un gestionnaire de démarrage léger et rapide.
---

Pré-requis
==========

*   Une LFS fonctionnelle (testé sur une LFS 7.0). Il est peut être mieux d'avoir une LFS avec un grub d'installé
    qui démarre déjà.
*   nasm (voir BLFS)
*   python (voir BLFS : python 2.7)

Astuce
======

Le but de cette astuce est de remplacer grub dans une LFS fonctionnelle ou d'installer syslinux
dans une LFS en construction.

{: .alert .alert-warning}
  __ATTENTION__&nbsp;:
  les manipulations de cette astuce vont installer un chargeur de démarrage, ces manipulations
  sont donc potentiellement dangereuse pour votre système. Il se peut qu'en cas de problèmes, le système
  ne veuille plus démarrer.

Etape 1 : Télécharger les sources et les déballer
=================================================

Vous pouvez télécharger les sources à l'adresse :
[http://www.kernel.org/pub/linux/utils/boot/syslinux/syslinux-4.05.tar.xz](http://www.kernel.org/pub/linux/utils/boot/syslinux/syslinux-4.05.tar.xz)

Ensuite il faut les déballer

```bash
tar -xf syslinux-4.05.tar.xz
```

Les sources contiennent également les versions binaires des fichiers. Il est donc possible si vous le souhaitez
de passer directement à l'étape installation.

Etape 2  : Compilation
======================

La compilation est très simple :

```bash
make
```

_Nota_&nbsp;: la compilation nécessite l'installation de nasm et python (voir BLFS pour leur installation)

Etape 3 : installation des binaires
===================================

En utilisant l'instruction suivante&nbsp;:

```bash
make install
```

Les "installeurs" de syslinux sont maintenant installé sur votre LFS

Etape 4 : Mise en place du démarrage avec syslinux
==================================================

Voilà la partie intéressante de l'astuce. Il est a noter que cette astuce ne traitera que de l'utilisation
de extlinux qui permet le démarrage du système depuis une partition ext2, ext3 ou ext4. Il est possible
d'utiliser syslinux pour démarrer depuis un CD, un disque FAT etc... Pour plus de renseignements sur les
autres méthodes, aller voir [sur le site officiel](www.syslinux.org)

Il faut créer un répertoire pour l'installation du gestionnaire de démarrage

```bash
mkdir /boot/extlinux
```

et utiliser extlinux pour installer le gestionnaire de démarrage

```bash
extlinux --install /boot/extlinux
```

Etape 5 : Configuration
=======================

La configuration se fera dans le fichier /boot/extlinux/syslinux.conf

Dans sa version la plus courte le fichier contiendra :

```
DEFAULT lfs
LABEL lfs
   KERNEL /boot/vmlinuz.img
   APPEND ro root=/dev/xxx
```

*   _vmlinuz.img_&nbsp;: image du noyau, ou un lien symbolique vers l'image du noyau.
*   _/dev/xxx_&nbsp;: partition qui contient votre système.

Pour plus d'information sur le fichier de configuration, je vous invite à consulter la documentation. Sachez
qu'il est possible d'avoir un menu et choisir son système à démarrer, de faire du boot PXE,
d'afficher un fichier...

Il faut vérifier que votre partition est correctement configurée pour démarrer (avec le drapeau de démarrage)

Pour initialiser ce drapeau :

```bash
fdisk /dev/yyy
```

(avec yyy le nom du disque sda, sdb, etc par exemple)

ensuite choix `a`

et donner le numéro de la partition qui doit avoir le drapeau de démarrage.
tapez `w` pour enregistrer

par exemple pour initialiser la partition `/dev/sda1` avec le drapeau de démarrage&nbsp;:

```
fdisk /dev/sda
Command (m for help): a
Partition number (1-4): 1
Command (m for help): w
```

Ensuite il faut initialiser le mbr (ce qui va remplacer l'installation de grub s'il a déjà été installé)

Dans les sources, allez dans le répertoire mbr

puis exécutez la commande

```bash
cat mbr > /dev/yyy #avec yyy le nom du disque de démarrage (par ex sda, sdb ...)
```

Etape 6 : tester
================

Maintenant que nous avons terminé l'installation, il faut redémarrer le système pour vérifier que syslinux
fonctionne correctement.

{: .alert .alert-warning }
__ATTENTION__&nbsp;:
Vous devez prévoir un dispositif de démarrage de secours pour votre système. Si syslinux rencontre
un problème, votre système ne va pas démarrer. Il faut donc avoir soit un live CD pour se dépanner en cas de
problème.


Normalement, au démarrage, avec le fichier de config minimal donné, vous devez voir votre machine démarrer
directement sur le noyau de LFS.

Syslinux est installé.

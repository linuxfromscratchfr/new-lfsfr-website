---
layout: astuce
use-site-title: true
title: Approche Fakeroot pour l'installation de paquets
translator: Denis Mugnier &lt;myou72 at orange point fr&gt;
license: BSD
date: 2006-01-17
synopsis: Approche Fakeroot pour l'installation de paquets
description: "
<p>
Les instructions LFS et BLFS installent les fichiers directement dans le répertoire
racine sans aucune sorte de gestion de paquets. Sauf quelques cas simplistes,
ne pas avoir de gestion des paquets sur le système conduira à des problèmes.
</p>

<p>Dans cette astuce, je discute d'une manière de gérer les paquets qui est très
souvent utilisée par les distributions - l'approche fakeroot. Et en voyant les
avantages, ce n'est pas étonnant que ce soit la méthode la plus populaire.
</p>
"
---


Pièces jointes
==============

*   [http://www.linuxfromscratch.org/hints/downloads/attachments/autotools-multiversion/autoconf-wrapper](http://www.linuxfromscratch.org/hints/downloads/attachments/autotools-multiversion/autoconf-wrapper)
*   [http://www.linuxfromscratch.org/hints/downloads/attachments/autotools-multiversion/automake-wrapper](http://www.linuxfromscratch.org/hints/downloads/attachments/autotools-multiversion/automake-wrapper)
*   [http://www.linuxfromscratch.org/patches/download/autoconf/autoconf-2.13-race.patch](http://www.linuxfromscratch.org/patches/download/autoconf/autoconf-2.13-race.patch)
*   [http://www.linuxfromscratch.org/patches/download/autoconf/autoconf-2.13-fedora_macro_fixes-1.patch](http://www.linuxfromscratch.org/patches/download/autoconf/autoconf-2.13-fedora_macro_fixes-1.patch)
*   [http://www.linuxfromscratch.org/patches/download/automake/automake-1.4-p6-fedora_fixes.patch](http://www.linuxfromscratch.org/patches/download/automake/automake-1.4-p6-fedora_fixes.patch)
*   [http://www.linuxfromscratch.org/patches/download/automake/automake-1.5-fedora_fixes.patch](http://www.linuxfromscratch.org/patches/download/automake/automake-1.5-fedora_fixes.patch)

Astuce
======

Dans l'approche fakeroot, au lieu d'installer les fichiers dans l'emplacement final (c'est à dire /),
les fichiers sont installés dans un endroit temporaire et ensuite déplacés à un
autre endroit. La façon de faire cela dépend des paquets.
Généralement, les instructions de compilation sont identiques à ce que vous pouvez trouver dans les livres
LFS ou BLFS. C'est seulement pendant l'installation que vous devez modifier les instructions.

Avant d'aller dans les détails de l'approche, d'analyser les avantages de l'approche Fakeroot pour que vous
puissiez décider si c'est la bonne technique pour votre système.

*   Ne requiert pas des paquets supplémentaires à installer sur votre système.
*   Permet la manipulation des fichiers installés avant qu'ils aillent à leur place finale.
    Cela supprime les correctifs de LFS comme coreutils-remove-XXX.patch
    qui ne seront jamais acceptés en amont. cela permet aussi des opérations de maintenance
    variées comme la compression des pages de manuel ou d'info. Cela vous permet aussi de ne pas autoriser
    le paquet à remplacer certains fichiers (comme les fichiers de configuration).
*   Permet la mise à jour des paquets sur un système en fonctionnement (y compris des paquets comme
    glibc). N'importe qui ayant essayé de mettre à jour qt en étant connecté dans kde sait de quoi je veux parler:)
    Déconnecter les utilisateurs pendant la durée des mises à jour ne donne pas une bonne image de l'administrateur système.
*   Si l'installation du paquet échoue pour une raison quelconque, il n'y aura pas un paquet "à moitié cuit" dans /.
    Bien qu'un peu extrême, imaginez l'exemple suivant. Vous avez construit gcc dans BLFS et avez un problème pendant le make install.
    Vous pouvez être planté car un gcc partiel est installé dans /  qui peut ou pas fonctionner.
    Vous avez aussi détruit le gcc précédement installé. Si vous l'aviez installé en fakeroot, vous pourriez chercher la cause, corriger le problème, effacer le répertoire
    fakeroot, et installer les fichiers de nouveau.
*   Permet la vérification des conflits avant que le paquet ne soit installé. Par exemple,
    heimdal installe des exécutables qui écrasent des exécutables d'autres paquets.
    Si vous installez aveuglément heimdal dans / vous pouvez ne pas le remarquer avant qu'il ne soit trop tard.
*   Fournit une distinction claire pour la configuration de post-installation. LFS et BLFS parlent seulement
    de l'installation des paquets (et parfois de la mise à jour des paquets) mais jamais de la suppression des paquets.
    Certains paquets sont particulièrement difficiles à supprimer. N'effacver que
    les fichiers qui ont été installés par ne paquet
    n'est pas suffisant, vous devez également lancer des commandes supplémentaires.
    Par example, pour les paquets GNOME vous devez désinstaller les schémas et régénérer les fichiers de scrollkeeper.
    Avec l'approche fakeroot, vous allez vite prendre l'habitude d'effectuer deux étapes supplémentaires :
    -   configuration post installation : Les étapes qui doivent être faites après que le paquet est déplacé dans /.
    -   configuration post suppression : Les étapes qui doivent être faites après que le paquet est effacé.
        Généralement, cela inverse ce qui doit être fait dans la configuration de post installation.

Les étapes pour installer un paquet avec cette approche sont les suivantes. Si vous avez créé des scripts pour l'installation des paquets, chacune des lignes
suivantes peut être une fonction de vos scripts.

1.  déballez les sources et appliquez les correctifs si nécessaire.

    ```bash
    tar -xvf $pkg-$ver.tar.bz2
    ```
2.  Compilez.

    ```bash
    cd $pkg-$ver
    ./configure --prefix=/usr
    make
    ```
3.  Éventuellement, testez le paquet.

    ```bash
    make -k check
    ```
4.  Installez dans fakeroot. Ajustez la variable FAKEROOT en fonction de votre système.

    ```bash
    FAKEROOT=/usr/fakeroot/$pkg-$ver
    make DESTDIR=$FAKEROOT install
    ```
5.  Effectuez toute la maintenance que vous voulez sur les fichiers installés.
6.  Pour une mise à jour de paquet, effectuez les étapes de configuation post-effacement pour la version précédement installée.
7.  Déplacez le paquet vers la destination finale.

    ```bash
    cd $FAKEROOT
    tar cf - . | (cd / ; tar xf - )
    ```
8.  Effectuez la configuration de post-installation.
9.  Pour la mise à jour d'un paquet, effacez les fichiers laissés par la version précédente.

En espérant que maintenant vous êtes convaincu que l'approche fakeroot est la bonne approche pour vous.
Alors voyons les détails sur la manière de la mettre en oeuvre.
Comme il est impossible d'inclure des instructions pour tout les paquets de LFS et BLFS, je vais passer en revue quelques façons
d'installer des paquets en fakeroot. Si vous êtes coincé par un paquet particulier :

*   récupérez mes scripts de construction sur
    [http://linuxfromscratch.org/~tushar/build-scripts/](http://linuxfromscratch.org/~tushar/build-scripts/).
*   Récupérez les Ebuilds de Gentoo sur [http://www.gentoo.org/cgi-bin/viewcvs.cgi/](http://www.gentoo.org/cgi-bin/viewcvs.cgi/).
*   Récupérez les fichiers spec de Fedora sur
    [http://cvs.fedora.redhat.com/viewcvs/éroot=core](http://cvs.fedora.redhat.com/viewcvs/éroot=core).

Les façons d'installer en fakeroot:

*   Installation contrôlée en utilisant une variable d'environnement.

    Comme l'approche fakeroot est l'approche la plus utilisée pour la gestion des paquets, beaucoup
    dresponsables de paquets utilisent une façon simple de contrôler l'installation. De plus, vu que beaucoup
    de paquets utilisent autotools, la variable d'environnement pour 
    fakeroot est DESTDIR. Donc l'installation pour ces paquets se fera avec

    ```bash
    make DESTDIR=$FAKEROOT install
    ```

    Certains paquets peuvent utiliser une autre variable d'environnement pour fakeroot. Par exemple, glibc
    utilise install_root. Vérifiez le fichier INSTALL du paquet pour trouver la variable d'environnement.
*   Installationen en modifiant le prefix pendant l'installation.

    Pour certains paquets anciens, une façon simple d'installer les paquets dans 
    fakeroot est de changer la variable prefix lors de l'installation.

    ```bash
    make prefix=$FAKEROOT/usr install
    ```
*   Installation manuelle.

    Certains paquets sont mal fichus. Parfois il est plus simple d'installer tous les fichiers manuellement.
    C'est d'autant plus vrai pour les paquets qui n'installent que quelques fichiers.

Les choses auxquelles prendre garde :

*   Parfois les paquets ne créent pas de fichier de destination avant d'installer des fichiers dans un répertoire.
    Normalement, quand on installe dans /, ce n'est pas un problème vu que le répertoire de destination
    comme /usr/bin existe déjà. Mais quand on installe avec fakeroot, vous devez créer ces répertoires manuellement
    en utilisant `install -d`.
*   Une petite majorité des paquets code en dur le répertoire de fakeroot dans les fichiers.
    C'est généralement une bonne idée de lire attentivement les journaux.
*   Les paquets qui installent des pages d'info créent un fichier /usr/share/info/dir. Ce fichier
    devrait être recréé après l'installation du paquet. voir le chapitre de LFS sur texinfo pour savoir comment régénérer
    ce fichier.
*   Les modules Perl ont deux problème. Le premier est que le fichier .packlist code en dur le répertoire
    FAKEROOT. Il faut le supprimer en utilisant sed. De plus, chaque module installe un fichier partagé
    perllocal.pod. Si le module est installé directement dans /, l'installation ajoutera les nouvelles entrées dans le fichier.
    Comme nous installons en fakeroot, vous devez manutellement modifier le fichier perllocal.pod installé sur le système
    et empêcher l'installation du fichier perllocal.pod installé par le module.
*   Les paquets qui installent des schémas GConf installent les schémas automatiquement pendant le 
    make install. Comme nous avons installé en fakeroot, cette étape nécessite d'être réalisée dans l'étape de configuration
    post installation. Pour désactiver l'installation des schémas, soit activez la variable
    `GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL` sur vrai pendant le make install ou passez le paramètre `--disable-schema-install`
    à configure.
*   Pour les paquets qui installent des catalogues scrollkeeper, le processus scrollkeeper-update
    doit être désactivé pendant l'installation et la base de données scrollkeeper
    doit être mise à jour dans la configuration post installation.
*   Pour les paquets qui installent des polices, fc-cache et gtk-updated-icon-cache doivent être appelés pendant la phase de configuration
    post installation.
*   Pour les paquets qui installent des chargeurs et des immodules gtk, la configuation doit être recréée manuellement en appelant 
    gtk-query-immodules et gdk-pixbuf-query-loaders.
*   Pour les paquets qui installent des fichiers mime db, la base de données mime doit être mise à jour manuellement en appelant update-mime-database.

Si vous vous sentez aventurier, vous pouvez combiner cette astuce avec l'astuce des utilisateurs-paquets pour avoir
le programme de gestion de paquets ultime :-) J'ai utilisé cela pendant quelque temps et même si c'est un peu complexe,
cela m'a bien servi.

Historique des modifications
============================

*  [09-12-2005]
   *   Version initiale de l'astuce.


---
layout: astuce
use-site-title: true
title: Impression depuis zéro
author: Uli Fahrenberg &lt;uli at math dot auc dot dk&gt; ; Declan Moriarty &lt;declan dot moriarty at ntlworld dot ie&gt;
translator: Julien Lepiller &lt;jlepiller at linuxfromscratch point org&gt;
date: 2019-03-01
license: GFDL-1.2
synopsis: Comment paramétrer son ordinateur pour l'impression
description: "
Voici un guide pour paramétrer son ordinateur pour l'impression. Il est aussi
utile pour régler les problèmes. Nous commençons par la configuration du noyau,
et à la fin vous pourrez (normalement) imprimer un fichier ps avec votre
imprimante.
"
---

Prérequis
=========

ghostscript (AFPL, GNU, ESP, peu importe; voir le livre [BLFS](/view/blfs-stable)
pour plus de détails).

Facultatifs : a2ps et psutils (pareil : voir le livre [BLFS](/view/blfs-stable)
pour plus de détails).


Astuce
======

Paramétrer son PC Linux pour l'impression peut être très facile (avec un peu
de chance). Si votre installation est simple (un ordinateur et une imprimante
avec laquelle vous voulez communiquer, et c'est tout), alors cette astuce est
pour vous. Si vous voulez partager votre imprimante avec d'autres ordinateurs
ou faire d'autres trucs rigolos, je ne peux pas vous aider.


Première étape
--------------

Vérifiez que vous avez le support de l'impression dans votre noyau, soit
intégré, soit dans un module. Ma configuration ressemble à cela ; vous
n'avez pas forcément besoin des deux dernières options PARPORT :

    CONFIG_PARPORT=y
    CONFIG_PARPORT_PC=y
    CONFIG_PARPORT_PC_CML1=y
    CONFIG_PARPORT_SERIAL=y
    CONFIG_PRINTER=y


Deuxième étape
--------------

Vérifiez que votre imprimante fonctionne et est connectée :

```bash
echo -en "blah\f\r" > /dev/lp0
```

(replacez lp0 par le port auquel votre imprimante est connectée)

Si votre imprimante crache une feuille avec le mot « blah » dessus, très bien.
Sinon, vous pouvez être inquiets.

Si votre imprimante est une Epson, la commande ci-dessus ne fonctionnera sans
doute pas. Dans ce cas, l'information suivante partagée par Jeroen Coumans
pourrait vous aider :

    Les imprimantes Epson ne fonctionnent pas sans un caractère spécial
    d'abord. L'utilitaire escputil de gimp-print est souvent utilisé pour
    cela (c'est sans doute possible de faire ça sans d'abord installer
    gimp-print, mais je ne sais pas comment). Voici la commande qui marche
    chez moi (Stylus C80) :
    
    escputil -r /dev/usb/lp0 -i

Vous devriez peut-être installer gimp-print de toute façon (mais je ne vous
dirais pas comment (ça a l'air facile quand même)), comme les imprimantes
Epson sont assez mal supportées par Ghostscript. Sinon, pour compiler seulement
escputil,

```bash
./configure && make -C lib && make -C src/escputil
```

fonctionne chez moi.


Troisième étape
---------------

Installez votre version favorite de Ghostscript.


Quatrième étape
---------------

Saisissez <code>gs -h</code> dans un shell et sélectionnez votre imprimante
à partir des pages de pilotes proposés. Si votre pilote n'apparaît pas,
repartez à l'étape 3 et installez une autre version de Ghostscript. Les
version AFPL et ESP de Ghostscript ont des différences dans le support des
imprimantes.

Si vous ne savez pas quel pilote sélectionner pour que votre imprimante
fonctionne, visitez le site de [linux printing](http://www.linuxprinting.org/)
et recherchez votre imprimante dans leur base de données. Ils vous diront
peut-être que vous avez besoin de logiciels supplémentaires comme HPIJS,
pnm2ppa ou d'autres. Si vous avez besoin de logiciels supplémentaires, à vous
de vous débrouiller. Ici nous ne parlons que des imprimantes supportées par
Ghostscript (mais continuez à lire, l'astuce peut toujours vous être utile).


Cinquième étape
---------------

Testez votre configuration. Récupérez un fichier ps (un pdf fonctionnera aussi)
et lancez

```bash
gs -q -dBATCH -dNOPAUSE -dSAFER -sDEVICE=<votre-pilote-d'imprimante> \
-sOutputFile=/tmp/testit <votre-fichier>
```

Vous trouverez des fichiers ps dans le répertoire des exemples de Ghostscript.
Chez moi c'est <code>/usr/share/ghostscript/8.00/examples/</code>. Cette
commande devrait vous donner un fichier binaire (probablement très gros)
<code>/tmp/testit</code> ; si vous avez de la chance, lancer la commande

```bash
file /tmp/testit
```

indentifiera ce fichier comme des données d'impression. Elle vous dira aussi
peut-être que la taille du papier ne correspond pas à ce que vous avez dans
l'imprimante. Si c'est le cas, ajoutez
<code>-sPAPERSIZE=&lt;votre-taille-de-papier&gt;</code> à la commande gs.
Les autres options signifient :

* _-q_ : dit à gs de ne rien afficher et l'empêche de chercher X.
* _-dBATCH_ : dit à gs de quitter après le traitement — c'est toujours une bonne idée.
* _-dNOPAUSE_ : gs n'attendra pas de touche de clavier après chaque page.
* _-dSAFER_ : évite que gs ne supprime ou ne tronque quoi que ce soit.
* _-sDEVICE=_ : votre pilote d'imprimante. Soyez précis et attentif à la casse.
         gs est stupide. Utilisez l'orthographe donnée par <code>gs -h</code>.
* _-sOutputFile=_ : écrire dans ce fichier de sortie (bravo pour votre perspicacité !).

Si tout à l'air bon, vous pouvez lancer

```bash
cat /tmp/testit > /dev/lp0
```

(de nouveau, replacez lp0 par le port auquel votre imprimante est connectée).
Cela devrait imprimer &lt;votre-fichier&gt; sur &lt;votre imprimante&gt;.

Les deux commandes ci-dessus peuvent vous donner des soucis avec les permissions
si vous les lancez en tant qu'utilisateur normal. Si la commande gs vous
pose problème lancez :

```bash
chmod 4777 /tmp 
```

et donnez-vous une baffe pour être sur une machine inutilisable pour les
utilisateurs normaux. Si la commande cat panique avec « Impossible d'écrire
sur /dev/lp0 » ou un message similaire, vous pouvez toujours décider que
seul root peut accéder à l'imprimante ou vous pouvez décider d'introduire
un problème (mineur) de sécurité avec

```bash
chmod a+rw /dev/lp0
```

Si vous voulez utiliser le script lpr ci-dessous pour l'impression, vous
devrez faire cela.


Sixième étape (facultative)
---------------------------

Si vous êtes arrivé jusqu'ici, votre imprimante fonctionne. Vous pouvez
partir maintenant ; vous avez imprimé un document à l'étape 5 après tout.
Si vous voulez quelque chose de plus pratique, le script ci-dessous vous
donnera la commande lpr, à utiliser soit en appelant

```bash
lpr <some-file>
```

soit en ayant lpr comme (dernier) élément d'un pipe. Cela devrait vous
permettre d'utiliser plus plupart des boutons / commandes « Imprimer » de
vos applications directement.

Un autre avantage à ce script est qu'il n'utilise par de fichier temporaire,
ce qui évite de remplir /tmp avec plein de saletés et certains problèmes de
sécurité. D'un autre côté, si vous voulez que les utilisateurs ordinaires
puissent imprimer, vous devrez leur donner accès à /dev/lp0 (ou n'importe quel
port que votre imprimante utilise), comme on l'a dit plus haut.

Voici le script. Comme vous pouvez le voir, vous devrez l'ajuster un peu
pour votre cas. Je n'ai encore jamais recontré d'application qui n'imprime pas
avec ce script. Si c'est votre cas, dites-le moi.

```bash
cat > /usr/bin/lpr << "EOF"
#!/bin/bash

####################  Ajustez en fonction de vos besoins et de vos envies
DEVICE=ljet4
LP=lp0
PAPER=a4
LOCKFILE=/tmp/.${LP}-lock
####################  Fin des ajustements

# Imprime un fichier sur l'imprimante $DEVICE connectée sur /dev/$LP, 
# avec la taille de papier $PAPER.
# Utilisation : lpr <fichier ps ou pdf> ( ou cat <fichier ps ou pdf> | lpr )

# Uli Fahrenberg, early 2003. Ce fichier est dans le domaine public.

if [ -e $LOCKFILE ] ; then
    echo "Erreur : imprimante verrouillée  ($LOCKFILE existe)"
    exit 1
fi

FILE=$1
if ! [ X$FILE = X ] ; then ### On a un nom de fichier en argument.
    if ! [ -r $FILE ] ; then
        echo "Erreur : Impossible de lire le fichier $FILE"
        exit 1
    fi

    FTYPE=$(file -bL $FILE | awk '{print $1}')
    if ! [ $FTYPE = 'PDF' ] && ! [ $FTYPE = 'PostScript' ] ; then
        echo "Erreur : $FILE n'est pas un fichier PS ou PDF."
        exit 1
    fi

    ### Commentez ceci si vous voulez que lpr soit discret :
    echo -n "Printing $FILE... "

    (touch $LOCKFILE ; \
    trap 'rm -f $LOCKFILE' EXIT ; \
    gs -q -dBATCH -dNOPAUSE -dSAFER -sDEVICE=$DEVICE \
        -sPAPERSIZE=$PAPER -sOutputFile=- $FILE \
    > /dev/$LP ) &

    ### Sans le sleep, certaines applications suppriment $FILE plus vite
    ###    que gs ne peut le lire :
    sleep 1
    ### Commentez ceci si vous voulez que lpr soit discret :
    echo "Done."

else ### On n'a pas de nom de fichier en argument, donc on essaye depuis stdin
    DATA="$(</dev/stdin)"
    (touch $LOCKFILE ; \
    trap 'rm -f $LOCKFILE' EXIT ; \
    echo "$DATA" \
    | gs -q -dBATCH -dNOPAUSE -dSAFER -sDEVICE=$DEVICE \
        -sPAPERSIZE=$PAPER -sOutputFile=- - \
    > /dev/$LP ) &
fi
EOF
chmod 755 /usr/bin/lpr
```


Septième étape (encore plus facultative)
----------------------------------------

Donc maintenant vous pouvez imprimer les fichiers pdf et postscript sur votre
imprimante. Si vous voulez imprimer d'autres types de fichiers (des fichiers
texte bruts, disons des astuces LFS...), vous pourriez apprécier utiliser
le paquet a2ps (AnyToPS). Aussi, pour manipuler des fichiers postscript,
le paquet psutils est une bonne chose à installer. Ces deux paquets sont
présents dans le livre BLFS.


Remerciements
-------------

Cette astuce est une édition retravaillée de _Printing Minority Report_, une
astuce sur l'impression à l'origine écrite par Declan Moriarty. Il a fait
tout le travail de fond des section 1 à 5 ; je lisais et utilisais son astuce
et je préparais le script lpr, quand tout d'un coup Declan m'a passé le
flambeau.

Bill Maltby et Jeroen Coumans ont aussi contribués un peu.

Modifications
-------------

* _2003-05-24_ Adoption de l'astuce de Declan.
* _2003-06-29_ pfs.txt version 1 soumise.
* _2003-07-01_ Insertion de <code>sleep 1</code> dans le script, pour que
  gv soit content.
* _2003-09-30_ Conversion en un format plus joli.

---
layout: astuce
use-site-title: true
title: Eudev
author: James Powell # &lt;james4591 [at] hotmail [dot] com&gt;
translator: Denis Mugnier
date: 2014-04-04
license: MIT
synopsis: Utiliser Eudev en alternative à Systemd
description: "
<p>
Systemd, bien qu'étant un nouveau démon de contrôle du système excitant, peut souvent
être très envahissant dans sa façon de fonctionner avec les autres outils du système,
les services démon, et même avec les différents ensembles de programmes. Pas seulement
parce que systemd est plus grand de 410 Mo et demande 4,5 SBU de temps pour la construction
et l'installation.
</p>

<p>
Eudev est considérablement plus petit à 7.4Mo, plus rapide à se construire avec 0.3 SBU, et
fournit les mêmes fonctionnalités que systemd-udev utilisé par LFS/BLFS sans les fonctionnalités
de gestion de service, l'init alternatif, et n'est pas envahissant pour l'environnement
en demandant lui-même d'être une dépendance dur.
</p>

<p>
il y a d'autres raisons qui font qu'une personne choisi de ne pas utiliser systemd qui sont
plus personnel, politique, et sont des opinions dirigés contre le mainteneur de systemd et
le but ultime de systemd, mais elles sont mieux ailleurs dans un autre sujet de discussions.
</p>
"
---

Remerciements
=============

je veux remercier Bruce Dubbs, un des mainteneurs du projet LFS pour m'avoir donné la chance de ne pas seulement
contribuer à LFS, mais de donner aux utilisateurs de LFS, encore un autre choix pour ce qu'ils veulent
dans leur système, aussi bien que l'équipe LFS complète pour donner aux utilisateurs avancés de Linux
une chance de construire leur propre système personnalisé.

Prérequis
=========

Aucun. Basiquement, vous pouvez construire LFS normalement jusqu'au chapitre 6.67 et ensuite passer
sur cette astuce.

Il est à noter que si eudev est une implémentation plus légère vous pouvez également sauter des parties
du livre LFS...

| Paquet      | Temps    | Où            |
|-------------|----------|---------------|
| Attr        | 4.5 SBUs | Chapitre 6.22 |
| Acl         | 0.2 SBUs | Chapitre 6.23 |
| libcap      | 0.1 SBUs | Chapitre 6.24 |
| expat       | 0.1 SBUs | Chapitre 6.40 |
| XML::Parser | 0.2 SBUs | Chapitre 6.43 |
| D-Bus       | 0.4 SBUs | Chapitre 6.68 |


Soit au total, ajouté à systemd, 9.9 SBU et environ 100 Mo de temps et d'espace en moins.

Le reste de la configuration du système est sensiblement la même.


Comment faire
=============

Commencez par télécharger une copie de eudev sur votre système, et ses pages de manuel:

Eudev (1.5.3) - 1,684 Ko
------------------------

Page du projet&nbsp;: [http://www.gentoo.org/proj/en/eudev/](http://www.gentoo.org/proj/en/eudev/)<br />
Téléchargement&nbsp;: [http://dev.gentoo.org/~blueness/eudev/eudev-1.5.3.tar.gz](http://dev.gentoo.org/~blueness/eudev/eudev-1.5.3.tar.gz)<br />
Somme de contrôle MD5&nbsp;: 51380938b489385cc394f4ebabc048f0

Pages de manuel Eudev (1.5.3) - 6 Ko
------------------------------------

Téléchargement&nbsp;: [http://anduin.linuxfromscratch.org/sources/other/eudev-1.5.3-manpages.tar.bz2](http://anduin.linuxfromscratch.org/sources/other/eudev-1.5.3-manpages.tar.bz2)<br />
Somme de contrôle MD5&nbsp;: eaa5b9af344e958c29288e5376b97a28

Pendant un temps bref où il a été inclu dans LFS, Eudev était au chapitre 6.62
et ces instructions sont les mêmes instructions conservés du livre. Notez que les futures versions
de Eudev peuvent nécessiter des instructions différentes, donc elles marcheront avec 1.5.3 seulement.

6.62.1 Installation de Eudev
----------------------------

En premier, nettoyez certains avertissements et corrigez un script de test&nbsp;:

```bash
sed    -i '/struct ucred/i struct ucred;' src/libudev/util.h
sed -r -i 's|/usr(/bin/test)|\1|'         test/udev-test.pl
```

Preparez Eudev pour la compilation&nbsp;:

```bash
  BLKID_CFLAGS=-I/tools/include       \
  BLKID_LIBS='-L/tools/lib -lblkid'   \
  ./configure --prefix=/usr           \
         --bindir=/sbin          \
         --sbindir=/sbin         \
         --libdir=/usr/lib       \
         --sysconfdir=/etc       \
         --libexecdir=/lib       \
         --with-rootprefix=      \
         --with-rootlibdir=/lib  \
         --enable-split-usr      \
         --enable-libkmod        \
         --enable-rule_generator \
         --disable-introspection \
         --disable-keymap        \
         --disable-gudev         \
         --disable-gtk-doc-html  \
         --with-firmware-path=/lib/firmware
```

Compilez le paquet&nbsp;:

```bash
make
```

Créez maintenant les répertoires suivants qui sont utilisés pour les tests, mais qui seront aussi utilisé comme une partie de l'installation&nbsp;:

```bash
mkdir -pv /lib/{firmware,udev/devices/pts}
mkdir -pv /lib/firmware
mkdir -pv /lib/udev/rules.d
mkdir -pv /etc/udev/rules.d
```

Pour tester les résultats, lancez&nbsp;:

```bash
make check
```

Installez le paquet:

```bash
make install
```

Maintenant, installez les pages de manuel&nbsp;:

```bash
tar -xvf ../eudev-1.5.3-manpages.tar.bz2 -C /usr/share
```

Finallement, créez quelques rêgles personnalisées qui ne sont pas faites par défaut&nbsp;:

```bash
cat > /etc/udev/rules.d/55-lfs.rules << "EOF"
# /etc/udev/rules.d/55-lfs.rules: Définitions de rêgles pour LFS.

# Péripphérique du coeur du noyau

# Cela initialise l'horloge système dès que /dev/rtc devient disponible.
SUBSYSTEM=="rtc", ACTION=="add", MODE="0644", RUN+="/etc/rc.d/init.d/setclock start"
KERNEL=="rtc", ACTION=="add", MODE="0644", RUN+="/etc/rc.d/init.d/setclock start"

# Périphérique de communication

KERNEL=="ippp[0-9]*",       GROUP="dialout"
KERNEL=="isdn[0-9]*",       GROUP="dialout"
KERNEL=="isdnctrl[0-9]*",   GROUP="dialout"
KERNEL=="dcbri[0-9]*",      GROUP="dialout"
EOF
```

6.62.2 Contenu de Eudev
-----------------------

*   _Programmes installés_&nbsp;: udevadm et udevd
*   _Bibliothèques installées_&nbsp;: libudev.so
*   _Répertoires installés_&nbsp;: /etc/udev, /lib/udev et /lib/firmware

### Descriptions courtes

#### udevadm

Outil d'administration générique de udev&nbsp;: contrôle le démon udevd, fournit des infos de la base de données
Udev, surveille les évènements "uevents", attends la fin des évènements "uevents", teste la configuration d'Udev
et déclenche les évènements "uevents" pour un périphérique donné.

#### udevd

Un démon qui écoute les évènements "uevents" sur le socket réseau, crée les périphériques et
lance les programmes de configuration externes en réponse à ces évènements "uevents".

#### libudev

Une bibliothèque interface vers les informations des périphériques udev

#### /etc/udev

Contient les fichiers de configuration de Udev, les permissions des périphériques, et les rêgles pour le nommage des périphériques

Fin
===

Voilà. vous avez maintenant Eudev d'installé sur votre système.

Si et quand vous voudrez reconstruire Eudev pour le support gudev, le processus de compilation
est le même excepté que vous devez simplement effacer ces lignes&nbsp;:

```bash
         --disable-introspection \
         --disable-keymap        \
         --disable-gudev         \
```

Cela demandera que vous installiez depuis BLFS les paquets suivants&nbsp;:

- GLib                  - requis pour gudev
- Gperf                 - requis pour keymap
- gobject-introspection - requis pour gir-data

Merci et profitez !

Changelog
=========

2014-04-04 - 0.0.1 - Premier brouillon


Licence
=======

Copyright (c) 2014 James Powell

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

---
layout: astuce
use-site-title: true
title: La gestion des paquets par la méthode des utilisateurs 
author: Matthias S. Benkmann &lt;article at winterdrache point de&gt;
translator: Denis Mugnier &lt;myou72 at orange point fr&gt;
date: 2007-10-20
synopsis: plus de contrôle et de gestion des paquets à l'aide des utilisateurs-paquets (v1.4)
description: Vous voulez savoir à quels paquets vos fichiers appartiennent&nbsp;? Vous souhaitez désinstaller un logiciel qui n'a pas de make uninstall&nbsp;? Vous êtes gêné par les programmes installés avec un setuid root derrière votre dos&nbsp;? Vous n'aimez pas que les fichiers des paquets écrasent tranquillement d'autres paquets&nbsp;? Vous n'aimez pas les gestionnaires de paquets comme RPM&nbsp;? VOUS VOULEZ LE CONTRÔLE TOTAL EN UTILISANT UNIQUEMENT LES COMMANDES INTEGREES A UNIX&nbsp;?
license: CC-BY-SA-3
---

Pièce jointe
============

http:/www.fr.linuxfromscratch.org/view/astuces/more_control_helpers.tar.bz2

Pré-requis
==========

*   Pour une utilisation avec le livre LFS 6.2&nbsp;: Cerveau.
*   Pour une utilisation avec les livres LFS supérieurs à 6.2&nbsp;:  Cerveau (éveil,
    bonne condition de travail).

Astuce
======

Contenu
-------

*  1. Préface
*  2. Aperçu
*  PARTIE 1&nbsp;: Informations générales
   * 3. Utilisateurs-paquets
      * 3.1 Introduction
      * 3.2 Nom d'utilisateur
      * 3.3 Groupes
      * 3.4 Répertoire Home
   * 4. Problèmes communs
      * 4.1 Introduction
      * 4.2 Procédure générale
      * 4.3 Changements des autorisations
      * 4.4 Changements d'appartenance
      * 4.5 Ecrire dans les Répertoires de non-installation
      * 4.6 Supprimer ou Remplacer un fichier
      * 4,7 /sbin/ldconfig
      * 4.8 Quelles commandes lancées en tant qu'utilisateur-paquet
   * 5. L'archive more_control_helpers
      * 5.1 Aperçu
      * 5.2 Les enveloppes
      * 5.3 add_package_user / install_package
      * 5.4 forall_direntries_from
      * 5.5 uninstall_package
      * 5.6 list_suspicious_files / list_suspicious_files_from
      * 5.7 list_package
      * 5.8 grep_all_regular_files_for
      * 5.9 Le répertoire etc
      * 5.10 Fichiers Temporaires
*  PARTIE 2&nbsp;: Spécificités LFS
   * 6. Phase Pré-chroot (chapitre 5)
   * 7. Phase de chroot (chapitre 6)
      * 7.1 Préparatifs
      * 7.2 Procédure pas à pas&nbsp;: Installation de linux-libc-headers
      * 7.3 Problèmes connus avec des paquets LFS
   * 8. Vérification de propreté
      * 8.1 fichiers suspects
      * 8.2 Références aux fichiers temporaires
*  Annexes
   * A. Problèmes de sécurité
        * 9.1 NFS
        * 9.2 Démons
   * B. Catégories de paquet
   * C. Remerciements et modifications
  

## 1. Préface

Disons que j'ai écrit un programme que vous souhaitez utiliser. Pour vous
faciliter les choses, je viens l'installer pour vous. Voulez-vous me donner le
mot de passe root, puis quitter la salle? Non? Alors pourquoi vous le donnez à
de parfaits étrangers que vous n'avez jamais vus de votre vie, pour installer
des paquets logiciels chargés à partir d'un serveur Internet, qui viennent avec
aucune garantie et n'ont même pas la liste de leurs contenus dans le fichier
README, mais qui seront heureux de se répandre sur votre système&nbsp;?

La raison pour laquelle les admins Unix qui ne feraient même pas confiance à
leur employeur avec un compte utilisateur ordinaire, exécutent
sans précautions des scripts d'installation complexes et incompréhensibles avec
les privilèges root est un mystère.

Les utilisateurs et les groupes sont le principe de sécurité de base dans un
système Unix. On les utilise avec succès depuis longtemps pour surveiller
qui a créé un fichier et pour contrôler qui est autorisé à le supprimer ou le
modifier. Mais ce contrôle n'est imposé que sur les fichiers des utilisateurs
ordinaires. Quel gâchis! Je suggère d'étendre ce contrôle à tous les fichiers du
système.

L'idée générale est de créer des utilisateurs-paquets, c'est à dire des comptes
utilisateurs avec des droits limités, pour construire et installer des
logiciels, plutôt que de faire ces tâches en tant que root. Non seulement cela
vous donne plus de contrôle sur ce que les scripts de construction et
d'installation peuvent ou non faire, mais cela peut aussi servir de gestion des
paquets du système tout à fait utile.

## 2. Aperçu

Cette astuce est divisée en 3 parties. La première partie contient des
informations générales a propos de la méthode d'utilisateurs-paquets. Cette
partie est la partie la plus importante de l'astuce. Lisez-la soigneusement. La
deuxième partie explique comment appliquer la méthode des utilisateurs-paquets à
la construction d'un système LFS. Enfin, la partie 3 de cette astuce est
l'annexe avec de l'information qui ne saurait pas être placée ailleurs ou qui
n'est pas d'intérêt général.

Il est inévitable que la partie 2 sera devenu obsolète avec le temps et les
modifications du livre LFS et les nouvelles versions des logiciels utilisés avec
LFS qui seront publiées.Je ne ferai aucune tentative pour suivre ces
changements. Lorsque quelqu'un signale un problème avec un paquet je vais
l'incorporer dans l'astuce, mais des modifications plus importantes qui
pourraient être nécessaires en raison des changements dans la méthodologie de
construction de LFS pourraient prendre un certain temps pour être intégrées.
Ceci car (mis à part le manque de temps), je considère la partie 2 comme un
sujet bonus qui aide les gens à démarrer, mais qui n'est pas indispensable. La
partie 1 décrit les concepts, qui sont indépendants des versions de paquets ou
du livre LFS, et vous devrez vous appuyer sur ces informations chaque fois que
la partie 2 échouera. N'oubliez pas que la partie 2 ne traite que des paquets
utilisés par le livre LFS. Pour tous les autres paquets que vous installerez sur
votre système après, même une mise à jour de la partie 2 ne vous offrirait de
toute façon aucune aide.

Le paragraphe précédent peut sembler décourageant, et au fur et à mesure que
vous lirez l'astuce, il est possible que vous ayez l'impression que la méthode
de l'utilisateur-paquet est compliquée, pose beaucoup de problèmes difficiles
et représente globalement beaucoup trop de travail pour quiconque, sauf pour un
admin système pur et dur avec une expérience de programmation. Mais vous auriez
tort. Tout d'abord, beaucoup de choses vécues comme des problèmes d'installation
lorsque l'on travaille avec la méthode de l'utilisateur-paquet sont en fait des
caractéristiques souhaitables. Si 'make install' échoue pour un paquet, car il
tente d'installer le fichier d'un paquet avec le même nom qu'un fichier existant
déja, vous ne devez pas maudire le fait que vous ayez à passer du temps pour
résoudre ce problème. Au contraire, vous devriez être heureux d'avoir été averti
de cette collision qui, si elle était passée inaperçue, aurait pu avoir mis le
bazar dans votre système avec des problèmes plus ou moins subtiles.
Deuxièmement, le système de l'utilisateur-paquet n'est pas une approche
tout-ou-rien. Il fonctionne sur une base par paquets. Si un paquet vous donne
trop de difficultés, vous pouvez toujours décider d'abandonner et terminer
l'installation en tant que root. Enfin, l'archive more_control_helpers fournie
avec cette astuce contient plusieurs scripts utiles qui permettent d'automatiser
de nombreux aspects de l'installation du logiciel en tant qu'utilisateur-paquet
et, de concert avec les conseils donnés dans cette astuce, ajouter un grand
nombre d'avantages au système de l'utilisateur-paquet. Donc, n'émettez pas de
jugement avant d'avoir lu au moins entièrement la partie 1, y compris la
description de "more_control_helpers".

PARTIE 1&nbsp;: Informations générales
=================================


## 3. Utilisateurs-paquets

### 3.1 Introduction

L'idée de base de ce principe est facile à expliquer. Chaque paquet appartient à
un certain "utilisateur-paquet". Lorsque vous installez un paquet, vous
construisez et installez le paquet en tant que cet utilisateur-paquet, donc tous
les fichiers qui sont installés appartiennent à l'utilisateur-paquet. En
conséquence, toute la gestion des paquets peut se faire aisément grâce à
l'utilisation des outils standards de la ligne de commande. Un simple `ls-l <fichier>`
vous dira, par exemple, à quel paquet appartient ce &lt;fichier&gt; et une
commande `find -user ...` vous permet d'effectuer une opération sur tous les
fichiers appartenant à un certain paquet, par exemple, les supprimer pour
désinstaller le paquet.

Mais les utilisateurs-paquets ne sont pas seulement bons pour la gestion des
paquets. Les utilisateurs-paquets n'ayant pas de privilèges root, l'installation
d'un paquet est limitée à ce qu'elle peut faire. Une chose Mais les
utilisateurs-paquets ne sont pas seulement bons pour la gestion des paquets. Les
utilisateurs-paquets n'ayant pas de privilèges différents paquets qui veulent
installer un binaire, une bibliothèque ou un fichier d'en-tête du même nom sont
plus fréquents que vous pourriez le penser. Avec les utilisateurs-paquets vous
ne courrez jamais le risque d'une installation d'un paquet B qui détruit les
fichiers d'un autre paquet silencieusement sans se faire remarquer. Toute
tentative pour le faire au cours de l'installation du paquet B va provoquer un
message «Autorisation refusée» ou une erreur "Opération non permise" de sorte
que vous ayez la chance de prendre les mesures appropriées.
Une autre chose que les utilisateurs-paquets ne sont pas autorisés à faire est
d'installer des binaires avec un setuid root. La décision de rendre un binaire
avec un setuid root est également quelque chose qu'un administrateur prudent ne
veut pas laisser au créateur d'un logiciel.

En général les comptes utilisateurs-paquets n'ont pas mot de passe afin que
seul root puisse faire un su d'un utilisateur-paquet, ce qui garantit que les
utilisateurs-paquets ne puissent pas ouvrir un shell supplémentaire dans le
système et nuire à la sécurité. Mais vous "pouvez" quand même mettre des mots de
passe partout de façon à permettre à un co-admin d'être en mesure d'installer et
d'entretenir certains logiciels sans avoir accès au compte root réel. Ce
co-admin pourrait, par exemple, installer, supprimer, modifier des bibliothèques
supplémentaires qui pourraient être nécessaires pour son groupe de travail. Il
serait par contre incapable d'enlever ou de modifier les bibliothèques qui ne
lui appartiennent pas, comme libc par exemple.


### 3.2 Nom d'utilisateur

Vous n'avez pas besoin de vous casser la tête à essayer de trouver des noms
d'utilisateurs-paquets ayant 8 caractères. J'utilise toujours le nom du paquet
sans le numéro de version, en incluant les tirets et un possible dépassement de
la longueur de 8 caractères, par exemple "Util-linux", et depuis les années que
j'utilise cette méthode, je n'ai pas rencontré de problèmes, ni vu quiconque
rapporter des ennuis. La limite de 8 caractères sur les noms d'utilisateur
semble être une chose du passé.

{: .alert .alert-info}
__ASTUCE__&nbsp;:
Vous pouvez utiliser la fonction de complétion programmable de bash pour vous
éviter quelques frappes lors de la saisie des commandes qui prennent un nom
d'utilisateur comme argument, comme Su, finger ou pinky. La commande

```bash
complete -o default -o nospace -A user su finger pinky
```
      
dit à bash de compléter avec Tab les mots comme des noms d'utilisateur pour
les commandes su, finger ou pinky. Avec cela en place, vous pouvez simplement
taper `su linux-li <TAB>` et bash viendra compléter en `su linux-libc-headers`
(en supposant que vous ayez un utilisateur-paquet nommé "linux-libc-headers").

*  "-o default" indique à bash que s'il n'existe pas de nom d'utilisateur
   convenable, il devra essayer la complétion par défaut.
*  "-o nospace" empêche l'ajout d'un espace après le mot complété.
  
C'est une commande très utile à mettre dans le .bashrc et le .bash_profile
de root.
  
D'ailleurs à l'adresse http://freshmeat.net/projects/bashcompletion/ vous
pourrez trouver un projet qui offre des complétions sophistiquées pour de
nombreuses autres commandes.
  
Ou passer à zsh (http://freshmeat.net/projects/zsh/). C'est plus puissant et
moins buggué que bash.
</div>

### 3.3 Groupes

Chaque utilisateur-paquet appartient au moins à 2 groupes. Un de ces groupes est
le groupe "install", dont tous les utilisateurs-paquets (et les
utilisateurs-paquets seulement) font partis. Tous les répertoires que les
paquets sont autorisés à installer là-dedans appartiennent au groupe install.
Ceci comprend les répertoires tels que /bin et /usr/bin, mais exclut des
répertoires comme /root ou /. Les répertoires appartenant au groupe install sont
toujours inscriptibles pour le groupe. Cela suffirait pour les aspects de
gestion des paquets, mais sans plus de préparation cela ne donnerait pas assez
de sécurité ou de contrôle car chaque paquet pourrait remplacer les fichiers
d'un paquet différent (le changement serait cependant visible dans la sortie de
"ls-l"). C'est pourquoi, tous les répertoires d'installation obtiennent
l'attribut sticky. Cela permet aux utilisateurs de créer de nouveaux fichiers et
de les supprimer ou de modifier leurs propres fichiers dans le répertoire, mais
les fichiers des autres utilisateurs ne peuvent pas être modifiés ou supprimés.
Dans la suite de cette astuce, chaque fois que «répertoire d'installation» est
utilisé, il se réfère à un répertoire qui appartient au groupe install, qui est
autorisé en écriture pour le groupe et qui possède l'attribut sticky. En
d'autres termes, pour passer &lt;dir&gt; en répertoire d'installation vous devez faire

```bash
chgrp install <dir> && chmod g+w,o+t <dir>
```

Bien que le groupe d'install est crucial pour le système de
l'utilisateur-paquet, il est mis en oeuvre comme un groupe supplémentaire,
plutôt que comme le groupe principal de l'utilisateur-paquet. Cela a au moins
deux avantages. Le premier avantage est que cela facilite l'obtention d'une
liste de tous les paquets installés sur le système avec la commande

```bash
grep install /etc/group
```

Plus important cependant, le groupe principal est celui à qui les fichiers créés
par l'utilisateur-paquet appartiennent. Ainsi, il sera affiché dans la sortie de
"ls -l" et il est possible de le trouver avec des tests de "-group". Cela est
très utile à des fins organisationnelles. Voici quelques suggestions pour savoir
comment utiliser le groupe principal.

1. nom du groupe = nom d'utilisateur 

   Selon ce schéma, l'utilisateur-paquet pour bash serait bash:bash.
   "ls -l /bin/bash" afficherait quelque chose comme ceci
   
   ```
   -rwxr-xr-x  1  bash  bash  1731859  30 février 2005  /bin/bash
   ```
   
   Un avantage important de ce shéma est que les informations utilisateurs ne
   sont pas perdues lorsque vous attribuez un setuid root au fichier, ce qui
   nécessite un changement du propriétaire du fichier. En raison de cet
   avantage, ce schéma est celui recommandé par cette astuce. Toutefois, les
   instructions de l'astuce fonctionneront parfaitement si vous choisissez un
   schéma différent.

2. Nom du groupe  = catégorie de paquets 

   Selon ce schéma, vous auriez certaines catégories pour les paquets, tels que
   jeux, system, net, ... et bash, étant un programme du système, il
   appartiendrait éventuellement au groupe system, de sorte que "ls -l
   /bin/bash" afficherait quelque chose comme
   
   ```
   -rwxr-xr-x  1  bash   system 1731859   4 juillet 1776 /bin/bash
   ```
   
   Ce système est agréable, mais probablement pas aussi utile que le 1
   ci-dessus, à moins que vous ayez une réelle utilité de cette catégorisation.
   Pour une catégorisation possible, voir l'annexe B à la fin de cette astuce.

3. nom du groupe = identificateur d'un vrai groupe de personnes
   
   Dans ce schéma, le groupe correspondrait à un véritable groupe de personnes
   en espace partagé, par exemple le groupe des admins responsables de
   l'emballage. Si vous avez besoin de quelque chose comme ça vous saurez
   bien le faire et comment l'appliquer, donc nous ne nous étendrons pas
   davantage sur cette méthode ici.

### 3.4 Répertoire Home
 
Même s'il est bien possible de ne pas avoir un répertoire valide pour
l'utilisateur-paquet ou d'avoir un seul répertoire partagé par tous les
utilisateurs-paquets, cela serait une occasion manquée. En ayant des répertoires
individuels pour les utilisateurs-paquets, cela offre la possibilité d'organiser
par paquet d'une belle façon les archives tar, les correctifs, les scripts de
construction, les notes et toutes les autres informations que vous accumulez
avec le temps.

Je suggère d'utiliser le répertoire home /usr/src/&lt;paquet&gt; pour un
utilisateur-paquet appelé &lt;paquet&gt; avec le contenu détaillé ci-dessous.
L'archive more_control_helpers contient les scripts et les fichiers squelettes
qui mettent en oeuvre cette suggestion.

*   .bash_profile&nbsp;:

    Vous souhaiterez généralement avoir le même environnement pour tous
    les utilisateurs-paquets, c'est donc une bonne idée que .bash_profile
    soit un lien symbolique vers un fichier dans un emplacement central.
    Par exemple more_control_helpers utilise le fichier
    /etc/pkgusr/bash_profile à cet effet.
           
*   .bashrc&nbsp;:

    Comme pour .bash_profile un lien symbolique est une bonne idée pour
    .bashrc. Par exemple more_control_helpers utilise /etc/pkgusr/bashrc
    comme cible du lien.Dans des circonstances normales les
    utilisateurs-paquets ne sont pas (Et même ne peuvent pas être)
    utilisés pour se connecter au système, donc il n'y a guère
    de raison de faire de distinction entre les shells de connexion et de
    non-connexion pour les utilisateurs-paquets. Par conséquent,
    l'exemple de bashrc dans more_control_helpers est simplement
    le même que .bash_profile. Cela permet de s'assurer que le
    même environnement sera utilisé, que `su <paquet>` ou `su - <paquet>` soient
    utilisés pour devenir l'utilisateur-paquet.
           
*   .project&nbsp;:

    Le contenu de ce fichier est affiché par les commandes
    `finger -l <utilisateur>` et `pinky -l <utilisateur>` ainsi .projet
    est un bon endroit pour mettre des informations sur un paquet. Vous
    devriez garder le contenu des fichiers .project pour vos mise à jour
    avec les utilisateurs-paquets.
                       
*   code source&nbsp;:

    Le répertoire home de l'utilisateur-paquet est l'endroit parfait pour
    ranger le code source d'un paquet. Cela inclut les archives des
    différentes versions, le dépôt CVS, les arborescences des sources
    déballées pour la construction, ...
           
*   script(s) de construction&nbsp;:

    Les installations avec l'utilisateur-paquet nécessitent un examen 
    plus attentif de la construction et des messages d'installation
    qu'une installation réalisée en tant que root, en raison des
    problèmes spécifiques à l'utilisateur-paquet qui peuvent survenir.
    C'est pourquoi il est imprudent de tout simplement "copier-coller"
    les instructions d'installation du Livre LFS. Les scripts de
    construction vous permettront d'utiliser des redirections de sortie
    sophistiquées pour enregistrer des choses qui ne sont pas
    exploitables directement depuis la ligne de commande.
    Le squelette du script "build" inclus dans l'archive
    more_control_helpers montre cela.
                       

## 4. Problèmes généraux

### 4.1 Introduction

L'installation de logiciels est le coeur du système de l'utilisateur-paquet.
Les scripts d'installation étant sont souvent écrits dans l'hypothèse qu'ils
seront exécutés en tant que root, parfois ils échouent lorsqu'ils sont exécutés
par un utilisateur-paquet. Une fois cet obstacle passé et que le paquet a été
installé, il n'y a généralement pas de différences avec une installation root.
Quelques programmes insistent pour que certains fichiers sensibles à la sécurité
appartiennent à root et ne s'exécuteront pas autrement, mais c'est une
exception rare. Ce chapitre présente quelques problèmes plus ou moins généraux
que vous allez rencontrer lors de l'utilisation de comptes utilisateur-paquet
pour installer des logiciels, et des directives sur la façon de traiter ces
problèmes. Bien que je l'ai déjà dit, je vais le redire&nbsp;: beaucoup des problèmes
que vous allez rencontrer lors d'une installation en utilisateur-paquet sont des
caractéristiques souhaitables du système de l'utilisateur-paquet. Vous allez
préférer un échec de l'installation plutôt que d'avoir des actions
potentiellement dangereuses effectuées dans votre dos avec les droits root.

 
### 4.2 Procédure générale
 
Quand une installation échoue, cela est presque toujours dû à une "Permission
non accordée" ou une erreur "Opération non permise" lors de l'exécution d'une
commande pendant `make install`. La première chose que vous avez à faire est
d'identifier la commande qui est à l'origine du problème. Habituellement, vous
trouverez cela dans la sortie de make juste avant le message d'erreur. Lorsque
vous avez identifié la commande coupable, vous avez à décider si l'action qui
est tentée est illégitime, partiellement légitimes ou complètement légitime. Les
commandes illégitimes peuvent être simplement retirées du Makefile. Les deux
autres possibilités sont plus difficiles à gérer. Vous devez soit modifier la
condition qui fait que la commande échoue, soit vous avez à changer ou parfois
supprimer la commande et noter si votre modification supprime une action
légitime.

Une fois que vous avez fait des changements pour résoudre un certain problème,
vous essayer de nouveau l'installation et il faut résoudre tous les problèmes
qui restent jusqu'à ce que l'installation réussisse. Une fois que vous avez
atteint ce point, il est temps d'effectuer toutes les actions restantes que vous
avez dues désactiver, comme donner à certains fichiers binaires un setuid root.

Notez que souvent, les fichiers Makefiles sont générés lors de l'étape de
configuration, parfois même plus tard dans le processus de construction. Si vous
voulez appliquer les changements avant l'étape de configuration, vous aurez
généralement à modifier des fichiers appelés «Makefile.in».

 
### 4.3 Changements des autorisations

Certains systèmes de construction rudimentaires qui n'utilisent pas le script
mkinstalldirs pour créer des répertoires cibles d'installation sont très mal
écrits. Au lieu de tester si un répertoire cible existe, ils tentent simplement
de le créer avec des permissions par défaut. Ce problème se manifeste
habituellement par une ligne telle que «install -d $(prefix)/bin" dans le
Makefile. Dans le cas général de prefix=/usr, ce serait tenter de créer le
répertoire /usr/bin. Si le répertoire cible existe déjà, comme dans ce cas,
l'installation va tenter de changer ses droits par les autorisations par
défaut (ou ceux passés sur la ligne de commande). Bien entendu, un
utilisateur-paquet n'est pas autorisé à changer les droits de /usr/bin,
donc la commande échoue avec un message comme
`Install : ne peut pas changer les permissions de '/usr/bin' : Opérations non permise`.
Ceci est un exemple d'une commande totalement illégitime. Il suffit de la retirer
du Makefile et tout va bien.

 
### 4.4 Changements d'appartenance

La situation la plus courante quand un paquet veut changer l'appartenance de
fichiers lors de l'installation, c'est quand il veut installer des binaires avec
un setuid root. Une commande courante pour faire cela serait quelque chose comme
"install -c -m 4755 -o root name /usr/bin/name" et le message d'erreur
ressemblerait à ceci&nbsp;:
`install : ne peut pas changer le propriétaire de 'name' : Opération non permise`.
Le changement de propriétaire est caché dans l'option "-o root" passée
à l'installation, qui demande de faire appartenir le fichier cible à root.
La commande est au moins partiellement légitime, parce que vous voudrez
probablement que le binaire soit installé. Que vous vouliez vraiment qu'il soit
setuid root est une autre affaire. Le fait que le binaire soit généralement
installé avec un setuid root ne signifie pas que vous devriez faire ainsi. Vous
devrez vous demander si les utilisateurs normaux ont absolument besoin 
d'exécuter ce binaire. Si vous pensez qu'ils peuvent vivre sans cela, vous
feriez mieux de ne pas donner un setuid root, parce que chaque binaire avec un
setuid root est un trou de sécurité potentiel. En tout cas, vous aurez à
modifier le fichier Makefile et enlever le paramètre, "-o root" dans ce cas,
afin que l'installation puisse réussir. Notez que cela va provoquer
l'installation du binaire avec un setuid &lt;paquet&gt;, ce qui bien sûr n'a
absolument aucun sens. Si vous n'avez pas l'intention de rendre le binaire
setuid root après l'installation, il est préférable de changer le "-m 4755" en
"-m 755", de sorte qu'il ne sera pas installé avec un setuid.
.

{: .alert .alert-info}
__ASTUCE__&nbsp;
Lorsque vous donnez à un binaire un setuid root après l'installation, utilisez
'chown root /usr/bin/name' et pas 'chown root:root /usr/bin/name'.
De cette façon vous pouvez garder intact le groupe d'origine du fichier (c'est
à dire le groupe de l'utilisateur-paquet). Avec le schéma nom d'utilisateur
= nom de groupe recommandé pour le nom des utilisateurs-paquets, cela permet 
de s'assurer que vous pouvez identifier le paquet source du binaire, même
après lui avoir donné un setuid root. Notez que par mesure de sécurité, chown
réinitialise le bit setuid, de sorte que vous aurez à faire `chmod u+s /usr/bin/name`
après le chown.


 
### 4,5  Ecrire dans les répertoires de non-installation

Parfois, les paquets veulent créer des fichiers ou des répertoires dans les
répertoires de non-installation. 3 situations sont à distinguer dans ce cas. La
1ère possibilité est que le répertoire cible devrait être un répertoire
d'installation. Un exemple de cela est /usr/share/aclocal. Ce répertoire ne fait
pas partie des répertoires système standards créés lors de la construction d'un
système LFS. Il sera créé par le premier paquet qui contient des fichiers à y
installer et appartiendra à l'utilisateur-paquet correspondant. Le prochain
paquet qui veut y écrire ne parviendra pas à s'installer. Le remède est simple.
Il suffit de faire du répertoire un répertoire d'installation. Vous n'avez même
pas besoin d'être root pour le faire. L'utilisateur-paquet qui possède le
répertoire a le droit de faire ce changement.

La 2ème raison possible pour qu'un paquet veuille écrire dans un répertoire de
non-installation est que la commande qui échoue n'est que partiellement
légitime,
c'est-à-dire que vous voulez bien installer tout ce qui est destiné à être 
installé, mais vous le voulez à un autre endroit. Par exemple, certains paquets
installent des binaires qui ne sont pas destinés à être appelés directement.
L'emplacement par défaut de ces binaires est parfois appelé libexec et avec le
prefix=/usr le paquet va tenter de créer /usr/libexec. Dans des cas pareil,
vous n'avez souvent pas besoin de changer de Makefiles. Il y a soit un paramètre
de configuration pour modifier le répertoire en question ou c'est juste une
question de suppression d'une variable d'un fichier Makefile comme dans
`make libexecdir=/usr/lib install`.

La 3ième raison possible pour une tentative d'écriture dans un répertoire de
non-installation est que la commande en question est illégitime, c'est à dire
que vous ne voulez pas installer tout ce que le paquet veut installer. Comme
d'habitude avec les commandes illégitimes vous pouvez éditer le fichier Makefile
et tout simplement les supprimer. Dans le cas où c'est un répertoire tout entier
que vous voulez supprimer de l'installation, cela pourrait représenter beaucoup
trop d'efforts de supprimer toutes les commandes qui veulent installer les
fichiers. Dans ce cas, une approche similaire à celle du précédent paragraphe
peut être plus efficace. Soit par l'intermédiaire des paramètres de
configuration ou des variables principales pour changer le répertoire en
question en quelque chose comme &lt;builddir&gt;/foobar, où &lt;builddir&gt; est le
répertoire dans lequel les commandes de construction sont exécutées
(généralement le haut de l'arborescence des sources déballées). Cela entraînera
que le paquet va créer le répertoire indésirable à l'intérieur de l'arborescence
de construction, ce qui ne pose aucun problème de droits et qui présente
l'avantage qu'il pourra être supprimé avec le répertoire de construction lorsque
vous allez nettoyer après la construction.


### 4.6 Supprimer ou Remplacer un fichier

Dans un monde parfait un paquet ne doit pas interférer avec les fichiers d'un
autre paquet, mais dans le monde réel les conflits se produisent
occasionnellement. Alors qu'un administrateur système utilisant une installation
normale en tant que root ne verrait pas cela avant qu'il soit trop tard, un
admin employant le système de l'utilisateur-paquet aura à gérer les conflits
sans tarder. Quand un paquet essaie d'écraser ou de supprimer un fichier ou un
répertoire appartenant à un autre paquet alors la tentative échouera.
Il va échouer même à l'intérieur des répertoires d'installation en raison du bit
sticky. Bien que parfois difficile à mettre en ½uvre, la solution à un tel
conflit est facile à décrire. Vous devez soit supprimer (ou renommer) l'ancien
fichier ou répertoire avant d'installer, ou supprimer l'installation du nouveau
fichier ou répertoire. L'installation des binaires individuels est parfois
facile à empêcher. Si vous trouvez une ligne comme "PROGRAMS=foo bar fubar
barfu" dans le Makefile, où «foo» est le nom du binaire en conflit, essayez
juste de l'enlever de cette liste. C'est peut-être suffisant pour l'empêcher de
s'installer.

 
### 4,7 /sbin/ldconfig
 
Les paquets qui installent des bibliothèques, lancent parfois /sbin/ldconfig 
dans le cadre de leur installation de sorte que les bibliothèques dynamiques
soient correctement inscrites sur le système. Un utilisateur-paquet n'étant pas
autorisé à remplacer /etc/ld.so.cache, ldconfig échoue. Cet échec est en général
ignoré dans les Makefiles, mais vous devriez quand même en tenir compte,
parce que vous devrez exécuter ldconfig en tant que root, après l'installation.
Sinon, more_control_helpers contient un programme enveloppe qui appellera
/sbin/ldconfig et peut lui donner un setuid root.


### 4.8 Quelles commandes lancer en tant qu'utilisateur-paquet

Un problème courant rencontré par les nouveaux utilisateurs de cette astuce est
de décider quelles sont les commandes à lancer en tant qu'utilisateur-paquet et
quelles sont les commandes à lancer en tant que root. La règle générale est que
les seules commandes à lancer en tant qu'utilisateur-paquet sont celles pour la
construction, l'installation,l'effacement et la modification des fichiers qui
appartiennent au paquet de *cet* utilisateur-paquet. Tout le reste devrait être
lancé normalement en tant que root. Les choses que vous NE POUVEZ/NE DEVEZ PAS
FAIRE en tant qu'utilisateur-paquet sont

-   démarrer un démon
-   lancer udevstart
-   nettoyer /lib/*

  
## 5. L'archive more_control_helpers

### 5.1 Aperçu

L'archive more_control_helpers contient des fichiers pour vous aider à
construire et maintenir un système qui utilise la méthode de
l'utilisateur-paquet. Parmi ce que l'archive more_control_helpers contient,
figurent quelques fichiers temporaires spécifiques LFS qui sont seulement
nécessaires pour la construction de votre système LFS et ne resteront pas
installés dans un emplacement permanent. Puis il y a les fichiers d'exemples
mentionnés précédemment qui montrent l'utilisation suggérée des répertoires du
home de l'utilisateur-paquet évoqués plus haut. Un autre groupe de fichiers
contenus dans l'archive est un ensemble de scripts qui aident pour les aspects
de gestion des paquets, tels que la création de nouveaux utilisateurs-paquets et
la vérification des fichiers installés par un paquet particulier. Enfin,
l'archive contient des scripts enveloppes pour certaines commandes qui
solutionnent de nombreux problèmes généraux examinés dans le chapitre précédent
et facilitent énormément les installations d'utilisateurs-paquets.
  
 
 
### 5.2 Les enveloppes
 
Le chapitre précédent a traité certains problèmes généraux rencontrés durant
la construction de paquets avec l'utilisateur-paquet et comment les résoudre. La
solution pour résoudre un échec d'installation nécessite généralement l'édition
d'un ou plusieurs fichiers Makefile. Faire de telles modifications à la main
s'avère pénible, même si cela n'arrive qu'occasionnellement, à chaque fois que
vous réinstallez un paquet vous aurez à faire de nouveau les changements. Des
Scripts Sed et des correctifs peuvent aider pour ce dernier problème, mais il
faut les écrire sur mesure pour chaque paquet qui en a besoin. Il existe
cependant une meilleure solution. Bien qu'il y ait d'innombrables façons
d'installer les fichiers, seulement très peu sont couramment utilisées par les
paquets. Les 5 commandes mkdir, chgrp, chown, chmod et install sont
responsables de la plupart des problèmes qui se posent régulièrement lors d'une
installation de LFS. Cela m'a poussé à écrire des scripts enveloppes pour ces
cinq commandes qui reconnaissent certains modèles de problématiques et les
traitent automatiquement.

Les instructions données dans cette astuce dans la partie spécifique à LFS vous
indiqueront d'installer ces paquets dans /usr/lib/pkgusr. Si vous faites cela,
assurez-vous que ce répertoire est la première entrée dans le PATH de chaque
utilisateur-paquet, puis ils vont vous faire économiser beaucoup de temps et
d'efforts pour traiter les questions récurrentes. Notez que si vous
voulez choisir un autre répertoire que /usr/lib/pkgusr pour les enveloppes, vous
devez être prudent. Certains scripts de configuration ignorent certains
endroits. Un sous-répertoire de /etc ne fonctionnerait pas, par exemple, parce 
que le fichier /etc est l'un de ces endroits.

Il est important que vous compreniez les limites des scripts enveloppe. Ils
peuvent résoudre certains problèmes, sans intervention de l'utilisateur, tels
que le passage en répertoire d'installation des répertoires nouvellement créés
dans /usr/share/local. Mais d'autres problèmes par leur nature même
nécessiteront une intervention manuelle. Quand un programme tente d'installer un
binaire avec un setuid root, par exemple, les scripts enveloppes supprimeront
la tentative de changement de propriétaire d'un fichier installé en root. Si
cela permet `make install` de se terminer sans erreur, c'est seulement une
solution partielle. Les scripts enveloppes ne peuvent pas (et ne devraient pas)
enlever votre responsabilité de décider si le programme en question devrait
avoir un setuid root et de faire en sorte qu'il l'ait si nécessaire. Pour tenir
compte de cela, les scripts enveloppes vont afficher des lignes d'avertissement
sur la sortie d'erreur standard qui commencent par "***" chaque fois qu'ils
rencontrent une situation à revoir. Ce qui suit "***" dans le message sera la
commande d'origine que l'installation a tenté d'effectuer. Vous *devez*
vérifier ces lignes "***", examiner les fichiers ou répertoires concernés et
prendre les mesures appropriées. Il est donc impératif que vous enregistriez la
sortie des messages au cours de l'installation d'un paquet et que vous vérifiez
ces journaux religieusement. Le script 'build' contenu dans l'archive
more_control_helpers montre quelques trucs utiles pour rediriger la sortie afin
de l'utiliser à cette fin. Les trois exemples suivants illustrent le genre de
choses que vous devrez rechercher&nbsp;:

#### Exemple 1&nbsp;:  "*** install -c rsh -o root -m 4775 /usr/bin/rsh"

Ce message est émis par l'enveloppe install pendant l'installation de
inetutils. Le paquet veut installer le binaire rsh avec un setuid root.
L'enveloppe install élimine le "-o root" et change le "-m 4775" en
"-m 755" avant de passer la commande au vrai programme d'installation.
L'élément important ici est le "-m 4xxx", parce qu'il veut régler le
bit setuid. Certains scripts d'installation vont mettre un "-o root" pour
faire bonne mesure lors de l'installation d'un binaire par ailleurs normal.
Dans ce cas il suffit que l'enveloppe install enlève le "-o root" et vous
n'avez pas à prendre de mesures supplémentaires. Mais lorsque, comme dans
le cas de inetutils, les autorisations indiquent une tentative pour donner
un setuid ou un setgid à un binaires, donc vous aurez à enquêter. Vous
devrez décider si vous voulez que rsh soit avec un setuid root et si vous
décidez de lui donner, vous aurez besoin de devenir root pour saisir les
commandes suivantes&nbsp;:
    
```bash
chown root /usr/bin/rsh
chmod u+s /usr/bin/rsh
```
  
{: .alert .alert-info}
__ASTUCE__&nbsp;:
Soyez prudent en attribuant les setuid aux binaires. Si vous n'êtes pas
certain que vous n'aurez jamais à utiliser un programme (en étant pas
non-root), vous ne voudrez probablement pas qu'il ait un setuid root. Gardez
à l'esprit que vous pouvez toujours faire le changement plus tard quand
vous en aurez besoin. Lorsque vous appliquez ce raisonnement à rsh, par
exemple, vous finirez probablement par ne pas lui donner un setuid root.


#### Exemple 2&nbsp;: "*** chgrp tty /usr/bin/write"

Ceci est la sortie de l'enveloppe chgrp lors de l'installation de util-linux.
Le paquet util-linux veut installer le programme write et lui donner un
setgid tty, de sorte qu'il soit autorisé à accéder aux terminaux des autres
utilisateurs. L'enveloppe chgrp empêche la modification du groupe et
l'enveloppe chmod empêche le réglage du bit setgid. Vous aurez à décider si
vous voulez que le programme ait un setgid et si vous décidez cela, en tant
que root faites

```bash
chgrp tty /usr/bin/write
chmod g+s /usr/bin/write
```

    
#### Exemple 3&nbsp;: "*** install -d -m 755 /sbin"

C'est aussi lors de l'installation de util-linux. Util-linux, pour aucune
bonne raison, essaie de recréer le répertoire /sbin. L'enveloppe install
empêche cela et vous n'avez pas à prendre d'autres mesures.

 
### 5,3 add_package_user/install_package

À chaque fois que vous installez un nouveau paquet sur votre système, vous devez
d'abord créer un nouveau compte utilisateur-paquet, peut-être créer un nouveau
groupe et si vous suivez les conseils de cette astuce de faire un usage 
productif du répertoire home de l'utilisateur-paquet, vous aurez aussi à mettre
en place cette structure. Si vous aviez à faire tout cela manuellement, ce
serait beaucoup de travail. Les scripts add_package_user et install_package de
l'archive more_control_helpers ont été écrits pour automatiser cela.

Le script install_package est celui que vous utilisez normalement pour préparer
l'installation d'un nouveau paquet. Il prend 3 paramètres&nbps;: la description du
paquet, le nom du compte de l'utilisateur-paquet à créer et le nom du
groupe principal de l'utilisateur-paquet. Donc, si vous utilisez le principe de
utilisateur = groupe recommandé par cette astuce et que vous êtes aussi créatif
que moi avec vos descriptions de paquet, alors la commande que vous utiliserez
pour préparer l'installation du paquet "toto" sera

```bash
install_package toto toto toto
```

Cette commande fait 2 choses. D'abord, elle appelle le script add_package_user
avec le nom fourni, le groupe et la description ainsi que les valeurs par défaut
raisonnables pour d'autres paramètres de add_package_user. Puis, après que
add_package_user a créé le compte utilisateur-paquet, install_package utilise
automatiquement la commande su pour passer sur le compte nouvellement créé. Si
les scripts .bashrc et .bash_profile par défaut que vous utilisez pour les
utilisateurs-paquets contiennent la commande "cd" comme le font ceux en exemple
dans l'archive more_control_helpers, vous serez mis directement dans le
répertoire home de votre utilisateur-paquet afin que vous puissiez commencer à
l'installer tout de suite.

{: .alert .alert-info}
__ASTUCE__&nbsp;:
Le script install_package peut être appelé avec un simple argument qui sera
utilisé comme nom d'utilisateur, nom de groupe, et description. Donc à la
place de la commande ci-dessus, une simple commande `install_package toto`
devrait suffire.

Le script add_package_user est responsable du véritable travail de création du
nouveau compte de l'utilisateur-paquet. Avec un nom, un nom de groupe et une
description, il va créer un nouveau compte utilisateur avec comme groupe
primaire celui passé en argument et comme groupe supplémentaire le groupe
d'installation. Les groupes seront créés si nécessaire. add_package_user prend
plusieurs arguments qui déterminent la plage numérique à partir de laquelle il
prendra l'UID dont il a besoin pour créer le nouvel utilisateur et le GID pour
les groupes. add_package_user ne crée pas seulement le compte
utilisateur-paquet. Il lui mettra également en place un répertoire personnel.
Vous pouvez spécifier le répertoire ou laisser l'option par défaut, qui est
/usr/src/&lt;nom&gt;, où &lt;nom&gt; est le nom fournis pour le nouveau compte. Si le
répertoire existe déjà, son appartenance et son contenu existant iront au nouvel
utilisateur. S'il n'existe pas, il sera créé.

Le contenu de /etc/pkgusr/skel-package sera copié dans le répertoire home du
nouvel utilisateur-paquet (sans écraser les fichiers déjà existants). L'archive
more_control_helpers contient un exemple utile de répertoire skel-package.
Notez que les liens symboliques sont copiés comme liens symboliques, donc
skel-package est l'endroit idéal pour mettre les liens symboliques vers un
endroit unique pour .bash_profile et .bashrc qui garantiront que tous les 
comptes utilisateurs-paquets auront le même environnement. Ceci est
particulièrement utile pour s'assurer que tous les utilisateurs-paquets aient
les répertoires des enveloppes dans leur PATH.

  
### 5,4 forall_direntries_from

Le script forall_direntries_from est un outil très utile pour la gestion usuelle
des paquets. Il peut être grossièrement décrit comme un raccourci pour
"find / -user &lt;nom&gt; -or -group &lt;nom&gt;  &lt;commands&gt;", où &lt;nom&gt; est le premier
paramètre à forall_direntries_from et &lt;commandes&gt; sont les autres paramètres.
Toutefois, forall_direntries_from prend soin de s'assurer que les systèmes de
fichiers concernés sont analysés et vous protège de certaines surprises
désagréables telles que "Oups, j'ai oublié que -depth nie -prune et mon
répertoire est accidentellement effacé." ou "Oups, j'ai oublié -prune /proc
et maintenant je reçois des erreurs de parité sur mon bus SCSI.".

{: .alert .alert-info}
__NOTE IMPORTANTE__&nbsp;:
Par défaut, le script forall_direntries_from n'analysera que 
le système de fichiers / et ne traversera pas les autres systèmes de fichiers.
Si vous avez des répertoires pertinents qui doivent être analysés sur d'autres
systèmes de fichiers, vous aurez besoin d'éditer le script et d'ajouter le(s)
point(s) de montage respectif(s) à la liste fs_to_scan au début du script.
L'ajout le plus probable est pour "/usr".

Exemples d'application&nbsp;:

Exemple 1&nbps;: Créer une archive tar.gz de tous les fichiers qui appartiennent à
&lt;paquet&gt;, par exemple, pour l'installation de &lt;paquet&gt; sur une autre machine
sans avoir à le recompiler.

```bash
forall_direntries_from <package> -fprint0 /tmp/files.lst
tar --null -P -czf /tmp/archive.tar.gz --files-from=/tmp/files.lst
```


Exemple 2&nbps;: Afficher la liste de tous les binaires ayant un setuid root 
installés par &lt;paquet&gt;. (Ceci ne fonctionne que si vous utilisez le principe de
utilisateur = groupe.)

```bash  
forall_direntries_from <paquet> -perm +u+s -print
```


Exemple 3&nbsp;: Liste tous les fichiers binaires dans /bin et /usr/bin appartenant à
«moi» (c'est à dire à l'utilisateur-paquet qui exécute la commande) dans l'ordre
alphabétique.

```bash  
forall_direntries_from $(whoami) -path "*/bin/*" -printf "%f\n" | sort
```


Exemple 4&nbsp;: Désinstallez &lt;paquet&gt;.
          
Voir la section suivante sur le script uninstall_package.
 
                                   
### 5,5 uninstall_package

Le script uninstall_package est fondamentalement un exemple d'application
forall_direntries_from sous forme de script. La commande
`uninstall_package toto` affiche l'appel à forall_direntries_from que vous avez
à utiliser pour supprimer tous les fichiers du paquet «toto» (sauf pour ceux
dans des répertoires que forall_direntries_from ne doit pas analyser) avec
quelques explications. Ainsi, afin de supprimer les fichiers du paquet toto,
vous aurez à exécuter `uninstall_package toto` et ensuite faire un copier-coller
de la commande qu'il affiche sur la ligne de commande. Par mesure de sécurité
pour l'appel de forall_direntries_from un "echo" est mis avant "rm" et "rmdir",
donc si vous l'exécutez, les fichiers ne seront pas réellement supprimés sauf si
vous supprimez les deux instances de «echo». Il est recommandé que vous
exécutiez la commande une fois avec les échos et que vérifiez la sortie pour
s'assurer que seuls les fichiers que vous avez l'intention de supprimer sont
dans la liste. Après en être certain, vous pouvez utiliser l'historique du
shell, modifier les "echo" et vraiment supprimer les fichiers.


### 5,6 list_suspicious_files / list_suspicious_files_from

list_suspicious_files cherche les entrées du système de fichiers qui ne sont pas
normales d'une certaine façon et les affiche dans une liste triée par catégorie.
Les choses qui sont considérées comme suspectes comprennent les binaires avec
setuid et setgid, les fichiers avec des droits en écriture pour tout le monde,
les liens symboliques qui sont éventuellement brisés, les liens en dur, les
répertoires d'installation avec des autorisations inhabituelles et d'autres
choses. Vous devriez exécuter ce script après avoir fini votre nouveau système
LFS et à intervalles réguliers par la suite. Etudiez attentivement la liste.

{: .alert .alert-info}
__ASTUCE__&nbsp;:
Lorsque vous vérifiez la liste des fichiers setuid et setgid, n'oubliez pas de
regarder l'utilisateur ou le groupe réellement propriétaire du fichier. Il est
facile d'oublier cela, surtout dans le cas de setuid, parce que nous
assimilons souvent setuid avec setuid root et que setuid est rarement utilisé
avec d'autres comptes utilisateurs.

list_suspicious_files_from prend comme argument d'entrée un nom d'utilisateur ou
un groupe ou un UID/GID et ne rapporte comme suspect que les fichiers
appartenant à l'utilisateur ou au groupe donnés. Habituellement vous ne devriez
pas appeler ce script directement, mais plutôt utiliser list_package (décrit
dans la section suivante), dont la sortie comprend celle de
list_suspicious_files_from.

{: .alert .alert-info}
__NOTE IMPORTANTE__&nbsp;:
Par défaut, le script list_suspicious_files n'analysera que
le système de fichiers / et ne parcourra pas les autres systèmes de fichiers.
Si vous avez des répertoires pertinents qui doivent être analysés sur d'autres
systèmes de fichiers, vous aurez besoin d'éditer le script et d'ajouter le(s)
point(s) de montage respectif(s) dans la liste fs_to_scan au début du script.
L'ajout le plus probable est pour "/usr".
 


### 5,7 list_package

list_package vous dit tout sur les fichiers installés par un paquet. En général,
vous voulez exécuter quelque chose comme

```bash
list_package $(whoami) >pkg.lst
```
   
juste après l'installation d'un paquet et vous pouvez oublier les listes de
contenu inexact chronique du livre (B)LFS. La sortie suivante (raccourcie)
pour util-linux est éloquente&nbsp;:

```
PS1> list_package util-linux

EXECUTABLES (dans */bin ou */sbin)
  agetty, arch, blockdev, cal, cfdisk, [...] vidmode(->rdev), whereis, write

BIBLIOTHEQUES (lib*.a ou lib*.so)
  libblkid, libmount, libuuid

EXECUTABLES SANS PAGES DE MANUEL (dans */bin ou */sbin)
  fsck.cramfs, mkfs.cramfs

RESUME DES PAGES DE MANUEL DES EXECUTABLES (dans */bin or */sbin)
  addpart : simple wrapper around the "add partition" ioctl
  agetty : alternative Linux getty
  arch : print machine architecture
  blkid : locate/print block device attributes
  blockdev : call block device ioctls from the command line
  cal : display a calendar
  cfdisk : display or manipulate disk partition table
  chkdupexe : find duplicate executables
  chrt : manipulate the real-time attributes of a process
  col : filter reverse line feeds from input

  [...]
  swapon : enable/disable devices and files for paging and swapping
  switch_root : switch to another filesystem as the root of the mount tree
  tailf : follow the growth of a log file
  taskset : retrieve or set a process's CPU affinity
  tunelp : set various parameters for the lp device
  ul : do underlining
  umount : unmount file systems
  unshare : run program with some namespaces unshared from parent
  uuidd : UUID generation daemon
  uuidgen : create a new UUID value
  wall : write a message to users
  whereis : locate the binary, source, and manual page files for a command
  wipefs : wipe a filesystem signature from a device
  write : send a message to another user

PAGES DE MANUEL SUPPLEMENTAIRES
  /usr/share/man/man1/ddate.1
  /usr/share/man/man3/libblkid.3
  /usr/share/man/man3/uuid.3
  /usr/share/man/man3/uuid_clear.3
  /usr/share/man/man3/uuid_compare.3

  [...]

  /usr/share/man/man3/uuid_parse.3
  /usr/share/man/man3/uuid_time.3
  /usr/share/man/man3/uuid_unparse.3
  /usr/share/man/man5/fstab.5

EXECUTABLES SUPPLEMENTAIRES (pas dans */bin ou */sbin)
  /usr/lib/libblkid.la
  /usr/lib/libmount.la
  /usr/lib/libuuid.la
  /usr/share/getopt/getopt-parse.bash
  /usr/share/getopt/getopt-parse.tcsh

TOUS LES FICHIERS
  /bin/arch
  /bin/dmesg
  /bin/findmnt
  /bin/lsblk
  /bin/more
  /bin/mount
  /bin/mountpoint
  /bin/umount
  /lib/libblkid.so.1
  /lib/libblkid.so.1.1.0
  /lib/libmount.so.1
  /lib/libmount.so.1.1.0
  /lib/libuuid.so.1
  /lib/libuuid.so.1.3.0
  /sbin/agetty
  /sbin/blkid
  /sbin/blockdev
  /sbin/cfdisk
  /sbin/ctrlaltdel
  /sbin/fdisk
  /sbin/findfs
  /sbin/fsck
  /sbin/fsck.cramfs
  /sbin/fsck.minix
  /sbin/fsfreeze
  /sbin/fstrim
  /sbin/hwclock
  /sbin/losetup
  /sbin/mkfs
  /sbin/mkfs.bfs
  [...]
  /usr/share/man/man8/losetup.8
  /usr/share/man/man8/lsblk.8
  /usr/share/man/man8/mkfs.8
  /usr/share/man/man8/mkfs.bfs.8
  /usr/share/man/man8/mkfs.minix.8
  /usr/share/man/man8/mkswap.8
  /usr/share/man/man8/mount.8
  /usr/share/man/man8/partx.8
  /usr/share/man/man8/pivot_root.8
  /usr/share/man/man8/rtcwake.8
  /usr/share/man/man8/setarch.8
  /usr/share/man/man8/sfdisk.8
  /usr/share/man/man8/swaplabel.8
  /usr/share/man/man8/swapoff.8
  /usr/share/man/man8/swapon.8
  /usr/share/man/man8/switch_root.8
  /usr/share/man/man8/tunelp.8
  /usr/share/man/man8/umount.8
  /usr/share/man/man8/uuidd.8
  /usr/share/man/man8/wipefs.8
  /var/lib/hwclock

FICHIERS AVEC SETGID
  -rwxr-sr-x "/usr/bin/wall"  util-linux:tty
  -rwxr-sr-x "/usr/bin/write"  util-linux:tty

FICHIERS AVEC DES PERMISSIONS INHABITUELLES
  -rwxr-sr-x "/usr/bin/wall"  util-linux:tty
  -rwxr-sr-x "/usr/bin/write"  util-linux:tty
```

{: .alert .alert-info}
__Note__&nbsp;: list_package fonctionne quel que soit le préfixe avec lequel vous avez
installé le paquet, de sorte que vous pouvez par exemple configurer avec -
prefix =/opt/paquet et list_package fonctionnera très bien (à condition que
/opt soit sur un système de fichiers configurés pour être analysés par
forall_direntries_from et list_suspicious_files).

{: .alert .alert-info}
__Remarque__&nbsp;: list_package ne considère que les pages de manuel qui appartiennent
au paquet dans la liste. Il ne tiendra pas compte des pages de manuel installées
par un autre paquet. Cela signifie que vous pouvez voir des exécutables
identifiés comme n'ayant pas de page de manuel bien qu'ils en aient une donnée
gracieusement par un autre paquet (Généralement man-pages).


### 5,8 grep_all_regular_files_for

Ce script n'est pas vraiment lié au système de l'utilisateur-paquet, mais à
cause de sa similitude avec les autres scripts, je l'ai quand même inclus. Le
seul but de ce script est d'identifier les fichiers enregistrant des références
à l'environnement de construction, en particulier dans le répertoire /tools. Ces
références peuvent montrer des problèmes, car le répertoire /tools est censé
être transitoire. N'oubliez pas que les résultats pour les fichiers binaires et
les bibliothèques nettoyés ne sont pas des références fiables, car les
informations de débogage incluent souvent des références à l'environnement de
compilation. Cela ne soulève pas de difficultés (sauf si vous essayez de
déboguer les objets en question après la suppression de /tools).
  
{: .alert .alert-info}
__NOTE IMPORTANTE__&nbsp;:
Par défaut, le script grep_all_regular_files_for n'analysera
que le système de fichiers / et ne traversera pas les autres systèmes de 
fichiers. Si vous avez des répertoires pertinents qui doivent être analysés sur
d'autres systèmes de fichiers, vous aurez besoin d'éditer le script et d'ajouter
le(s) point(s) de montage respectif(s) dans la liste fs_to_scan au début du
script. L'ajout le plus probable est pour "/usr".

  
### 5.9 Le répertoire etc
 
Si vous suivez les instructions fournies dans la partie spécifique de LFS de
cette astuce, le contenu du répertoire /etc sera installé dans /etc/pkgusr. Le
répertoire contient un bashrc et bash_profile pour les utilisateurs-paquets qui
prend soin des détails spécifiques aux utilisateurs-paquets comme mettre le
répertoire des enveloppes au début du PATH et appeler cd, de sorte que `su <paquet>`
vous placera directement dans le répertoire home de
l'utilisateur-paquet. On trouve aussi dans le répertoire /etc un répertoire
skel-package utilisé par install_package/add_package_user pour peupler les
répertoires personnels des nouveaux utilisateurs-paquets créés.
 
 
### 5.10 ldconfig.c

Un grand nombre de paquets contiennent des bibliothèques. Avoir à appeler
manuellement /sbin/ldconfig en tant que root après l'installation de ces paquets
peut devenir agaçant. Il serait beaucoup plus facile si l'on pouvait accorder
aux utilisateurs-paquets l'autorisation d'utiliser /sbin/ldconfig. Donner
un setuid root à ldconfig serait une solution simple et efficace, mais il existe
quelques pièges. Tout d'abord, il est impératif que les utilisateurs ordinaires
aient l'interdiction d'exécuter ldconfig avec des privilèges élevés. Sinon, un
utilisateur ordinaire peut écraser et éventuellement lire des fichiers
de son choix sur le système. Ceci peut être évité en faisant appartenir ldconfig
au groupe install et en retirant le bit o+x du mode du fichier. Bien que cette
configuration n'est pas moins sûre que de lancer "make install" en tant que
root, l'une des raisons pour laquelle nous utilisons les utilisateurs-paquets
est que nous ne nous sentions pas en sécurité en faisant cela. Pour se protéger
contre le danger (certes très théorique) d'un utilisateur-paquet malveillant,
l'archive more_control_helpers fournit ldconfig.c. La seule chose que ce
programme fait, c'est appeler `/sbin/ldconfig -v` avec un environnement vide.
N'évaluant pas toutes les entrées utilisateur et ne passant aucune donnée 
fournie par l'utilisateur à ldconfig, il peut en toute sécurité avoir un setuid
root.


### 5.11 Fichiers Temporaires

3 fichiers dans l'archive more_control_helpers ne sont utilisés que pendant
l'installation du système de base LFS et ne sont pas installés de façon
permanente. Le premier d'entre eux est le fichier installdirs.lst qui contient
une liste des répertoires qui devront être répertoires d'installation. Le second
fichier est sbin/useradd, qui est un script shell très basique qui ajoute une
nouvelle entrée au fichier /etc/passwd. Elle nous permet d'ajouter des
utilisateurs-paquets avant d'avoir installé shadow, qui fournit un useradd réel.
Enfin, il y a groupadd, qui est comme useradd, uniquement pour le fichier
/etc/group. Les deux scripts, useradd ainsi que groupadd, ont un contrôle
d'erreurs très faible et ne supportent que la syntaxe nécessaire à
install_package/add_package_user. Donc, n'essayez pas quelque chose de funky
avec eux.


PARTIE 2: Spécificités LFS
==========================

## 6. Phase Pré-chroot (chapitre 5)

Construisez le Chapitre 5 exactement comme expliqué par le livre LFS avec les
modifications suivantes

### coreutils

Après avoir exécuté `make install` pour le paquet coreutils, 
exécutez la commande suivante (toujours au sein du répertoire de construction
de coreutils)&nbsp;:

```bash
cp src/su /tools/bin
```

Ceci installe le binaire su. Coreutils n'installe pas su quand on travaille
en tant que non-root (ce que nous faisons dans le chapitre 5), parce que su a
besoin d'avoir un setuid root pour fonctionner normalement et un utilisateur
non-root ne peut pas installer les binaires setuid root. Mais pour nos besoins
(c'est à dire passer de root à un utilisateur-paquet) un su non-setuid est
suffisant, donc nous copions le su de coreutils dans /tools/bin sans lui
donner un setuid root.

### more_control_helpers

Lorsque vous aurez atteint la fin du chapitre 5, avant de commencer le
chapitre 6 vous aurez besoin d'installer les scripts d'aide dans le répertoire
/tools de sorte qu'ils soient disponibles une fois que vous serez entré dans
l'environnement chroot. Utilisez les commandes suivantes pour installer le
more_control_helpers dans /tools&nbsp;:

```bash
    cd /tools &&
    tar xjf /path/to/more_control_helpers.tar.bz2 &&
    cd  more_control_helpers &&
    cp ./sbin/* /tools/bin
```
    
Notez que le répertoire cible est «/tools/bin" dans la commande cp et
*pas* "/tools/sbin", bien que ce dernier emplacement serait plus approprié.
La raison en est simplement que les instructions de LFS n'ajoutent pas
"/tools/sbin" à la variable PATH (et ni les instructions de cette astuce).


## 7. Phase de chroot (chapitre 6)

### 7.1 Préparatifs

Entrez dans l'environnement chroot et suivez les instructions du livre 'en vous
arrêtant à' l'installation du premier paquet
(ce qui au moment de la rédaction de cette astuce est linux-libc-headers).
Maintenant, installez les fichiers de more_control_helpers au bon endroit sur le
nouveau système LFS&nbsp;:

```bash
cp -a /tools/more_control_helpers/etc /etc/pkgusr &&
chown -R 0:0 /etc/pkgusr &&
cp -a /tools/more_control_helpers/lib /usr/lib/pkgusr &&
chown -R 0:0 /usr/lib/pkgusr &&
cp /tools/more_control_helpers/bin/* /usr/bin &&
cp /tools/more_control_helpers/sbin/* /usr/sbin &&
rm /usr/sbin/{useradd,groupadd}
```

Notez que les scripts useradd et groupadd ne sont pas installés sur le nouveau
système LFS. Ces scripts ne sont des solutions de contournement que temporaires,
nous allons les utiliser tant que les commandes useradd et groupadd réelles ne
sont pas disponibles. Par conséquent, elles ne devraient être que dans 
/tools/bin.

{: .alert .alert-warning}
__ATTENTION&nbsp;!__&nbsp;:
Si vous décidez d'utiliser un répertoire différent de /usr/lib/pkgusr
pour les enveloppes, vous devez être prudent, car au moins le script de
configuration de glibc ignore certains répertoires lors de la recherche des
programmes. La liste des répertoires ignorés pour la glibc comprend, entre
autres, tout ce qui commence par "/etc", "/usr/etc" et "/sbin". Mettre les
enveloppes dans un répertoire qui correspond à un de ces modèles serait
inefficace.

Maintenant il est temps de créer le groupe d'installation&nbsp;:

```bash
groupadd -g 9999 install
```

Le GID 9999 a été choisie parce que la plage par défaut utilisé par
add_package_user pour le GID utilisateur-paquet commence à 10000. Choisissez
n'importe quel nombre que vous souhaitez.

Une fois que le groupe installation a été créé, vous devrez changer tous les
répertoires où les paquets installeront des fichiers en répertoires
d'installation. Pour faciliter cela, j'ai compilé une liste de répertoires
d'installation que vous pouvez trouver dans le fichier
/outils/more_control_helpers/installdirs.lst. La commande suivante utilise cette
liste pour attribuer les répertoires nécessaires au groupe d'installation. Notez
que vous aurez plusieurs messages d'erreur en raison de répertoires
non-existants. C'est parce que la liste contient certains répertoires pas encore
créé par LFS.

```bash
chown 0:9999 $(cat /tools/more_control_helpers/installdirs.lst)
```

Pour être utilisables par les utilisateurs-paquets, les répertoires devront être
accessibles en écriture au groupe et devraient être avec le bit sticky comme
cela a été expliqué au début de cette astuce. La commande suivante définit les
autorisations de manière appropriée. Vous obtiendrez les mêmes messages d'erreur
que pour la commande précédente.

```bash
chmod ug=rwx, o=rxt $(cat /tools/more_control_helpers/installdirs.lst)
```


### 7.2 Procédure pas à pas&nbsp;: Installation de linux-libc-headers

À ce stade, tout a été mis en place pour créer le premier utilisateur-paquet. Au
moment d'écrire ces lignes le premier paquet installé dans le livre LFS est
Linux-Libc-Headers, donc ce paquet servira d'exemple pour la manière dont les
choses se font. La commande

```bash
install_package 'Linux Headers' linux-libc-headers linux-libc-headers
```

va créer un utilisateur-paquet avec le nom d'utilisateur et le nom du groupe
linux-libc-headers. Si vous ne souhaitez pas utiliser le principe
utilisateur=groupe, changez le dernier argument avec le nom du groupe désiré. La
description est arbitraire, mais doit répondre au exigences pour le champ de
description d'une entrée /etc/passwd.

{: .alert .alert-info}
__ASTUCE__&nbsp;:
Rappelez vous que vous pouvez appeler install_package avec un seul
argument, si vous souhaitez que le nom d'utilisateur, le nom de groupe et la
description soient les mêmes.

Le répertoire /usr/src/linux-libc-headers sera mis en place en tant que
répertoire home pour l'utilisateur-paquet, rempli automatiquement avec le
contenu de /etc/pkgusr/skel-package. La commande install_package appelle aussi
la commande "su - linux-libc-headers" pour passer avec l'identité de
l'utilisateur nouvellement créé. Si vous utilisez des scripts bashrc et
bash_profile de l'archive more_control_helpers, vous serez mis directement dans
le répertoire /usr/src/linux-libc-headers et votre prompt devrait ressembler à
ceci&nbsp;:

```bash
package linux-libc-headers:/usr/src/linux-libc-headers>
```

pour vous montrer que vous travaillez en tant qu'utilisateur-paquet
linux-libc-headers et que votre répertoire de travail courant est
/usr/src/linux-libc-headers.

Utilisez la commande
   
```bash
echo $PATH
```

pour vérifier que votre chemin commence par "/usr/lib/pkgusr", le répertoire qui
contient les enveloppes, et se termine par "/tools/bin".

Maintenant tout est prêt pour l'installation du paquet selon les instructions
contenues dans le livre LFS. Notez que, au moment d'écrire ces lignes, le livre
LFS vous demande d'exécuter une commande chown pour s'assurer que les en-têtes
sont détenues par root. C'est juste parce que le mainteneur a fait une erreur
très ordinaire lors de la création de l'archive pour les en-têtes&nbsp;:
il a archivé les fichiers avec un utilisateur/groupe non-root. Lors du déballage
d'une telle archive en tant que root, les fichiers finissent par appartenir à
par certaine combinaison bizarre utilisateur/groupe, ce qui peut ouvrir un trou
de sécurité. Lorsque vous travaillez en tant qu'utilisateur-paquet cela ne peut
pas arriver et vous ne voulez pas des en-têtes avec chown root:root, parce que
cela irait à l'encontre de l'ensemble de l'installation des en-têtes avec un
utilisateur-paquet. C'est l'un des petits points sur lesquels vous aurez à vous
écarter des instructions standards de LFS lors de l'utilisation
des utilisateurs-paquets. Vous pouvez trouver plus de problèmes liés à
l'utilisateur-paquet avec le livre LFS dans la section suivante.

Après avoir installé les en-têtes, il suffit de lancer la commande

```bash
exit
```
    
pour devenir à nouveau root. Maintenant c'est le bon moment pour penser aux
personnalisations utiles pour les fichiers /etc/pkgusr/{bash_profile,bashrc}
et/ou /etc/pkgusr/skel-paquet, si vous ne les avez pas déjà personnalisés.
Une fois que vous êtes satisfait de votre configuration, installez le reste
des paquets. La section suivante vous aidera à résoudre certains problèmes que
vous rencontrerez.


### 7.3 Problèmes connus avec des paquets LFS

Cette section contient des détails sur les problèmes liés à la méthode de
l'utilisateur-paquet auxquels vous devrez faire face lors de la construction de
votre système LFS. Vous devriez copier les informations de cette section
d'installation dans les Notes des dossiers des projets concernés, avec vos
propres notes.

{: .alert .alert-info}
__REMARQUE__&nbsp;:
Si vous construisez une version supérieure à 6.2 du livre LFS, il est
recommandé de lire complètement ce chapitre avant de commencer à construire
tous les paquets. Si votre version de LFS est la 6,2 alors il est bon de lire
cette section paquet par paquet en suivant la progression de votre
construction.


#### linux-libc-headers
Au moment d'écrire ces lignes le livre LFS vous dit d'exécuter une commande
chown pour s'assurer que les en-têtes appartiennent à root. Ceci est
juste parce que le mainteneur a fait une erreur très ordinaire lors de
la création de l'archive pour les en-têtes&nbsp;: il a archivé les fichiers en  
les attribuant à un utilisateur/groupe non-root. Lors du déballage d'une
telle archive en tant que root, les fichiers finissent par appartenir à
certaines combinaisons bizarres utilisateur/groupe, ce qui peut ouvrir un
trou de sécurité. Lorsque vous travaillez en tant qu'utilisateur-paquet,
cela ne peut se produire et vous ne voulez pas que les en-têtes
appartiennent à root:root, parce que cela irait à l'encontre de
l'ensemble de l'installation des entêtes avec un utilisateur-paquet.

Il y a une autre erreur classique dans l'archive du paquet 
linux-libc-headers.
La version 2.6.12.0 (la dernière au moment de cette astuce) n'en a pas
d'autres, mais les versions plus anciennes contenaient des fichiers avec
les droits mal initialisés.
Toutes les en en-tête sont supposées lisibles par tous, mais ce n'étaient 
pas le cas. Plus de détail sur cela dans la note concernant glibc


#### man-pages
Si le nom que vous utilisez pour l'utilisateur-paquet man-pages n'est pas
exactement "Man-pages", alors vous aurez à changer la variable
"manpagesowner" tout au début du script enveloppe `install`.

Les versions récentes de man-pages contiennent les pages de manuel POSIX que
le paquet essaie d'installer dans /usr/share/man/man{0,1,3}. Il y a aussi
une page de manuel qui veut être installée dans /usr/share/man/man9.
Comme /usr/share/man n'est pas un répertoire d'installation et que le livre
LFS n'a pas les instructions pour créer ces répertoires au moment où 
j'écris, l'installation échouera et les pages de manuel concernées ne
seront pas installées. Remèdes possibles&nbsp;:

1. Faire de /usr/share/man un répertoire d'installation.
   Conséquence&nbsp;: Tous les paquets seront en mesure de créer des
   sous-répertoires nouveaux dans /usr/share/man. Je ne trouve pas cela
   souhaitable, car des paquets pourront créer des répertoires pour des
   pages de manuel en langues étrangères que vous ne voulez pas. C'est
   vous qui voyez.
2. Ignorer le problème et vivre sans les pages de manuel POSIX. À moins
   que vous ne soyez un développeur (y compris de scripts) qui est
   intéressé par l'écriture de programmes/scripts portables ceci est une
   bonne solution.
3. Créer les répertoires /usr/share/man/man{0,1,3} et man9 en tant que
   root avant d'installer man-pages. Vous devrez donc soit les attribuer
   à l'utilisateur-paquet man-pages, soit les passer en répertoires
   d'installation. C'est ma solution préférée.


#### glibc
Il est en quelque sorte malheureux que l'erreur d'emballage de
libc-linux-headers concernant les autorisations n'existe plus dans la
version actuelle. Cela permettait une excellente expérience d'apprentissage.
J'ai gardé la section suivante dans l'astuce pour cette raison, même si
elle ne s'applique plus. Merci de prendre le temps de la lire.
    
<!-- ------------------ début de l'ancien propos ------------------------------ -->
>    À cause d'une erreur, Les en-têtes dans /tools/include ne sont pas
>    lisibles par tous. Malheureusement le livre LFS (au moment de cette 
>    écriture) ne corrige pas cela dans le chapitre 5 comme il le fait dans le
>    chapitre 6. Pour la construction d'une LFS standard ce n'est pas un
>    problème, parce que la glibc est construite en tant que root et root peut
>    accéder à tout indépendamment des autorisations.
>    Par contre, l'utilisateur-paquet glibc, n'a pas la permission d'accéder à
>    ces en-têtes. Cela entraînera plusieurs échecs des tests configure, parce
>    que les programmes de test respectifs ne peuvent pas être compilés.
>    Le résultat final est le message d'erreur "/lib/cpp échoue le test de
>    validation», ce qui est complètement absurde car nous n'avons pas
>    /lib/cpp.
>    
>    C'est l'occasion parfaite d'introduire la règle n ° 1 du diagnostic
>    d'erreur&nbsp;:
>    
>        NE JAMAIS FAIRE CONFIANCE AUX MESSAGES DE DIAGNOSTICS&nbsp;!
>       
>    Il existe 2 types de messages de diagnostic&nbsp;:
>    
>    1. Ceux qui sont inutiles, car une fois que vous voyez quel composant a
>       a échoué, la source du problème est évidente.
>    2. Ceux qui sont une grossière erreur de diagnostic de la source du
>       problème et vous amènent à tirer de fausses conclusions.
>    
>    Non, il n'y en a pas d'autre. Croyez-moi ;-)
>
>    Dans ce cas, /lib/cpp n'a rien à voir avec le problème. Il n'existe pas
>    et c'est très bien. Le message veut juste vous inciter à faire
>    quelque chose de stupide, créer un lien symbolique /lib/cpp ->
>    /tools/bin/cpp. Mais ça n'irait pas du tout. Avant de vous lancer dans
>    des conclusions prématurées vous devriez toujours essayer d'obtenir autant
>    d'informations *bas niveau* que vous pouvez. Les messages de diagnostic
>    sont des informations de *haut niveau*. Ils représentent une vue filtrée
>    du problème, qui est généralement de peu d'aide. Heureusement le message
>    (celui complet, et non pas la partie citée ci-dessus) pointe égaleme>nt vers
>    la source nécessaire pour les informations de bas niveau. Dans ce cas
>    c'est le fichier config.log (à ne pas confondre avec configure.log, le
>    fichier créé par le script de compilation inclus dans l'archive
>    more_control_helpers). config.log est créé par tous les "configure" créés
>    par autoconf (et pas seulement ceux de glibc) et il contient, entre autres
>    choses, les programmes de test utilisés par "configure" et la sortie des
>    messages pendant leur construction et leur exécution. Chaque fois qu'un
>    script "configure" échoue ou donne des résultats bizarres, vérifiez
>    config.log. Et toujours souvenez-vous toujours de la règle n°2 du diagnostic
>    d'erreur
>    
>        TOUJOURS COMMENCER PAR LA PREMIÈRE ERREUR
>
>    Cela semble assez évident, mais néanmoins les gens font couramment
>    exactement l'opposé. C'est juste trop tentant de commencer par le dernier
>    échec et essayer de travailler à reculons. Dans ce cas, beaucoup de gens
>    auraient ouvert config.log et fait défiler jusqu'au point d'échec du
>    test de cohérence /lib/cpp. Après tout, c'est ce qui cause l'erreur à
>    "configure" et c'est ce qui doit être corrigé, non? FAUX!
>    Quelqu'un qui adopte cette approche voit juste le message d'erreur
>    "/lib/cpp: No such file or directory" et est encore plus convaincu que le
>    lien symbolique (ou le programme) manquant /lib/cpp est le problème.
>    
>    La bonne manière d'aborder un tel problème est de commencer au début
>    de config.log, pour faire défiler vers le bas jusqu'au premier message
>    d'erreur et de vérifier si c'est un point qu'il faut corriger (les
>    messages d'erreur dans config.log ne sont pas toujours les signes d'un
>    problème). Si le problème doit être corrigé, alors il doit être corrigé en
>    premier, parce que toutes les erreurs suivantes pourraient être la
>    conséquence de cette erreur (même si, non, *en particulier* si vous ne    
>    croyez pas que c'est le cas). Si nous appliquons ce conseil à notre
>    problème donné, nous avons rapidement nous rendre à la première erreur
>    grave de config.log:
>
>    ```"/tools/include/linux/limits.h : Permission denied"```
>    
>    Une vérification rapide avec ls révèle en effet que le répertoire avec les
>    en-têtes linux n'est pas lisible par tous ce qui est évidemment une erreur.
>    Le correctif est facile. Il suffit de rendre (en tant que root) les
>    répertoires d'en-tête /tools/include/{linux,asm} lisibles par tous avec des
>    commandes similaires à celles que le livre LFS présente dans le chapitre 6
>    pour l'installation de linux-libc-headers. Une fois que cette modification
>    a été apportée, le "configure" de glibc réussit.

<!-- ------------------- fin de l'ancien propos --------------------------- -->

{: .alert .alert-info}
__ASTUCE__&nbsp;:
Même lorsque "configure" se termine avec succès, vous devriez toujours
vérifier la sortie soigneusement pour voir s'il n'y a pas quelque chose
de bizarre. Par exemple si vous utilisez les enveloppes, vous devez
vérifier que "configure" indique la ligne

```
checking for a BSD-compatible install... /usr/lib/pkgusr/install -c
```

Si configure détecte une autre installation que /tools/bin/install, quelque
chose est faux. Peut-être il y a une faute
de frappe dans le chemin (PATH) de l'utilisateur-paquet, ou vous avez mis
les enveloppes dans un répertoire qui est ignoré par "configure".

Avec les enveloppes la construction et l'installation de glibc devrait bien
fonctionner. Le script enveloppe d'installation permet de s'assurer que les
répertoires /usr/share/locale/* deviennent des répertoires d'installation
afin que d'autres programmes puissent installer leurs messages localisés.
Une chose que les enveloppes ne prennent pas en charge, cependant, est
le fichier /usr/share/info/dir. Parce que dans l'ordre de construction de
la LFS actuelle glibc est le premier paquet qui installe les fichiers
info, dir, détenus et seulement accessibles en écriture à glibc. Afin de
permettre à d'autres paquets d'installer des pages info, exécuter les
commandes suivantes en tant que root&nbsp;:

```bash
chown root:install /usr/share/info/dir &&
chmod ug=rw,o=r /usr/share/info/dir
```

{: .alert .alert-info}
__REMARQUE__&nbsp;:
glibc veut installer le programme pt_chown en tant que setuid root. Si vous
l'installez en tant qu'utilisateur-paquet, le programme sera installé, mais
pas avec les privilèges root (à cause de l'enveloppe install). L'info
suivante est tirée de la documentation de la glibc&nbsp;:

Un programme auxiliaire, "/usr/libexec/pt_chown", est installé avec un
setuid Root. Ce programme est appelé par la fonction "grantpt", il définit
les autorisations sur un pseudo-terminal de sorte qu'il peut être utilisé
par le processus d'appel. Cela signifie que des programmes comme "xterm"
et "screen" n'ont pas à avoir un setuid pour obtenir un pty. (Il peut y
avoir d'autres raisons pour qu'ils aient besoin de ce privilège.) Si vous
utilisez un noyau Linux 2.1 ou plus récent avec les systèmes de fichiers
`Devptsfs` ou `devfs` fournissant des esclaves PTY, vous n'avez pas besoin
de ce programme. Le source de "pt_chown" est dans
"login/programmes/pt_chown.c".

Donc, sauf si vous construisez un système qui n'utilise pas devpts (ce qui
serait tout à fait inhabituel), il n'y a pas besoin de s'inquiéter.


{: .alert .alert-info}
__ASTUCE__&nbsp;:
Dans le cas où vous vous demandiez si vous devez créer les fichiers
/etc/nsswitch.conf et /etc/ld.so.conf en tant que root ou glibc, je vous
recommande d'attribuer tous les fichiers que vous créez manuellement ou
que vous modifiez manuellement au compte root. De cette façon, vous pouvez
faire la distinction entre les fichiers qui peuvent être régénérés
automatiquement et ceux qui ne peuvent pas. L'attribution à root même
des fichiers générés automatiquement la première fois que vous les éditez
à la main assure que, plus tard, la réinstallation du paquet n'écrasera
pas silencieusement votre personnalisation manuelle.


#### ldconfig

Maintenant que glibc a installé /sbin/ldconfig vous pouvez activer
l'enveloppe ldconfig si vous le souhaitez. Effectuez les étapes suivantes
en tant que root APRES le réajustement de la chaîne d'outils, juste avant
de commencer binutils&nbsp;:

```bash
cd /usr/lib/pkgusr
gcc -O2 -W -Wall -o ldconfig ldconfig.c
chown root:install ldconfig
chmod u=rwxs,g=rxs,o= ldconfig
```

Ces instructions font que l'enveloppe ldconfig obtient un setuid root et
un setgid install et empêche les utilisateurs non-root qui ne sont pas dans
le groupe install de le lancer. Le setuid root est requis pour qu'il
puisse réécrire /etc/ld.so.cache. Le setgid install n'est pas
strictement requis, mais sans lui /etc/ld.so.cache appartient aux
groupes du dernier utilisateur-paquet qui y a touché. Si vous utilisez le
principe du nom d'utilisateur = nom du groupe, cela fera que les scripts
more_control_helpers penseront que /etc/ld.so.cache appartient au paquet 
en question, ce qui peut être source de confusion.

#### binutils

Avez vous autorisé /usr/share/info/dir en écriture pour le groupe comme
expliqué précédement dans les notes de glibc&nbsp;? Si vous avez oublié cette
partie, retournez-y et faite-le maintenant. L'installation de binutils 
devrait se faire sans problème. Cela va cependant entraîner des conflits
mineurs avec autoconf (voir plus loin).

{: .alert .alert-info}
__NOTE__&nbsp;:
Au moment d'écrire ces lignes la version de bash utilisée dans le livre LFS
a un bogue qui fait que le script list_package donne des erreurs et énumère
toutes les pages de manuel de binutils (et d'autres paquets) comme cassées.
Ce bogue est déjà corrigé par le correctif bash utilisé par le livre,mais le
correctif n'est pas appliqué dans le chapitre 5. Vu que la fonctionnalité de
synthèse des pages de manuel de list_package exige man, qui n'est installé
qu'après que bash est reconstruit, ce n'est pas vraiment important, parce   
que même si corriger bash au chapitre 5 permettrait de se débarrasser des
messages d'erreur, cela ne se traduirait pas par des résumés utilisables des
pages de manuel.
   
    
#### gcc
Le lien symbolique /usr/lib/libgcc_s.so.* créé au début du
chapitre 6 appartenant à root, l'installation de gcc ne peut pas
l'effacer. Vous devrez alors le retirer en tant que root avant de faire
"make install". Sinon utilisez
    
```bash
chown -h gcc: /usr/lib/libgcc*
```       

pour changer les permissions des fichiers en question après la création de
l'utilisateur-paquet gcc. Notez l'option -h qu'il faut utiliser pour
changer la permission des liens symboliques eux-mêmes plutôt que leurs
fichiers cibles.

#### db

Il devrait être évident que vous ne voulez pas modifier l'appartenance des
fichiers installés.
    
#### coreutils

Les liens symboliques  /bin/cat/bin/pwd et /bin/stty appartenant à
root, l'installation de coreutils ne peut pas les supprimer.
Vous devrez donc les enlever manuellement avant de faire les "make
install". Sinon utiliser la commande

```bash
chown -h coreutils: /bin/{cat,pwd,stty}
```
   
après la création de l'utilisateur-paquet coreutils. Notez l'option -h
qu'il faut utilisée pour changer la permission des liens symboliques
eux-mêmes plutôt que leurs fichiers cibles.

Les instructions du chapitre 6 déplace les binaires de coreutils dans /bin,
y compris le binaire de mv lui-même. Vous avez besoin d'être certain que le
hachage est désactivé pour que cela marche. Le livre LFS fait ceci en 
plaçant `set +h` dans le .bashrc de l'utilisateur LFS. Si vous suivez 
l'astuce, vous utilisez probablement les scripts "build", alors vous aurez
besoin de mettre cette commande dans le script "build" avant les commandes
mv.

{: .alert .alert-info}
__REMARQUE__&nbsp;:
Le paquet man-pages a déjà installé les pages de manuel pour les
binaires de coreutils. L'enveloppe install permettra d'éviter de
les écraser par ceux de coreutils. Ceci est fait parce que les pages de
manuel du paquet man-pages sont de qualité supérieure (mais cependant pas
nécessairement à jour). Cela empêche également les erreurs lors du "make
install" qui autrement se produiraient parce que l'utilisateur-paquet
coreutils ne peut pas écraser les pages de manuel appartenant à un autre
utilisateur. Si vous n'aimez pas le comportement ci-dessus et voulez
plutôt avoir le paquet original des pages de manuel (car elles sont à jour),
vous pouvez définir la variable "manpagesowner" au début de l'enveloppe
d'install avec une chaîne qui ne correspond pas à un nom
d'utilisateur-paquet (par contre elle ne doit pas être vide&nbsp;!).
Si vous faites cela, vous aurez à résoudre des conflits de pages de manuel
d'une autre manière. La meilleure façon de gérer cela est probablement de
ne pas installer le paquet des pages de manuel au début du chapitre 6,
mais à la fin, après que tous les autres paquets aient déjà installé leurs
pages de manuel. Ensuite, vous ne devrez traiter qu'une seule fois les
conflits, lors de l'installation de man-pages.


#### ncurses

L'installation de ncurses (comme celle d'autres paquets qui incluent des
bibliothèques) veut lancer /sbin/ldconfig pour mettre à jour
/etc/ld.so.cache. Cela échoue parce que l'utilisateur-paquet n'a pas
l'autorisation de modifier /etc/ld.so.cache. Rendre le fichier
/etc/ld.so.cache inscriptible pour le groupe install ne vous aidera
pas, parce que les autorisations seraient réinitialisées par le prochain
appel à /sbin/ldconfig. Cette erreur ne bloquera généralement pas
l'installation et vous pouvez juste exécuter /sbin/ldconfig manuellement
en tant que root par la suite. Sinon vous pouvez utiliser l'enveloppe
ldconfig décrite précédemment.

#### répertoire aclocal

Au moment de cette écriture, le répertoire /usr/share/aclocal est
créé pendant l'isntallation de bison. Ce répertoire contient des macros
pour autoconf. D'autres paquets veulent installer des fichiers dans ce
répertoire, donc vous devriez autoriser l'écriture pour le groupe
install avec le bit sticky.

#### perl

Avant de faire "make install", vous devrez faire "chown -h perl:
/usr/bin/perl" afin que l'utilisateur-paquet perl soit autorisé à
retirer le lien symbolique vers le répertoire /usr/bin/perl.

Si vous allez installer des paquets supplémentaires pour perl avec leurs
propres utilisateurs-paquets dans /usr/lib/perl5/site_perl, alors vous
aurez besoin de changer /usr/lib/perl5/site_perl/ et ses sous-répertoires
en répertoires d'installation. Vous n'avez pas besoin de le faire
maintenant et vous le remarquerez de toute façon quand l'installation
d'un supplément perl échouera.


#### autoconf

Le paquet autoconf veut installer sa propre copie de standards.info,
qui échoue parce que binutils a déjà installé ce fichier. Vous pouvez soit
ignorer l'erreur soit supprimer la version de binutils standards.info
avant le "make install".


#### bash
##### configure

Le script de configuration de bash teste la présence des périphériques
spéciaux /dev/std* et /dev/fd/*. Malheureusement au moment de la
rédaction de cet article le test de /dev/fd/* est bogué (le test utilisé
pour /dev/stdin ne fonctionne pas, également en bash-2.x, mais a été
corrigé depuis). Il finit par tester l'accès en lecture à l'entrée
standard, qui est le terminal (pseudo) dans lequel vous construisez votre 
système.
Malheureusement su ne modifie pas l'appartenance du terminal, alors
quand vous passez avec su à un compte utilisateur-paquet , le terminal
appartient toujours à l'utilisateur de connexion. Comme l'utilisateur-paquet
n'a pas accès en lecture au périphérique, les tests échouent.

Il y a une façon simple de contourner cela. Lancez simplement ./configure
comme ceci

```bash
./configure ....  </dev/null
```

L'astuce ici est de rediriger l'entrée standard (à noter qu'il s'agit d'un
'&lt;' pas un '&gt;'!) vers /dev/null. Contrairement au périphérique terminal,
/dev/null est lisible et inscriptible par tous, de sorte que les tests 
réussiront comme ils le devraient. Si vous n'aimez pas cette astuce, vous 
pouvez aussi faire un chown sur le terminal en question (voir `ls -la /dev/fd/0`)
pour l'utilisateur-paquet avant la construction de bash.
    
##### make check

Lors de l'exécution de la suite de test en tant qu'utilisateur-paquet, le
test "run-test" échouera avec une sortie comme&nbsp;:

```
  33d32
  < *** chmod g+s /tmp/test.setgid
  35c34
  < 1
  ---
  > 0
  64d62
  < *** chmod u+s /tmp/test.setuid
  66c64
  < 1
  ---
  > 0
  154c152
  < 1
  ---
  > 0
  160c158
  < 1
  ---
  > 0
```

Les 2 premiers échecs sont causés par l'enveloppe chmod qui empêche le
test de définir les bits setuid et setgid, et sort l'avertissement ***.
Les échecs sont inoffensifs. Vous pouvez les éviter si vous retirez les
répertoires des enveloppes du PATH avant d'exécuter les tests.

Les deux derniers échecs ne sont pas spécifiques aux utilisateurs-paquets,
mais surviendront chaque fois que vous lancerez les tests avec le su d'un
autre utilisateur. Les raisons sont les mêmes que pour les problèmes de
configuration décrits précédemment. Appliquer la même solution.
Utilisez soit chown (si vous avez utilisez chown avant la configuration,
bien sur ) ou lancez "make check" comme ceci
   
```bash
make check </dev/null
```
    
##### make install

Avant que vous puissiez faire `make install` vous avez besoin de `chown -h bash: /bin/bash`
en tant que root pour que l'installation de bash puisse
remplacer le lien symbolique /bin/bash que vous avez manuellement créé au
début du chapitre 6.

#### Répertoire pkgconfig

Au moment de la rédaction le répertoire /usr/lib/pkgconfig est
créé pendant l'installation de e2fsprogs. Ce répertoire contient des
informations de construction utilisées par les outils de pkg-config.
D'autres paquets veulent installer des fichiers dans ce répertoire,donc
vous devriez autoriser l'écriture pour le groupe install avec le bit
sticky.
   
#### grub

Les commandes pour créer et peupler /boot/grub doivent être exécutées comme
root.

#### grep

Avant de faire `make install` vous devez faire `chown -h grep: /bin/grep`
en tant que root pour que l'installation de bash puisse remplacer le lien
symbolique /bin/bash que vous avez manuellement créé au début du chapitre 6.
    
#### inetutils

Ce paquet contient quelques programmes qui veulent être setuid root&nbsp;:

   rsh, rcp, rlogin et ping

L'enveloppe install empêche ces programmes de le devenir.
Vous devez décider les programmes que vous voulez rendre 
setuid root et le faire manuellement. Soyez prudent. Ne rendez pas un
binaire setuid root sans que vous soyez *certain* que les utilisateurs
ordinaires ne peuvent pas vivre sans lui. Tous les binaires setuid root
sont des trous de sécurité potentiels.

#### iproute2

Ce paquet tente de changer les permissions de /usr/sbin et de quelques
répertoires man. L'enveloppe d'installation s'en charge.


#### man-db

Juste après l'installation de man-db vous n'avez pas les listes des pages de
manuel de list_package, car la façon dont list_package appelle man
nécessite col pour travailler et col est installé avec util-linux. Vous
pouvez cependant installer util-linux plus tôt. L'ordre
alphabétique est la seule raison qui fait qu'il est à la fin du chapitre 6.

#### shadow

Par défaut shadow veut installer les pages de manuel non anglophones. Cela
échoue parce que le répertoire /usr/share/man n'est pas un répertoire
d'installation et donc les utilisateurs-paquets ne sont pas autorisés à
créer de nouveaux sous-répertoires.Pour résoudre ce problème, avant que
vous fassiez "make install", ouvrez le fichier man/Makefile, cherchez la
ligne

```
SUBDIRS = cs de en es fr hu id qu'il ja ko pl ru pt_BR zh_CN zh_TW
```

et supprimez toutes les langues que vous ne souhaitez pas installer. Pour
les langues que vous souhaitez installer, créez des répertoires avec leur
nom respectif dans /usr/share/man en tant que root et rendez-les répertoires
d'installation (c'est à dire qu'ils appartiennent au groupe d'installation,
qu'ils sont autorisés en écriture pour le groupe, avec le bit sticky).

Il y a encore un autre problème avec les pages de manuel relatives à shadow.
Le paquet shadow contient des pages de manuel passwd.5 et getspnam.3.
L'installation de ces pages de manuel est automatiquement supprimées par
l'enveloppe install, car elle écraserait les pages de manuel
fournie par le paquet man-pages. Comme d'habitude la version de man-pages
est meilleure, donc vous pouvez simplement ignorer cette question.

Shadow veut installer les programmes su, chage, chfn, chsh, expiry, gpasswd,
newgrp et passwd avec le setuid root. Vous aurez besoin de décider lesquels
de ces programmes auront un setuid root et le faire manuellement.


#### sysklogd

Le makefile de sysklogd a /usr/bin/install d'écrit en dur comme programme
d'installation, ce qui contourne l'enveloppe install. L'enveloppe
est nécessaire pour sysklogd car il essaie de faire appartenir ses pages
de manuel à root. (Ce que de toute évidence un utilisateur-paquet n'est
pas autorisé à faire).
Par conséquent, installez-le avec

```bash
make INSTALL=install install
```
    

#### udev

udev veut installer des fichier dans le répertoire /usr/lib/pkgconfig. Si
vous avez suivi les instructions données précédemment, vous avez déjà fait
en sorte que ce répertoire soit un répertoire d'installation. Si vous ne
l'avez pas fait, faites-le maintenant ou l'installation de udev échouera.

Les instructions de LFS pour installer udev vous demandent d'exécuter la 
commande
  
```bash
mknod -m0666 /lib/udev/devices/null c 1 3
``` 

Comme un utilisateur-paquet n'est pas autorisé à créer un noeud de
périphérique, exécutez cette commande en tant que root.
    

#### util-linux

util-linux veut installer write comme setgid tty et le de/monter avec un
setuid root. Les enveloppes interceptent cela, afin de ne pas provoquer
d'échec d'installation, mais comme d'habitude, vous aurez à décider si
vous voulez que ces programmes aient des privilèges spéciaux et à faire
une action manuelle en tant que root, si tel est le cas.


## 8. Vérification de propreté

### 8.1 fichiers suspects
 
Vous avez probablement parcouru la commande "list_package" pour chaque paquet et
examiné les résultats qui incluent des fichiers suspects appartenant à ce
paquet. Mais même si vous savez que c'est toujours une bonne idée d'exécuter la
commande non spécifique aux paquets "List_suspicious_files" une fois que votre
construction est terminée. Il pourrait y avoir quelque chose que vous avez
négligé la première fois, ou peut-être que vous ayez créé un fichier en tant que
root avec les autorisations erronées. Il ne fait pas de mal de vérifier à 
nouveau et ce sera aussi l'occasion d'examiner toutes les décisions
setuid/setgid que vous avez prises avec les binaires installés.

{: .alert .alert-info}
__ASTUCE__&nbsp;:
Lorsque vous vérifiez la liste des fichiers setuid et setgid, n'oubliez pas de
regarder l'utilisateur ou le groupe réels propriétaires du fichier. Il est
facile d'oublier cela, surtout dans le cas setuid, comme nous assimilons
souvent setuid avec setuid root puisque setuid est rarement utilisé avec
d'autres comptes utilisateurs.


  
### 8.2 Références aux fichiers temporaires

Une grande préoccupation lors de la construction d'un système LFS est
l'indépendance du nouveau système LFS vis-à-vis des fichiers installés dans
/tools. Le répertoire /tools est destiné à être temporaire et il devrait être
possible de le supprimer après la construction de votre système LFS, sans effets
secondaires indésirables. Le script "grep_all_regular_files_for" du paquet
more_control_helpers peut vous aider à vérifier que votre nouveau système LFS
est vraiment propre. La commande

```bash
grep_all_regular_files_for /tools
```

vous donnera une liste de tous les fichiers qui contiennent la chaîne "/tools".
Passez en revue les fichiers dans la liste pour s'assurer qu'il n'y a pas de
dépendances des fichiers temporaires dans /tools qui se sont glissées. Mais
rappelez-vous que les résultats à partir des binaires et des bibliothèques ne
sont significatifs qu'après avoir enlevé des informations de débogage, car les
informations de débogage comprennent nécessairement des références à
l'environnement de compilation. Bien sûr, si vous êtes un développeur qui va
potentiellement exécuter gdb sur les bibliothèques/binaires systèmes, la
position qui serait de dépouiller des informations de débogage est la mauvaise
façon de faire disparaître les références à /tools. L'autre façon de faire face 
à cela est de reconstruire les paquets dont les références /tools sont
signalées. La nouvelle construction n'inclura pas tous les fichiers de /tools et
donc les informations de débogage de la nouvelle construction ne feront pas
référence à /tools. Notez que les instructions de LFS de compilation pour la
glibc font compiler glibc contre /tools/glibc-kernheaders. Sauf si vous copiez
le répertoire glibc-kernheaders à un emplacement hors de /tools et compiler
contre cette copie, vous ne pourrez pas vous débarrasser des références de
/tools dans les informations de débogage de glibc. Quelle que soit la façon dont
vous choisissez de traiter la question des informations de débogage, à la fin,
vous devriez avoir un système où la commande ci-dessus ne produit que des faux 
positifs (tels que "perlfaq3.1", qui inclut l'URL
"http://www.research.att.com/sw/tools/uwin/») et les fichiers qui font
légitimement référence à /tools (comme une copie de cette astuce).


Annexes
=======

## Annexe A&nbsp;: Problèmes de sécurité

### A.1 NFS

Si vous utilisez le système de fichiers réseau NFS, il y a quelques choses que
vous devez regarder en utilisant le système de l'utilisateur-paquet. Un problème
fondamental de sécurité avec NFS est qu'il donne aveuglément les UID et GID au
client. Si un pirate peut accéder au compte root sur un système dans votre
réseau qui est autorisé à monter des partages NFS à partir de votre serveur, ou
si le pirate peut attacher son propre ordinateur à votre réseau, alors ce pirate
peut prétendre être n'importe qui. NFS se fera un plaisir de permettre
au pirate de travailler dans le répertoire NFS exporté comme n'importe quel
utilisateur. La seule exception est le compte root. NFS exporte par défaut
les répertoires avec l'option root_squash qui relie toutes les
demandes entrantes provenant d'uid 0 à anonuid (65534 au moins défini dans
/etc/exports) et le gid 0 à anongid (65534 au moins défini dans /etc/exports).
Cela protège les fichiers appartenant à root:root. Sur un système normal cela
inclut la plupart des fichiers dans /bin, /etc, /lib et la plupart des autres
répertoires, sauf pour /home. Si vous utilisez l'utilisateur-paquet, cependant,
la plupart de ces fichiers appartiennent à des utilisateurs-paquets. Ces
fichiers ne sont pas protégés par l'option root_squash. Afin de rendre les
exportations NFS sécurisées, vous devez ajouter l'option "all_squash" à chaque
entrée dans /etc/exports qui contient des fichiers appartenant à des
utilisateurs-paquets. Notez que all_squash est toujours une bonne idée car même
les systèmes qui n'utilisent pas les utilisateurs-paquets ont souvent des
programmes appartenant à d'autres utilisateurs ou groupes, et ils ont besoin
d'être setuid ou setgid.


### A.2 Démons


C'est une pratique courante, par mesure de sécurité, d'exécuter les démons
sous des comptes utilisateur spéciaux plutôt qu'en tant que root. Si vous vous
sentez tenté d'utiliser un compte utilisateur-paquet pour cet effet, résistez à
la tentation. Ce serait une idée très stupide. Bien qu'ils soient délibérément
moins puissant que root, les comptes des utilisateurs-paquets n'en restent pas
moins privilégiés et doivent être considérés comme équivalents au compte root
pour les questions de sécurité. Ne faite pas des choses avec un
utilisateur-paquet que nous ne feriez pas avec root sur un système sans les
utilisateurs-paquets.


## Annexe B&nbsp;: Catégories de paquet

Bien que le schéma nom d'utilisateur = nom du groupe est recommandé par cette
astuce, il n'est pas le seul possible. Un autre système qui a un certain attrait
est de définir des catégories de paquets et d'utiliser le groupe dans le but de
catégoriser les paquets. Ce qui suit est un ensemble de catégories qui peut 
servir de guide pour la mise en ½uvre de cette astuce.

*      devel&nbsp;:

       les choses en rapport avec le développement, par exemple les  
       compilateurs. Ce n'est pas limité aux développement de logiciels.
       TeX, par exemple, appartiendrait à ce groupe.
       
*      utils&nbsp;:

       La plupart des logiciels s'inscrit dans cette catégorie, même un
       logiciel peu essentiel comme grep ou des éditeurs de texte.
      
*      net&nbsp;:

       les choses en rapport avec le réseau comme un démon FTP ou un navigateur
       Web. Ce groupe chevauche dans une large mesure d'autres groupes. Il 
       devrait être utilisé de préférence aux autres groupes à
       chaque fois qu'un paquet est clairement axé vers Internet, LAN, WWW,...
       Un outil comme wget, par exemple, irait dans net plutôt que dans
       utils. Des exceptions à cette règle sont les groupes docs, addons, jeux
       (games) et mmedia. Si un paquet s'insère dans un de ces groupes, utilisez
       le groupe en question au lieu de net.
     
*      docs&nbsp;:

       les paquets relatifs à la documentation, comme une archive avec des
       guides pratiques Linux. Notez que les logiciels pour créer des documents
       tels que les processeurs XML devraient probablement aller dans 
       devel et les logiciels pour afficher ou faire du post-traitement
       de la documentation tels que man ou groff devrait probablement aller dans
       utils.
      
*      system&nbsp;:

       les logiciels systèmes importants, tels que bash. Ce groupe devrait
       être utilisé uniquement pour les paquets vraiment essentiels. La plupart
       des paquets que vous mettriez dans ce groupe sont mieux dans "utils". Vi,
       par exemple, appartient à utils. Il est peu probable que tous les paquets
       faisant partie de la base de LFS appartiennent à ce groupe système.
        
*      libs&nbsp;:

       utils est pour les exécutables, libs est pour les bibliothèques. Les
       bibliothèques qui ne sont pas fortement liées à l'une des autres
       catégories doivent aller ici, comme zlib ou libpng. Les bibliothèques
       systèmes essentielles telles que glibc, ncurses ou gettext devraient 
       plutôt aller dans le groupe system. Le groupe libs est également utilisé 
       pour les environnements d'exécution tels que Java Virtual Machine
       (machine virtuelle Java), Dosemu et wine. D'autres émulateurs comme
       MAME devraient probablement plutôt aller dans games (jeux).
     
*      jeux (games)&nbsp;:

       vous vous attendez à quoi ;-)

*      mmedia&nbsp;:

       C'est le groupe pour l'audio et les éditeurs vidéo, lecteurs mp3 etc

*      apps&nbsp;:

       Les applications telles que les tableurs et les traitements de texte (pas
       les éditeurs de texte) mais aussi les logiciels de CAO et les
       logiciels de graphisme comme Gimp. Le groupe apps est un peu comme utils,
       mais les applications sont généralement plus conviviales pour
       l'utilisateur, plus simple que dans utils.
      
*      addons&nbsp;:

       suppléments, filtres et autres qui sont destinés à être utilisés en
       conjonction avec un autre paquet.
       
*      x&nbsp;:

       un logiciel qui se rapporte au système X Window en général, et ne rentre
       pas dans une des autres catégories, telles que le serveur X lui-même
       ou un gestionnaire de fenêtres. La plupart des logiciels X devraient être
       mis dans d'autres groupes plus spécifiques. Un jeu comme xmines irait
       dans jeux (games), par exemple, et un éditeur de texte pour X irait
       dans utils.
   
*      KDE&nbsp;:

       logiciels qui se rapportent à KDE et ne rentrent pas dans une autre
       catégorie. Ce groupe devrait être utilisé avec précaution.
       *Ne* l'utilisez *pas* pour tous les logiciels KDE. DesktopK, par exemple,
       appartient a apps. Konqueror appartient a net.
     
*      gnome&nbsp;:

       logiciels qui se rapportent à GNOME et ne rentrent pas dans une
       autre catégorie. Ce groupe devrait être utilisé avec précaution.
       *Ne* l'utilisez *pas* pour tous les logiciels GNOME. Gimp par exemple,
       appartient à apps. Un gestionnaire de fenêtres basé sur GNOME qui
       fonctionne avec X devrait aller dans le groupe x.


## Annexe C&nbsp;: Remerciements et historique des changements

### Remerciements

*   Matthias Benkmann pour avoir écrit la version originale de cette astuce
*   Tushar Teredesai pour avoir suggéré le schéma utilisateur = groupe.
*   Markus Laire pour avoir signalé le bogue de construction le 01/01/2005

### Historique des changements

20/10/2007 Matthias Benkmann

-   modification de la licence en CC-BY-SA (avant CC-BY-ND).
-   ajout des noms dans les modifications pour que l'astuce puisse être
    maintenue par différents auteurs.
-   ajout d'une solution pour list_package pour le bogue dans man-db qui
    fait que quelques pages de manuel sont vues comme "Weird
    manpage" (page de man bizarre) dans le sommaire.
-   l'enveloppe chmod empêche maintenant shadow d'installer des fichiers
    setuid shadow.
-   ajout d'une enveloppe pour résoudre les problèmes de ldconfig.
-   install_package fonctionne maintenant quand il est appelé avec un
    seul argument. Cet argument est utilisé pour le nom d'utilisateur,
    le nom du groupe et la description.
-   bash_profile de more_control_helpers maintenant a /sbin et
    /usr/sbin dans le PATH pour correspondre au PATH utilisé par root
    pendant la construction.
-   install_package peut maintenant faire su - &lt;nom&gt; (c'est à dire
    démarrer un shell de connexion).
-   le script build gère maintenant le déballage des archives et permet
    d'appeler les différents étages individuellement.
-   useradd utilise le shell fourni par -s et pas plus que bash ne
    permet de mémoriser.
-   Les notes de bash au chapitre 6 gèrent maintenant correctement les
    problèmes de configure et de make check.

21/03/2007 Matthias Benkmann

-   modification de forall_direntries_from pour éviter le message
    d'alerte de find quand -depth est utilisé.
-   ajout de 4.8 Quelles commandes lancer en tant qu'utilisateur-paquet
 

22/12/2005 Matthias Benkmann

-   ajout de conseils sur la façon faire avec le problème de déplacement
    de mv dans la note de coreutils.

13/11/2005 Matthias Benkmann

-   Correction de list_suspicious_files et list_package pour   
    fonctionner avec une version conforme à POSIX plus
    récente de GNU find
-   Publication de la version 1.2

01/01/2005 Matthias Benkmann

-   Correction d'un bogue dans le script skel-package/build qui
    rapportait toutes les étapes comme un succès, même si elles ne
    réussissaient pas
-   Publication de la version 1.1

01/11/2004 Matthias Benkmann

-   Mise en majuscule des titres
-   Version 1.0
            
14/10/2004 Matthias Benkmann

-   Début du développements des outils de more_control_helpers
            
14/08/2004 Matthias Benkmann

-   Début d'une réécriture majeure (mise à jour pour la
    nouvelle version LFS, nouveau format d'astuce, améliorations
    textuelles ,...)

20/04/2002 Matthias Benkmann

-   Changement des en-têtes de VERSION LFSF pour être plus
    conservateurs
-   Ajout des balises &lt;br&gt; dans le synopsis pour le bien de l'index de
    l'astuce
-   Ajout du groupe Mmedia à la liste des groupes suggérés
-   Soumission de la version v0.8

16/03/2002 Matthias Benkmann

-   Ajout d'une note, que le make de Linux n'a pas besoin d'être
    setgid kmem

18/02/2002 Matthias Benkmann

-   Ajout de la section "Les problèmes de sécurité avec NFS"
-   Soumission de la version v0.7

30/01/2002 Matthias Benkmann

-   Ajout de l'historique des changements
-   Déplacement de la commande "chown 0.10000.`cat /tmp/installdirs`"
    (avant la création de l'utilisateur-paquet glibc)
-   Add_package_user&nbsp;: création d'un un répertoire home avec
    "mkdir -p" utilisation de $grpfile partout au lieu de
    /etc/group
-   Amélioration de la phrase sur mammouth dans l'introduction
-   Ajout d'une note sur la possibilité d'avoir nom d'utilisateur ==
    nom du groupe
-   Source bashrc_basic dans bashrc_package
-   Modifications textuelles mineures

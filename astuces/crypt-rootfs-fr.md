---
layout: astuce
use-site-title: true
title: Chiffrer la racine.
author: Lars Bamberge <Lars.Bamberger at gmx dot de>
translator: Emmanuel Trillaud <etrillaud at gmail point com>
date: 2009-12-30
license: GFDL-1.2
synopsis: Comment chiffrer un système de fichiers y compris celui de la racine.
description: Ce document décrit un des moyens de chiffrer votre disque dur, y compris le système de fichiers racine. Il est destiné aux utilisateurs expérimentés et tente d'affronter les difficultés du chiffrement du système de fichiers racine de façon directe.
---

# PRÉREQUIS&nbsp;:

Cette astuce nécessite une connaissance suffisante de
BeyongLinuxFromScratch ainsi que des logiciels suffisamment à jour. Vous
devez être à l'aise pour construire un logiciel, trouver, lire et
comprendre toute autre documentation pertinente. Vous devez savoir
comment configurer un initramfs. (Voir
`filesystems/ramfs-rootfs-initramfs.txt` dans la documentation du
noyau). Vous devez connaître la raison pour laquelle vous voulez un
système de fichiers chiffré et vous devez comprendre les menaces contre
lesquelles vous essayez de vous protéger. Vous devez aussi comprendre
les insuffisances et les problèmes de sécurité si vous suivez les
instructions contenues dans cette astuce.

Vous devez avoir une sauvegarde complète de votre système dans un
endroit sûr&nbsp;! Ceci implique un périphérique de démarrage alternatif.

{: .alert .alert-danger}
Vous __DEVEZ ABSOLUMENT LIRE ET COMPRENDRE CETTE ASTUCE AVANT DE MODIFIER VOTRE SYSTÈME__&nbsp;!


# 1. C'est à quel sujet&nbsp;?

Cette astuce parle du chiffrement de toutes les partitions de votre
disque dur sauf une en utilisant LUKS pour dm-crypt. Nous démarrerons à
partir d'une petite partition non chiffrée en utilisant initramfs pour
déchiffrer le système de fichiers racine. Cette astuce suppose la
présence d'une petite partition disponible pour démarrer. La taille de
cette partition devrait être entre 10 et 15 Mo afin qu'elle puisse
contenir plus d'un noyau et plus d'une image initramfs pour les tests et
les mises à jour. Évitez de créer une partition plus grande car à chaque
démarrage une somme de contrôle de cette partition sera calculée, et
c'est un processus qui prend du temps.

# 2. Logiciels nécessaires et dépendances

## 2.1 Logiciels présents dans le livre BLFS

Vous avez besoin d'installer popt, car cryptsetup en dépend. Vous
aurez également besoin de uuencode pour créer les fichiers contenant
les clés. Uuencode est inclus dans shareutils et gmime qui a
d'autres dépendances listées dans le livre BLFS. Pour créer l'initramfs,
vous aurez besoin de cpio.

## 2.2 Logiciels absents du livre BLFS

### 2.2.1 devmapper

Récupérez-le sur [packages.debian.org/stable/source/devmapper](http://packages.debian.org/stable/source/devmapper)
Compilez et installez-le. Nécessaire à cryptsetup.

### 2.2.2 cryptsetup avec l'extension LUKS

Récupérez-le sur [code.google.com/p/cryptsetup](http://code.google.com/p/cryptsetup/).
Compilez et installez-le. Nécessaire pour gérer les partitions chiffrées.

### 2.2.3 busybox

Récupérez-le sur [www.busybox.net](http://www.busybox.net/). La configuration minimum
requise comprend&nbsp;:

* cp
* hush (shell interactif non requis)
* mount (avec le support pour de nombreux drapeaux -o ) et
* switch_root.

Compilez-le, mais __NE L'INSTALLEZ PAS__. Conservez l'exécutable et
nommez-le `busybox-minimum`. Ensuite, Re-configurez busybox pour un
environnement de bureau complet. Vous aurez besoin de tous les outils et
utilitaires standards pour le chiffrement initial de votre partition
racine et pour cibler les problèmes éventuels (N'oubliez pas `mkefs`).
Nommez cet exécutable `busybox-large` ou quelque chose d'approchant. De
même, il n'est pas nécessaire de l'installer.

# 3. Recompiler le noyau

Choisissez l'algorithme que vous voudriez utiliser pour chiffrer votre
disque dur. Notez que c'est une décision cruciale et vous devriez vous
renseigner plus avant. (Voir la section _Remerciements_). Les modules
appropriés doivent être compilés (en dur, pas comme modules) dans le
noyau. Par exemple, vous pouvez utiliser la méthode
`twofish-cbc-essiv:sha256`.

A partir du menu `Device Drivers` -> `Multiple devices driver support`
de la configuration du noyau, sélectionnez aussi les options
`Device mapper support` et `crypto target`.

Dans la section `Device Drivers` -> `Block Devices`, sélectionnez
`RAM block device support` , et dans `General setup`, sélectionnez
`Initial RAM filesystem`.

{: .alert .alert-info}
__NOTE__&nbsp;: vous devez démarrer ce nouveau noyau avant d'aller plus loin.

# 4. Chiffrer les partitions autres que le système de fichiers racine et autre que celle d'échange

Vous devez modifier votre système pour qu'il puisse gérer les partitions
chiffrées. Dans un premier temps, nous modifions le système pour qu'il
puisse gérer les partitions __autres__ que le système de fichiers racine. Il
est fortement recommandé que vous conserviez une sauvegarde de tous les
fichiers que vous modifierez par la suite.

## 4.1 Chiffrer les partitions

{: .alert .alert-info}
__NOTE__&nbsp;: Ce document décrit comment chiffrer chaque partition séparément.
Si vous avez plus d'un disque dur dans votre système, vous pourriez
vouloir chiffrer tout le périphérique, y compris la table de partitions.
Utiliser la méthode décrite dans ce document laisse la table des
partitions en clair, vous exposant à une attaque. Considérez ceci comme
un risque potentiel pour votre sécurité.

{: .alert .alert-danger}
__PIÈGES__&nbsp;! Si `/usr` est une partition séparée, cryptsetup et
toutes les bibliothèques nécessaires à son exécution doivent être placés
sur la partition racine. Utilisez `ldd cryptsetup` pour les trouver. Il
peut être nécessaire de passer au niveau d'exécution 1 pour pouvoir
démonter `/usr`. Assurez-vous également que le shell de root n'utilise pas
de bibliothèques sur cette partition. Si nécessaire, compilez un shell
lié de façon statique à l'usage de root.


La procédure pour chaque partition est la suivante&nbsp;:

1.  Créez autant de clés que vous voulez pour la partition, par exemple&nbsp;:

    ```bash
    head -c 2880 /dev/urandom | uuencode -m - | head -n-1 | tail -n+2 > keyfile
    ```

    ou utilisez une phrase de passe facile à retenir.
2.  Faites une sauvegarde sécurisée de vos clés et sécurisez le fichier
    de clés par `chmod 0400` ou autre. Vos clés de sauvegarde doivent
    absolument être sécurisées (i.e. pas sur votre ordinateur).
    Souvenez-vous, si vous perdez votre clé, vous ne serez
    définitivement __plus__ capable d'accéder à vos données&nbsp;!
3.  Faites une sauvegarde des données présentes sur la partition.
4.  Démontez la partition.
5.  Créez une partition chiffrée (Toutes les données présentes sur cette
    partition seront perdues). Lancez

    ```bash
    cryptsetup -c $algorithme-chiffrement luksFormat /dev/sd?? $fichier_de_clés
    ```

    Remplacez `$algorithme-chiffrement`, `/dev/sd??` et `$fichier_de_clés` avec
    les valeurs correspondantes.
6.  Éventuellement, ajoutez d'autres clés à la partition. Lancez

    ```bash
    cryptsetup -y -d $fichier_de_clés luksAddKey /dev/sd??
    ```

    Remplacez `$fichier_de_clés` de la même façon que plus haut et `/dev/sd??`
    par la partition correspondante.
7.  Ouvrez la partition chiffrée. Lancez

    ```bash
    cryptsetup -d $fichier_de_clés luksOpen /dev/sd?? sd??
    ```

    Remplacez `$fichier_de_clés` et `/dev/sd??` par les valeurs correspondantes.
    Remplacez `sd??` par un nom ayant du sens. Si tout s'est bien passé, la partition
    non chiffrée apparaîtra comme `/dev/mapper/sd??` où `sd??` est le nom
    que vous avez choisi.
8.  Créez un système de fichiers sur la partition. Lancez

    ```bash
    mkefs.$QUELQUECHOSE /dev/mapper/sd??
    ```

    Remplacez `$QUELQUECHOSE` par le type de système de fichiers que vous voulez
    utiliser (par exemple ext2) et `/dev/mapper/sd??` par la partition correspondante.
9.  Modifiez `/etc/fstab` Puisque le périphérique de la partition chiffrée
    a changé, vous devez préciser au système de fichiers où les trouver.
    Modifiez le périphérique en insérant `mapper/` dans le champ du
    périphérique.

	__Exemple__&nbsp;:

	```bash
    /dev/sda4 /home ext2 defaults 1 2
    ```
    devient&nbsp;:

    ```bash
    /dev/mapper/sda4 /home ext2 defaults 1 2
    ```
10. Montez le système de fichiers avec `mount /dev/mapper/sd??`
11. Copiez les données sur la partition.

## 4.2 Configurer le système pour qu'il monte et déchiffre automatiquement le(s) partition(s)

Créez un script de démarrage qui déchiffrera votre partition chiffrée.
On suppose que les phrases de passe sont stockées dans `/etc/crypt` par
exemple. Notez que conserver des phrases de passe sur le disque peut
poser des problèmes de sécurité&nbsp;! Utilisez le modèle pour les scripts de
démarrage fournis dans BLFS et faites-les exécuter&nbsp;:

```bash
/sbin/cryptsetup -d /etc/crypt/$PARTITION.key luksOpen \
   /dev/$PARTITION $PARTITION
```
pour chaque partition chiffrée autre que la partition racine et le(s)
partition(s) d'échange.

__Exemple__&nbsp;:

```bash
#!/bin/sh
########################################################################
# Début de $rc_base/init.d/cryptsetup
#
# Description : Rend disponible les systèmes de fichiers chiffrés pour être
#                               montés et nettoyés après
#
# Auteur      : Lars Bamberger
#
# Version     : 00.01
#
# Notes       : Il ne devrait pas être exécuté automatiquement avec un autre
#               argument que "start". Durant l'arrêt et le redémarrage, il
#               suffit de démonter les systèmes de fichiers. /dev/mapper/*
#               disparaîtra quand le noyau s'arrête ou redémarre.
#
########################################################################

. /etc/sysconfig/rc
. ${rc_functions}
PROC=/sbin/cryptsetup

case "${1}" in
        start)
                boot_mesg "luksOpen Home..."
                $PROC -d /etc/crypt/home.key luksOpen /dev/sda4 sda4
                evaluate_retval
        stop)
                boot_mesg "luksClose Home..."
                $PROC luksClose sda4
                evaluate_retval
                ;;
        reload)
                boot_mesg "Reloading home..."
                $PROC reload sda4
                evaluate_retval
                ;;
        restart)
                ${0} stop
                sleep 1
                ${0} start
                ;;
        status)
                $PROC status sda4
                ;;
        *)
                echo "Usage: ${0} {start|stop|reload|restart|status}"
                exit 1
                ;;
esac
# Fin de $rc_base/init.d/cryptsetup
```

Maintenant, avant de continuer, assurez-vous que tout ce qui a été fait
jusqu'à présent fonctionne comme prévu. Familiarisez-vous avec cette
manière de chiffrer vos partitions. Créez un lien symbolique pour que le
script soit lancé au démarrage&nbsp;:

```bash
cd /etc/rc.d/rcsysinit.d
ln -s ../init.d/cryptsetup S19cryptsetup
```

Vérifiez une nouvelle fois que le démarrage, le redémarrage, l'arrêt etc
fonctionnent comme prévu.

# 5. Un mot sur le chiffrement de(s) partition(s) d'échange

N'oubliez pas de chiffrer vos partitions d'échange. Beaucoup de données
intéressantes se trouvent sur les espaces d'échange. Ne considérez pas
que vos données sont en sûreté si vous n'utilisez pas des espaces
d'échange chiffrés.

En théorie, les données présentes sur le(s) partition(s) d'échange n'ont
pas besoin d'être cohérentes entre les redémarrages. Cela signifie que
nous pourrions créer un nouvel espace de d'échange au démarrage en
utilisant une clé de chiffrement aléatoire (donc différente) à chaque
démarrage. De cette façon vous n'avez pas à vous soucier de la gestion
des clés de chiffrement pour l'espace d'échange et vous n'aurez pas à
les stocker (sauf en mémoire). On pourrait considérer cela comme une
sécurité supplémentaire. Néanmoins, si vous suspendez votre système (en
RAM ou sur disque), les données de l'espace d'échange doivent rester
cohérentes. Vous devrez donc traiter la/les partition(s) d'échange comme
si c'étaient des partitions standards, ce qui signifie que vous devrez
les chiffrer comme expliqué plus haut.

# 6. Chiffrer le système de fichiers racine

Il n'est pas possible de chiffrer le système de fichiers racine comme
les autres partitions, car le système s'exécute dessus. L'idée est de
créer un initramfs contenant tous le nécessaire pour chiffrer (et
déchiffrer) le système de fichiers racine (Voir la documentation du
noyau pour les détails&nbsp;: `filesystems/ramfs-rootfs-initramfs.txt`).

Vous aurez besoin des répertoires standards (_bin_, _sbin_, _usr/{bin,sbin}_,
_proc_, _sys_, _dev_, _lib_). Dans bin, nous mettons busybox-large (renommé en
busybox), ainsi qu'un lien symbolique vers busybox appelé hush. Copiez
cryptsetup vers sbin. Placez quelques périphériques utiles dans dev&nbsp;:
console, null, sd??, ainsi qu'un répertoire `mapper` contenant
`control`. Puis faites une copie de dev&nbsp;:

```bash
cp -a dev init-dev
```

Dans lib (et dev) placez tout ce qui est nécessaire à l'exécution de busybox et
cryptsetup.

Le script d'initialisation est le suivant&nbsp;:

```bash
#!/bin/hush
/bin/busybox mount -t proc proc /proc
/bin/busybox mount -t sysfs sysfs /sys
/bin/busybox mount -t tmpfs tmpfs /dev
/bin/busybox cp -a /init-dev/* /dev
/bin/busybox --install -s
exec /bin/busybox hush
```

Placez tout cela dans un répertoire (init sera là et non pas dans sbin),
placez-vous dans ce répertoire puis créez l'image en utilisant

```bash
find . | cpio --quiet -H newc -o | gzip -9 -n > /boot/imagefile.img
```

Transmettez l'argument initrd approprié au noyau lors du démarrage (par exemple
`initrd (hd0,0)/initramfs.img`) et vous atterrirez dans un shell hush
après le démarrage du système.

{: .alert .alert-danger}
__PIÈGE__&nbsp;! Cryptsetup a besoin que `/proc` et `/sys` soient montés.
Il requiert aussi le répertoire `/dev`. Puisque nous voulons sauvegarder
`/dev` quand nous lancerons switch_root plus tard, nous le monterons
comme tmpfs. Cela signifie que les périphériques de `/dev` seront absents,
il faut qu'on les copie dans `/dev`. Notez bien que vous avez besoin que
'null' et 'console' soient présents dans `/dev` avant de monter tmpfs dans
`/dev`.

Une fois dans le shell, chiffrez votre système de fichiers racine comme
n'importe quel autre partition comme décrit plus haut. N'oubliez pas la
sauvegarde&nbsp;! Assurez-vous __absolument__ d'être capable __à coup sûr__
de monter et d'accéder à la sauvegarde non chiffrée du système de fichiers racine
depuis le shell hush&nbsp;!

Ensuite, créez la partition racine chiffrée. Notez que la phrase de
passe ne sera stockée nul part sur le disque, donc lancez&nbsp;:

```bash
cryptsetup -y -c $algorithme_de_chiffrement luksFormat /dev/sd??
```

pour créer le système de fichiers racine chiffré. Remplacez `$algorithme_de_chiffrement`
et `/dev/sd??` par leurs valeurs respectives. Ensuite, ouvrez la
partition, formatez-la et restaurez la sauvegarde&nbsp;:

```bash
cryptsetup luksOpen /dev/sd?? sd??
$BACKUPROOTFS/mkefs.$TYPE /dev/mapper/sd??
mkdir /new-root
mount -t $FSTYPE /dev/mapper/sd?? /new-root
cp -a $BACKUPROOTFS /new-root
```

{: .alert .alert-danger}
__PIÈGES__&nbsp;! Puique votre ancien système de fichiers racine
n'est pas monté, vous ne devriez pas être capable d'exécuter mkefs à
cause de bibliothèques manquantes. Vous pouvez, soit copier ce qui est
nécessaire à un emplacement accessible à l'éditeur de liens, soit
utiliser la version de mkfs fournie avec busybox. Assurez-vous que
busybox est configuré en conséquence.

Ensuite, modifiez `/etc/fstab` (sur `/new-root`) pour prendre en compte le
nouveau périphérique de système de fichiers racine. Modifiez aussi le
script cryptsetup comme décrit plus bas (7. PIÈGE).

# 7. Déchiffrer la partition racine lors des démarrages suivants

Comme au 6., créez un initramfs. La différence est que nous utilisons à
présent l'exécutable "busybox-minimum" et que vous aurez besoin d'un
répertoire supplémentaire new-root. N'oubliez pas le lien symbolique
'hush'. L'initialisation est comme suit&nbsp;: (remplacez `sd??` par le
périphérique racine et ajustez le type de système de fichiers).

```bash
#!/bin/hush
/bin/busybox mount -t proc proc /proc
/bin/busybox mount -t sysfs sysfs /sys
/bin/busybox mount -t tmpfs tmpfs /dev
/bin/busybox cp -a /init-dev/* /dev
/sbin/cryptsetup luksOpen /dev/sd?? sd??
/bin/busybox mount -r -t ext2 /dev/mapper/sd?? /new-root
/bin/busybox mount --move /proc /new-root/proc
/bin/busybox mount --move /sys /new-root/sys
/bin/busybox mount --move /dev /new-root/dev
exec /bin/busybox switch_root /new-root /sbin/init $@

```


{: .alert .alert-danger}
__PIÈGE__&nbsp;! Vous voulez conserver `/proc`, `/sys` et `/dev` après
switch_root car cryptsetup les utilise. D'où la commande
`mount --move`. Notez que `/dev/mapper/sd??` (le périphérique racine) ne sera
plus là quand vous aurez monté la vraie partition racine, exécuté
switch_root et quand le système de fichiers racine aura démarré udev.
C'est pour cela que ce périphérique doit être recréé. Modifiez donc le
script d'initialisation cryptsetup en ajoutant&nbsp;:


```bash
if [[ ! -b /dev/mapper/sd?? ]];
then
    boot_mesg "Making device for rootfs..."
    /bin/mknod -m 0600 /dev/mapper/sd?? b 254 0
    evaluate_retval;
fi
```
à la section 'start' du script

# 8. S'assurer que la sécurité n'est pas compromise

Une fois que tout marche comme il le devrait, supprimez la sauvegarde
non chiffrée de votre système de fichiers racine. Protégez votre
chargeur de démarrage (et si possible le BIOS) par un mot de passe pour
supprimer la possibilité de modifier les paramètres de démarrage sans
autorisation. Créez un script de démarrage (checkbootfs) qui s'assure
que la partition non chiffrée utilisée pour démarrer n'est pas
compromise. Utilisez quelque-chose comme&nbsp;:

```bash
boot_mesg "Checking integrity of boot FS..."
if
[[ $(/bin/md5sum -b /dev/sd??) == \
"$whatevermd5sum */dev/sd??" ]] \
    && \
[[ $(/bin/sha1sum -b /dev/sd??) == \
"$whatevensha1sum */dev/sd??" ]];
then
    echo_ok;
else
    echo_failure
boot_mesg -n "FAILURE:\n\nThe boot file system seems to have been altered!\n\n" ${FAILURE}
boot_mesg -n " DO NOT TRUST THIS SYSTEM!\n\n"
boot_mesg_flush
```

{: .alert .alert-danger}
__PIÈGE__&nbsp;! Assurez-vous que c'est la dernière chose que vous
implémenterez, car les sommes de contrôle changeront lors de la mise en
oeuvre. Les sommes de contrôle changeront aussi si vous exécutez fsck
sur la partition de démarrage.


# Remerciements&nbsp;:

 * Emmanuel Trillaud pour les suggestion et les liens.
 * Various pour les pages de wiki sur [en.gentoo-wiki.com/wiki/SECURITY_System_Encryption_DM-Crypt_with_LUKS](http://en.gentoo-wiki.com/wiki/SECURITY_System_Encryption_DM-Crypt_with_LUKS)
 * [Clemens Fruhwirth](http://clemens.endorphin.org/) pour LUKS pour dm-crypt: [luks.endorphin.org/dm-crypt](http://luks.endorphin.org/dm-crypt)

# Historique des changements&nbsp;:

[2009-12-30]

  * Intégration des suggestion (coquilles, format et autre) d'Emmanuel Trillaud
  * Plus de détails sur la taille de la partition de démarrage
  * Reformatage

[2009-11-23]

  * listage les dépendances du livre BLFS

[20-11-2009]

  * cryptsetup a besoin de /dev/urandom
  * mkefs pourrait ne pas fonctionner à partir de initramfs
  * mise à jour de certaines URLs
  * changements mineurs

[15-02-2009]

  * Réécriture basique.

[17-02-2008]

  * Astuce initiale.

# Historique de la traduction&nbsp;:

[04-01-2010]

  * mise à jour suite à la version du [2009-12-30]

[30-12-2009]

  * Relecture par JPM et publication

[18-12-2009]

  * première relecture par appzer0

[16-12-2009]

  * traduction de la version du [2009-11-23]

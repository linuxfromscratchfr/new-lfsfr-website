---
layout: astuce
use-site-title: true
title: Fuseaux horaires
author: Archaic &lt;archaic@A_ENLEVER.indy.rr.com&gt;
translator: Franck Chuiton &lt; franck point chuiton at gmail point fr &gt;
date: 2003-09-28
license: GFDL-1.2
synopsis: Explications sur les fuseaux horaires, UTC et la variable d'environnement TZ
description: "
Votre système Linux est multi-national. Il peut non seulement 
parler différentes langues, mais en plus il est sensible aux fuseaux horaires. 
Cette astuce vous explique comment configurer tout cela. 
"
---

Prérequis
=========

Le script fourni ici est basé sur lfs-bootscripts-1.11, mais il 
doit être facile de le modifier même si les scripts de démarrage subissent 
des changements importants.

Astuce
======

La Terre est divisée en 24 fuseaux horaires. L'heure locale d'un 
fuseau dépend du nombre de fuseaux qui le sépare de Greenwich, Grande 
Bretagne. La différence en temps est relative à la zone 0, celle de 
Greenwich. Par exemple, à Indiana, USA, où je vis, nous sommes dans le fuseau 
-5. Ce qui veut dire que nous sommes 5h plus tôt qu'en Grande Bretagne. Cela 
vient compenser la rotation de la Terre.

## Heure locale / heure universelle
La première question, et la plus importante, à laquelle il va vous falloir 
répondre est de savoir si vous voulez que votre machine conserve l'heure au 
format local ou au format universel. UTC ( Universal Time Coordinated = Temps 
Universel Coordonné) est équivalent à GMT (Greenwich Mean Time = Heure 
Moyenne de Greenwich). L'heure locale est l'heure qui est affichée à 
l'horloge, près de vous, sur le mur. Chaque format a ses avantages et ses 
inconvénients, mais les deux sont évoqués dans cette astuce.

La tradition veut que l'heure système de toutes les machines POSIX (par 
exemple Solaris, BSD, UNIX) soit au format universel (UTC). Les O.S. les plus 
stupides (ceux de Microsoft principalement) demandent à leurs utilisateurs de 
configurer leur machine en heure locale. Heureusement, Linux peut gérer à la 
fois les machines configurées en UTC et celles qui, souffrant de la maladie de 
Microsoft, conservent l'heure système au format local.
Arrivés ici, il va vous falloir choisir une des deux : heure locale ou heure 
universelle ?

Quelques éléments pour vous aider : si vous disposez de Windows et Linux dans 
le même boîtier, je vous conseille d'utiliser l'heure locale. Si vous avez
Windows mais le démarrez rarement ou si vous ne l'avez pas du tout, c'est une 
bonne idée d'utiliser l'heure au format universel. Une fois que votre choix 
est fait, éditez /etc/sysconfig/clock. Saisissez UTC=0 pour l'heure locale et 
UTC=1 pour le format universel (UTC/GMT).

## Détermination de votre fuseau horaire

Le fait de savoir dans quel fuseau horaire vous vivez est important pour la 
suite de cette astuce.

Cependant connaître le nombre de fuseaux qui vous séparent de celui de 
Greenwich ne suffit pas, les changements d'heure été/hiver sont également 
influencés par ce choix. 

LFS dispose d'un programme très simple qui vous permet de déterminer votre 
fuseau horaire en seulement quelques questions (généralement 2 ou 3).

Lancez-le : 

```bash
tzselect
```

À la sortie du programme, la dernière ligne qui s'affiche correspond à 
votre fuseau horaire. Chez moi, elle affiche "America/Indianapolis" (sans les 
guillemets). Conservez cette valeur. Notez-la ou collez-là quelque part dans
un fichier texte. Pour simplifier ces explications, nous utiliserons $TIMEZONE 
dans la suite de cette astuce pour nous référer à cette variable.
    
   
## Utilisation du paramètre $TIMEZONE

Maintenant que vous connaissez le fuseau horaire dans lequel vous vous 
trouvez, nous allons pouvoir utiliser cette information. D'abord, nous 
commençons par créer le fichier /etc/localtime . Ce fichier doit exister quel 
que soit votre choix (UTC ou heure locale). Ça peut être un lien symbolique 
ou une copie du fichier réel renommé en localtime. À l'heure où j'écris 
cette astuce, LFS utilise un fichier réel donc j'utiliserai cette méthode 
même si un lien symbolique fonctionne très bien. 

```bash
cp --remove-destination /usr/share/zoneinfo/$TIMEZONE /etc/localtime
```

Ensuite il nous faut modifier le script de démarrage setclock. Mais tout 
d'abord, quelques explications sur l'horloge système et l'horloge matérielle.
L'horloge matérielle est celle qui fait tic-tac dans votre bios et conserve 
l'heure même quand votre système est éteint. Cependant, cette horloge 
matérielle n'est pas si précise que cela.  L'horloge système est quant à 
elle maintenue par votre noyau en cours de fonctionnement et est bien plus 
précise. Mais comment l'horloge système peut-elle savoir l'heure qu'il est ?
Pendant le démarrage, les scripts standards de démarrage de LFS configurent 
l'horloge système en se basant sur l'horloge matérielle. Ensuite, l'horloge 
matérielle est ignorée. Le problème avec ce scénario est qu'après un temps 
de fonctionnement sur plusieurs jours (et en fonction de l'exactitude de votre 
BIOS), un décalage assez important peut intervenir entre l'horloge système et 
l'horloge matérielle. Après un redémarrage, l'horloge matérielle, qui n'est 
plus à l'heure, va être utilisée pour configurer l'horloge système. Il vous 
incombe donc de permettre au noyau de mettre à jour l'heure du BIOS à
l'extinction de la machine afin qu'il ait plus de chances d'être à la bonne 
heure au prochain redémarrage de la machine.  On utilise le programme 
hwclock(8) pour lire ou modifier l'horloge matérielle. Ce programme fait 
partie d'un système LFS normal, vous n'avez donc pas besoin de le 
télécharger. Nous allons modifier quelques uns des scripts de démarrage pour 
lancer ce programme à l'allumage et à l'extinction de la machine. 
NOTE : Keith Harwood m'informe que l'horloge matérielle de son boîtier DS10
Alpha est plus précise que son horloge système. L'heure système perdait deux 
minutes par jour. Il s'agit vraisemblablement d'une quelconque faille du noyau, 
mais vous devriez probablement vérifier l'heure de votre système auprès 
d'une source fiable et la comparer après au moins un jour de fonctionnement du 
système.

Je doute que des utilisateurs sur x86 puissent avoir une horloge matérielle plus 
précise que l'horloge système.

Dans le livre, un script, nommé setclock, est créé dans /etc/rc.d/init.d. Ce 
script ne réalise que la moitié du travail puisqu'il ne fait que configurer 
l'heure système d'après l'horloge matérielle. Nous allons modifier ce script 
et créer de nouveaux liens symboliques pour qu'il puisse enregistrer l'heure 
système dans l'horloge matérielle à l'extinction ou au redémarrage.

```bash
cat >/etc/rc.d/init.d/setclock <<"EOF"
#!/bin/bash
# Begin $rc_base/init.d/setclock - Configuration de l'horloge Linux

# Basé sur le script setclock de LFS-3.1 et précédentes.
# Réécrit par Gerard Beekmans  - gerard@linuxfromscratch.org
# Réécrit par Marc Heerdink pour y inclure la modification de l'horloge 
matérielle
# Réécrit par Archaic <archaic@A_ENLEVER.indy.rr.com> pour conformité avec 
lfs-bootscripts-1.11

source /etc/sysconfig/rc
source $rc_functions
source /etc/sysconfig/clock

case "$1" in
     start)
          case "$UTC" in
               yes|true|1)
                    echo "Configuration de l'heure système..."
                    hwclock --hctosys --utc
                    evaluate_retval
                    ;;

               no|false|0)
                    echo "Configuration de l'heure système..."
                    hwclock --hctosys --localtime
                    evaluate_retval
                    ;;

               *)
                    echo "Valeurs incorrectes pour UTC dans 
/etc/sysconfig/clock: $UTC"
                    echo "Les valeurs possibles pour UTC sont 1 et 0."
                    exit 1
                    ;;
          esac
          ;;

     stop)
          case "$UTC" in
               yes|true|1)
                    echo "Mise à jour de l'horloge matérielle..."
                    hwclock --systohc --utc
                    evaluate_retval
                    ;;

               no|false|0)
                    echo "Mise à jour de l'horloge matérielle..."
                    hwclock --systohc --localtime
                    evaluate_retval
                    ;;

               *)
                    echo "Valeurs incorrectes pour UTC dans 
/etc/sysconfig/clock: $UTC"
                    echo "Les valeurs possibles pour UTC sont 1 et 0."
                    exit 1
                    ;;
          esac
          ;;

     *)
          echo "Usage: $0 {start|stop}"
          exit 1
          ;;
esac

# End $rc_base/init.d/setclock
EOF

chmod 755 /etc/rc.d/init.d/setclock
```

Étape suivante : les liens symboliques. Le lien qui permet de lancer le 
script setclock est déjà présent dans /etc/rc.d/rc.sysinit.d, donc les seuls 
liens symboliques que nous avons à créer sont ceux qui vont permettre de 
lancer setclock à l'extinction du système :

```bash
ln -s ../init.d/setclock /etc/rc.d/rc0.d/K45setclock &&
ln -s ../init.d/setclock /etc/rc.d/rc6.d/K45setclock
```

À ce point, les scripts de démarrage sont correctement configurés et il reste 
juste à paramétrer la variable d'environnement TZ.

## La variable d'environnement TZ

Cette variable est utilisée par hwclock, quand il est lancé depuis un shell, 
et par certains programmes qui dépendent fortement des fuseaux horaires. Cette 
variable est utilisée globalement par le système. C'est donc une bonne idée 
de la définir au sein de l'environnement du système, dans le fichier 
/etc/profile.
Ajoutez ces lignes à /etc/profile :

```bash
export TZ=value_of_$TIMEZONE
```

Informations
============

l'auteur original de cette astuce est Marc Heerdink. C'est 
toujours 80% de son texte.

Changelog
=========

-  [10-04-2002]
   *   Version finale de Marc Heerdink
-  [28-09-2003]
   *   Suppression d'infos redondantes ou inutiles
   *   corrections orthographiques et grammaticales
   *   mise à jour du script et des emplacements de fichiers pour correspondre à 
       un système LFS actuel
   *   Actualisation de la mise en page au format "astuce"

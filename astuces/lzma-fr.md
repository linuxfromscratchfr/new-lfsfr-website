---
layout: astuce
use-site-title: true
title: compression de fichiers LZMA / 7-zip
author: Robert Connolly &lt;robert@linuxfromscratch.org&gt;
translator: Emmanuel Trillaud &lt;emmanuel.trillaud@gmail.com&gt;
date: 2005-10-06
license: Public Domain
synopsis: compression de fichiers LZMA / 7-zip
description: "
LZMA ou 7-zip est un nouvel outil de compression de fichier qui compresse les
fichier 10-30% plus efficacement que bzip2 ou gzip. LZMA a été porté sur Linux,
BSD et Windows. 7-zip utilise aussi plus de puissance processeur que bzip2, mais il est
rapide pour décompresser.
"
---

Astuce
======

La page web pour le logiciel principal est&nbsp;:

*	[http://www.7-zip.com/sdk.html](http://www.7-zip.com/sdk.html)

Logiciels compatibles&nbsp;:

*	[http://p7zip.sourceforge.net/](http://p7zip.sourceforge.net/)
*	[http://martinus.geekisp.com/rublog.cgi/Projects/LZMA/](http://martinus.geekisp.com/rublog.cgi/Projects/LZMA/)
*	[http://www.zelow.no/floppyfw/download/Development/lzma/lzmatool-0.11.tgz](http://www.zelow.no/floppyfw/download/Development/lzma/lzmatool-0.11.tgz)

Tout d'abord, téléchargez ceci (ou une version plus récente)&nbsp;:
[http://www.7-zip.org/dl/lzma417.tar.bz2](http://www.7-zip.org/dl/lzma417.tar.bz2)

Déballez le logiciel
--------------------

```bash
mkdir lzma417/ &&
tar jxf lzma417.tar.bz2 -C lzma417/
```

Compilez et installez le logiciel
---------------------------------

```bash
cd lzma417/SRC/7zip/Compress/LZMA_Alone/ &&
make &&
install lzma /bin/
```

Il n'y a pas de page de manuel. Entrez 'lzma' pour accéder à un menu d'aide.
lzma ne se comporte pas comme gzip ou bzip2 par défault. Deux petits scripts
peuvent aider&nbsp;:

```bash
cat > /tmp/7zip.sh << "EOF"
#!/bin/sh
/bin/lzma e ${1} ${1}.7z &&
rm -f ${1}
EOF
install /tmp/7zip.sh /bin/7zip

cat > /tmp/7unzip.sh << "EOF"
#!/bin/sh
/bin/lzma d ${1} $(echo ${1} | sed -e 's/.7z$//') &&
rm -f ${1}
EOF
install /tmp/7unzip.sh /bin/7unzip

rm /tmp/{7zip,7unzip}.sh
```

Ces deux scripts se comporteront comme gzip et gunzip en compressant le
fichier et en supprimant l'original en cas de succès.

Pour compresser votre noyau avec LZMA utilisez l'un de ces correctifs&nbsp;:

*	[linux-2.4-lzma-1.patch](http://www.linuxfromscratch.org/patches/downloads/linux/linux-2.4-lzma-1.patch)
*	[linux-2.6-lzma-1.patch](http://www.linuxfromscratch.org/patches/downloads/linux/linux-2.6-lzma-1.patch)

Appliquez le correctif au noyau et ça marchera tout seul. Les routines Zlib
sont remplacées par LZMA. Mon noyau pesait 1,3Mio avant LZMA et 1,1Mio après.

À faire
-------

*   Ce serait bien de supporter LZMA pour les modules du noyau dans modutils.

Remerciements
-------------

*   Merci à Google pour m'avoir aidé à chercher des informations sur LZMA
*   Merci à l'équipe de lZMA&nbsp;:
    [http://www.7-zip.com/sdk.html](http://www.7-zip.com/sdk.html)
*   Merci à Ming-Ching Tiew pour les correctifs LZMA du noyau. 

Historique des modifications
----------------------------

-  [2005-06-09]
   *  Astuce initiale.
-  [2005-06-10]
   *  Ajout des URLs pour les logiciels compatibles.

Historique de la traduction
---------------------------

-  [2009-05-15]
   *  Traduction initiale de l'astuce

